%% Calculate State Specific Rate 
clear
clc
addpath('./Core');  %Core Code

PES_use    = 1;
PES_folder = {'One','Double'};
addpath(['./PesData/',PES_folder{PES_use}]); % PES data

IsEq = 0;                    % set to one if it's equilibrium rate calculation
jobid = 601;                 % identify hostlist and rand number, 3 digit
 
runon = 1;                   % 1 for rice 2 for carter 3 for local
cores = 38;

%% Create folder for data storage
if IsEq
    RateType = 'Eq';    
else
    RateType = 'SS';
end

if runon == 1
    cluster = 'rice';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/',RateType,'ReacRate/',PES_folder{PES_use}];
    data_folder = ['/scratch/',cluster,'/l/luo160/DATA/',RateType,'ReacRate/',PES_folder{PES_use}];
elseif runon == 2
    cluster = 'carter';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/',RateType,'ReacRate/',PES_folder{PES_use}];
    data_folder = ['/scratch/',cluster,'/l/luo160/DATA/',RateType,'ReacRate/',PES_folder{PES_use}];   
else
    trj_folder = [RateType,'ReacRate/',PES_folder{PES_use}];
    data_folder = [RateType,'ReacRate/',PES_folder{PES_use}];
end
if ~exist(data_folder,'dir')
    mkdir(data_folder);
end

global_log = fopen([RateType,'ReacRateLog',num2str(jobid,'%03d'),'.txt'],'a+');

log_folder = ['Log',RateType,'ReacRate/',PES_folder{PES_use}];
if ~exist(log_folder,'dir')
    mkdir(log_folder);
end

r_folder    = [RateType,'ReacRate/',PES_folder{PES_use}]; %set carefully
if ~exist(r_folder,'dir')
    mkdir(r_folder);
end

%% Set run enviroment
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
else
    p = parcluster('local');
    p.NumWorkers = cores;
%     p.saveProfile;
end
fprintf(global_log,'Parallel enviroment set\n');

%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%% Load property
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
%% Set sweep range
Tsweep = [2000 3000 4000 5000 6000 7000 8000 9000 10000];      %Temperature
if IsEq
    Vsweep = 1:prop.Vmax(1)+1;
    Jsweep = 1:prop.Jmax(1,1)+1;
else
    Vsweep = [5];
    Jsweep = [0 20];
    
    l = [length(Tsweep), length(Vsweep), length(Jsweep)];
    NSweepPoint = l(1)*l(2)*l(3);
    SweepPoint = zeros(NSweepPoint,3);   % T v J feel free to set by your self
    for i = 1:l(1)
        index = l(2)*l(3)*(i-1)+1;
        SweepPoint(index:index+l(2)*l(3)-1,1) = Tsweep(i);
        for j = 1:l(2)
            SweepPoint(index:index+l(3)-1,2) = Vsweep(j);
            SweepPoint(index:index+l(3)-1,3) = Jsweep';
            index = index+l(3);
        end
    end
end

%% set up input
input.Bmax=6.5; % A
input.Calc=2; % Type 1 is X-sect calc, Type 2 is state specific rate calc., Type 3 is monoquantum Deex
input.Ncalls = 1e6;
input.InnerLoops = 10;
input.SaveData=1;

input.Vrel=1; % useless, dummy varibale
fprintf(global_log,'Work before pool is done\n');
%% open parpool
if runon == 1 || runon ==2
    try
        parpool(conf,cores);
    catch
        try
            pause(300)
            parpool(conf,cores);
        catch
            try
                pause(600)
                parpool(conf,cores);
            catch
                pause(600)
                parpool(conf,cores);
            end
        end
    end
else
    parpool;
end
fprintf(global_log,'Pool is open\n');
%% Take note of running cluster number
if runon==1 || runon==2
    fid=fopen(['hostlist',num2str(jobid)],'w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen(['hostlist',num2str(jobid)],'a+');
        [~,name]=system('hostname');
        fprintf(fid,'Parallel:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end
if exist(['random',num2str(jobid,'%03d'),'.mat'],'file')
    load(['random',num2str(jobid,'%03d'),'.mat'])
    rng(rangset)
end
%% Write gllobal log
DATE1 = date;
DATE2 = clock;
fprintf (global_log,' Running Reac Rate Sweep \n');
fprintf (global_log, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
fprintf (global_log,' JobId      = %3d \n',        jobid);
fprintf (global_log,' Bmax       = %5.3f A\n', input.Bmax);
fprintf (global_log,' Ncalls     = %5.3f \n', input.Ncalls);
fprintf (global_log,' InnerLoops = %5.3f \n', input.InnerLoops);
if input.Calc == 2
    if ~IsEq
        fprintf (global_log,' Calc Type  = State Specific Rate \n');
        fprintf (global_log,' T          = ['); fprintf (global_log,'   %5.2f',Tsweep);fprintf (global_log,']\n');
        fprintf (global_log,' Vsweep     = ['); fprintf (global_log,'%5d', Vsweep);fprintf (global_log,']\n');
        fprintf (global_log,' Jsweep     = ['); fprintf (global_log,'%5d', Jsweep);fprintf (global_log,']\n');
    else
        fprintf (global_log,' Calc Type  = Equilibrium Rate \n');
        fprintf (global_log,' T          = ['); fprintf (global_log,'%5.2f',Tsweep);fprintf (global_log,']\n');
    end
else
    fprintf (global_log,' Wrong script is used \n');
    return
end


%% Calculation

if IsEq
    for Tloop = 1:length(Tsweep)
        input.Ttrans = Tsweep(Tloop);
        
        input.Log = [log_folder,'/Log_T=',num2str(input.Ttrans),'.txt'];
        input.LogFid = fopen(input.Log,'a+');
        
        input.TrjLoc = [trj_folder,'/T=',num2str(input.Ttrans)];
        if ~exist(input.TrjLoc,'dir')
            mkdir(input.TrjLoc);
        end
        
        input.TrjLog = [log_folder,'/Log_Trj_T=',num2str(input.Ttrans),'.txt'];
        input.TrjFid = fopen(input.TrjLog,'a+');
        
        
        restart_folder = [r_folder,'/T=',num2str(input.Ttrans)];
        if ~exist(restart_folder,'dir')
            mkdir(restart_folder);
        end
        
        DATE1 = date;
        DATE2 = clock;
        fprintf (global_log,' ----------------------------------------------------- \n');
        fprintf (global_log,' Running Equilibrium Rate Calculation \n');
        fprintf (global_log, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
        fprintf (global_log,' Bmax       = %5.3f \n', input.Bmax);
        fprintf (global_log,' Trans      = %10.3f \n', input.Ttrans);
        
        DATE1 = date;
        DATE2 = clock;
        fprintf (input.LogFid,' ----------------------------------------------------- \n');
        fprintf (input.LogFid,' Running Equilibrium Rate Calculation \n');
        fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
        fprintf (input.LogFid,' Bmax       = %5.3f \n', input.Bmax);
        fprintf (input.LogFid,' Trans      = %10.3f \n', input.Ttrans);
        
        clear StateRate ErrorCount CurrentStateRate CurrentErrorCount
        StateRate = cell(prop.Vmax(1)+1,prop.Jmax(1,1)+1);
        ErrorCount = cell(prop.Vmax(1)+1,prop.Jmax(1,1)+1);
        
        tic
        clear time time_count
        time_count = 1;
        time(time_count) = 0;
        
        
        for VLoop = 1: (prop.Vmax(1)+1)
            input.V = VLoop-1;
            
            time(time_count) = toc;
            name2 = [restart_folder,'/V=',num2str(input.V),'.mat'];
            if exist(name2,'file')
                load(name2,'CurrentStateRate','CurrentErrorCount')
                StateRate(VLoop,:) = CurrentStateRate;
                ErrorCount(VLoop,:)= CurrentErrorCount;
                time_count = time_count+1;
                time(time_count) = toc;
                fprintf (' Load (T, V)=(%5.3f, %5.0f)  \n', input.Ttrans, input.V);
                fprintf (' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                
                fprintf (global_log,' Load (T, V)=(%5.3f, %5.0f)  \n', input.Ttrans, input.V);
                fprintf (global_log,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                
                fprintf (input.LogFid,' Load (T, V)=(%5.3f, %5.0f)  \n', input.Ttrans, input.V);
                fprintf (input.LogFid,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
            else
                for JLoop = 1: (prop.Jmax(VLoop)+1)
                    input.J = JLoop-1;
                    
                    time(time_count) = toc;
                    
                    [StateRate{VLoop,JLoop}, ErrorCount{VLoop,JLoop}] = Reac_Trj_Calc(input, prop, par,prop.wellDepth(1)-prop.wellDepth(2)-0.05);
                    
                    time_count = time_count+1;
                    time(time_count) = toc;
                    
                    % Report progress
                    fprintf (' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                    fprintf (' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                    
                    fprintf (global_log,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                    fprintf (global_log,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                    
                    fprintf (input.LogFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                    fprintf (input.LogFid,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                    
                    fprintf (input.TrjFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                    fprintf (input.TrjFid,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
                    
                    rangset = rng;
                    save(['random',num2str(jobid,'%03d'),'.mat'],'rangset')
                end
                % temperoray save
                CurrentStateRate = StateRate(VLoop,:);
                CurrentErrorCount = ErrorCount(VLoop,:);
                
                name2 = [restart_folder,'/V=',num2str(input.V),'.mat'];
                temp = whos('CurrentErrorCount');
                if max([temp.bytes])/1024/1024/1024 > 1.2
                    save(name2,'CurrentStateRate','CurrentErrorCount','input','-v7.3');
                else
                    save(name2,'CurrentStateRate','CurrentErrorCount','input');
                end
                
                %save a copy in case
                d_folder = [data_folder,'/T=',num2str(input.Ttrans),'_vib'];   % save to scratch
                if ~exist(d_folder,'dir')
                    mkdir(d_folder);
                end
                name2 = [d_folder ,'/V=',num2str(input.V),'.mat']
                save(name2,'CurrentStateRate','CurrentErrorCount','input');
            end
            
        end
        
        fprintf (global_log,  'Calculation is done for T = %9.2f\n.',input.Ttrans);
        fprintf (input.LogFid,'Calculation is done for T = %9.2f\n.',input.Ttrans);
        fprintf (input.TrjFid,'Calculation is done for T = %9.2f\n.',input.Ttrans);
        
        
        name2 = [data_folder,'/T=',num2str(input.Ttrans),'.mat'];
        temp = whos;
        if max([temp.bytes])/1024/1024/1024 > 1.2
            save(name2,'StateRate','ErrorCount','prop','par','input','-v7.3');
        else
            save(name2,'StateRate','ErrorCount','prop','par','input');
        end
        
        fprintf (global_log,  'Multiple data file for T = %9.2f is saved\n');
        fprintf (input.LogFid,'Multiple data file for T = %9.2f is saved\n');
        fprintf (input.TrjFid,'Multiple data file for T = %9.2f is saved\n');
        
        fclose(input.LogFid);
        fclose(input.TrjFid);
    end
else
    for SweepCount = 1:NSweepPoint
        input.Ttrans = SweepPoint(SweepCount,1);
        input.V      = SweepPoint(SweepCount,2);
        input.J      = SweepPoint(SweepCount,3);
       
        switch input.Ttrans
        case  2000
          input.Ncalls = 1e9
        case 4000
          input.Ncalls = 1e7
        case 3000
          input.Ncalls = 5e7
        case 5000
          input.Ncalls = 1e7
        case 6000
          input.Ncalls = 3.5e6
        case 7000
          input.Ncalls = 2.5e6
        case 8000
          input.Ncalls = 2e6
        case 9000
          input.Ncalls = 1e6
        case 10000
          input.Ncalls = 1e6
        otherwise
          input.Ncalls = 1e6
        end

             
        input.Log = [log_folder,'/Log_T=',num2str(input.Ttrans),'.txt'];
        input.LogFid = fopen(input.Log,'a+');
        
        input.TrjLoc = [trj_folder,'/T=',num2str(input.Ttrans)];
        if ~exist(input.TrjLoc,'dir')
            mkdir(input.TrjLoc);
        end
        
        input.TrjLog = [log_folder,'/Log_Trj_T=',num2str(input.Ttrans),'.txt'];
        input.TrjFid = fopen(input.TrjLog,'a+');
        
        % two copies are saved for
        d_folder = [data_folder,'/T=',num2str(input.Ttrans)];   % save to scratch
        if ~exist(d_folder,'dir')
            mkdir(d_folder);
        end
        
        restart_folder = [r_folder,'/T=',num2str(input.Ttrans)];  % save to local
        if ~exist(restart_folder,'dir')
            mkdir(restart_folder);
        end
        
        DATE1 = date;
        DATE2 = clock;
        fprintf (global_log,' ----------------------------------------------------- \n');
        fprintf (global_log, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
        fprintf (global_log,' Bmax       = %5.3f \n', input.Bmax);
        
        DATE1 = date;
        DATE2 = clock;
        fprintf (input.LogFid,' ----------------------------------------------------- \n');
        fprintf (input.LogFid,' Running Equilibrium Rate Calculation \n');
        fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
        fprintf (input.LogFid,' Bmax       = %5.3f \n', input.Bmax);

        
        clear StateRate ErrorCount
        StateRate = cell(1,1);
        ErrorCount = cell(1,1);
        
        tic
        clear time time_count
        time_count = 1;
        time(time_count) = toc;
        
        name2 = [d_folder,'/T=',num2str(input.Ttrans),'_V=',num2str(input.V),'_J=',num2str(input.J),'.mat'];
        if exist(name2,'file')
            fprintf (global_log,' (T, V, J)=(%5.3f, %5.0f, %5.0f) exist  \n', input.Ttrans, input.V, input.J);
            fprintf (' (T, V, J)=(%5.3f, %5.0f, %5.0f) exist  \n', input.Ttrans, input.V, input.J);
        else
            
            [StateRate{1,1}, ErrorCount{1,1}] = Reac_Trj_Calc(input, prop, par, 3.2);%prop.wellDepth(1)-prop.wellDepth(2)-0.05);
            
            time_count = time_count+1;
            time(time_count) = toc;
            
            % Report progress
            fprintf (' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{1,1}.Count);
            fprintf (' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
            
            fprintf (global_log,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{1,1}.Count);
            fprintf (global_log,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
            
            fprintf (input.LogFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{1,1}.Count);
            fprintf (input.LogFid,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
            
            fprintf (input.TrjFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{1,1}.Count);
            fprintf (input.TrjFid,' This took %5.3f s . Total %f mins \n', time(time_count)-time(time_count-1),time(time_count)/60);
            
            rangset = rng;
            save(['random',num2str(jobid,'%03d'),'.mat'],'rangset')
            
            name2 = [restart_folder,'/T=',num2str(input.Ttrans),'_V=',num2str(input.V),'_J=',num2str(input.J),'.mat'];
            temp = whos;
            if max([temp.bytes])/1024/1024/1024 > 1.2
                save(name2,'StateRate','ErrorCount','input','-v7.3');
            else
                save(name2,'StateRate','ErrorCount','input');
            end
            
            name2 = [d_folder,'/T=',num2str(input.Ttrans),'_V=',num2str(input.V),'_J=',num2str(input.J),'.mat'];
            if max([temp.bytes])/1024/1024/1024 > 1.2
                save(name2,'StateRate','ErrorCount','input','-v7.3');
            else
                save(name2,'StateRate','ErrorCount','input');
            end
            
        end
        
        fclose(input.LogFid);
        fclose(input.TrjFid);
    end
end
    
fclose all 
delete(gcp)
