clear
clc
addpath('E:\Git\mkqct\Core');
addpath('E:\Git\mkqct\PesData\double');
%
% addpath('./Core');  %Core Code
% addpath('./PesData/double'); % PES data
parpool('standby_rice',40);

prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
par.Etol = 1e-5;

load IC.mat DAT

%% Initilize data
ntrj = length(DAT);
CurrentOut = struct( 'React', 0,  'JLevel', 0, 'VLevel', 0);
CurrentOut = repmat(CurrentOut,[ntrj,1]);

error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
error = repmat(error,[ntrj,1]);

parfor i = 1:10
    x0 = reshape(DAT(i).x0',[],1);
    p0 = reshape(DAT(i).p0',[],1);
    p0 = p0*2.1876912633*1e3*0.01/1822.888486192;
    
    [out,error(i)] = CallMaratCode( x0,p0,prop,par );
    CurrentOut(i).React = out.React;
    CurrentOut(i).JLevel = out.JLevel;
    CurrentOut(i).VLevel = out.VLevel;
end

code = [error.code];
WrongIndex = find(code ~= 0 & code ~=41 & code ~=42);
if ~isempty(WrongIndex)
    CurrentError = struct('index',0,'code', 0, 'where', 'Nowhere', 'what', 'Nothing');
    CurrentError = repmat(CurrentError,[length(WrongIndex),1]);
    
    for i = 1:length(WrongIndex)
        id = WrongIndex(i);
        CurrentError(i).index = id;
        CurrentError(i).code = error(id).code;
        CurrentError(i).where = error(id).where;
        CurrentError(i).what = error(id).what;
    end
    clear DAT error code prop out par
    temp = whos;
    if max([temp.bytes])/1024/1024/1024 > 1.2
        save('Result.mat','CurrentOut','CurrentError','WrongIndex','-v7.3');
    else
        save('Result.mat','CurrentOut','CurrentError','WrongIndex');
    end
else
    clear DAT error code prop out par
    temp = whos;
    if max([temp.bytes])/1024/1024/1024 > 1.2
        save('Result.mat','CurrentOut','WrongIndex','-v7.3');
    else
        save('Result.mat','CurrentOut','WrongIndex');
    end
end
