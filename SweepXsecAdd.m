%% Test Sample size compare reuslt with esposito
clear
clc
%% Set run enviroment
runon = 1;    %1 for rice 2 for carter 3 for local
cores = 38;
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
    matlabpool(conf,cores);
else
    p = parcluster('local');
    p.NumWorkers = cores;
    p.saveProfile;
    parpool(conf,cores);
end
%% Take note of running cluster number
if runon==1 || runon==2
    fid=fopen('hostlist','w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen('hostlist','a+');
        [~,name]=system('hostname');
        fprintf(fid,'Serial:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end
if runon == 1
    cluster = 'rice';
elseif runon == 2
    cluster = 'carter';
end
if exist('random.mat','file')
    load random.mat
    rng(rangset)
end
%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%---------------------------------------------------------------------------------------------------------------
load DATA/Vr=0.11.mat prop par Output ErrorC input Jsweep Vsweep VrLoop
input.TrjFid = fopen(input.TrjLog,'a+');
input.LogFid = fopen(input.Log,'a+');

VsweepAdd = [7 15 25 30];
OldVsweepLen = length(Vsweep);
Vsweep = [Vsweep VsweepAdd];
%% Open Log file
DATE1 = date;
DATE2 = clock;
fprintf (input.LogFid,' Continue Running CrossSection Sweep \n');
fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
tic
time(1) = 0;
l = 1;

%% attention!! length(Vrel) = 1
input.Vrel = input.Vrel;
for VLoop = OldVsweepLen+1: length(Vsweep)
    input.V = Vsweep(VLoop);
    for JLoop = 1: length(Jsweep)
        l = l + 1;
        input.J = Jsweep(JLoop);
        
        % Calculate Cross Sections for this level
        [Output{VrLoop,VLoop,JLoop}, ErrorC{VrLoop,VLoop,JLoop}] = CrossSection_Rate_Calc_NNO(input, prop, par);
        
        time(l) = toc;
        
        % Report progress
        fprintf (' Ran (Vr, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Vrel, input.V, input.J, ErrorC{VrLoop, VLoop, JLoop}.Count);
        fprintf (' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600)
        fprintf (input.LogFid,' Ran (Vr, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Vrel, input.V, input.J, ErrorC{VrLoop, VLoop, JLoop}.Count);
        fprintf (input.LogFid,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);
        fprintf (input.TrjFid,' Ran (Vr, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Vrel, input.V, input.J, ErrorC{VrLoop, VLoop, JLoop}.Count);
        fprintf (input.TrjFid,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);
        CurrentOut = Output{VrLoop,VLoop,JLoop};
        CurrentError = ErrorC{VrLoop,VLoop,JLoop};
        name2 = strcat(Restartfile,'Vr=',num2str(input.Vrel),'V=',num2str(input.V),'J=',num2str(input.J),'.mat');
        save(name2,'CurrentOut','CurrentError');
    end
end
[A,B]=sort(Vsweep);
S=Output(VrLoop,:,:);
D=ErrorC(VrLoop,:,:);
for iii=1:length(Vsweep)
    Output(VrLoop,iii,:)=S(1,B(iii),:);
    ErrorC(VrLoop,iii,:)=D(1,B(iii),:);
end
Vsweep=A;
clear S D A B
name2 = strcat(Restartfile,'Vr=',num2str(input.Vrel),'_add.mat');
temp = whos;
if max([temp.bytes])/1024/1024/1024 > 1.2
    save(name2,'-v7.3');
else
    save(name2);
end

% name2 = strcat(Restartfile,'Vr=',num2str(input.Vrel),'V=*','J=*');
% delete(name2);
rangset = rng;
save('random.mat','rangset')
fclose all;
delete(gcp)
