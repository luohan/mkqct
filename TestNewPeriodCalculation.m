%% Test Sample size compare reuslt with esposito
clear
clc
addpath('./Core');  %Core Code
autoang = 5.2917721092e-11*1e10;
autofs = 2.418884326505e-17*1e15;

PES_use    = 2;
PES_folder = {'One','Double'};
addpath(['./PesData/',PES_folder{PES_use}]); % PES data
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
V = 20;
J = 0;
[RR, PP, IRUN, R_stretch, Pv_stretch,error] = RunVibPeriod(V,J,prop,par);
figure(2)
subplot(1,2,1)
plot([1:IRUN]*par.dt,R_stretch(1:IRUN))
subplot(1,2,2)
plot([1:IRUN]*par.dt,Pv_stretch(1:IRUN))
 %% compare
% plot([1:IRUN]*par.dt,R_stretch(1:IRUN))
% hold on
% 
% rin = min(R_stretch(1:IRUN));
% rout = max(R_stretch(1:IRUN));
% nstep = 10000;
% dr = 1/nstep*(rin - rout);
% 
% x = 1:-1/nstep:0;
% dx = 1/nstep;
% r = rin+(rout-rin)*x;
% 
% constant = prop.mu_bc/sqrt(2*prop.mu_bc/par.conv3)*(rout-rin);
% v = r;
% e = r;
% rot = r;
% for i = 1:length(v)
%     e(i) = PotentialEnergySurface([100, r(i),100]);
%     rot(i) =  J*(J+1)*prop.hbar^2/2/prop.mu_bc/r(i)^2*par.conv1;
% end
% feval = constant./sqrt(RoVibLevels(V,J,prop) - e - rot);
% 
% drint = zeros(1,length(v));
% t = drint;
% for i = 2:length(drint)
%     dx = abs(x(i)-x(i-1));
%     drint(i) = dx*(feval(i-1)+feval(i))/2;
%     t(i) = t(i-1)+drint(i);
% end
% plot(t,r);
%% with damping
plot([1:IRUN]*par.dt/autofs,R_stretch(1:IRUN)/autoang)
hold on

rin = min(R_stretch(1:IRUN));
rout = max(R_stretch(1:IRUN));
nstep = 2000;

p = 4; q = 4;
y = [[0:1/nstep:1],[1-1/nstep:-1/nstep:0]];
dy = 1/nstep;
x = y.^p./(y.^p+(1-y).^q);
r = rin+(rout-rin)*x;

constant = prop.mu_bc/sqrt(2*prop.mu_bc/par.conv3)*(rout-rin);
v = r;
e = r;
rot = r;
damp = r;
for i = 1:length(v)
    e(i) = PotentialEnergySurface([100, r(i),100]);
    rot(i) =  J*(J+1)*prop.hbar^2/2/prop.mu_bc/r(i)^2*par.conv1;
    damp(i) = (1-y(i))^(q-1)*y(i)^(p-1)*(p-p*y(i)+q*y(i))/((1-y(i))^q+y(i)^p)^2;
end
v = sqrt(2*prop.mu_bc*(RoVibLevels(V,J,prop) - e - rot));
v(nstep+1:end)= -v(nstep+1:end);

feval = constant./sqrt(RoVibLevels(V,J,prop) - e - rot).*damp;
feval(1) = 0;
drint = zeros(1,length(v));
t = drint;
for i = 2:length(drint)
    drint(i) = dy*(feval(i-1)+feval(i))/2;
    t(i) = t(i-1)+drint(i);
end
figure(2)
plotyy(t/autofs,r/autoang,t/autofs,v);
%,'DisplayName',['p=',num2str(p),' q=',num2str(q)]