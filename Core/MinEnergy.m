function Rmin = MinEnergy( prop,par )
%% Find the minimum energy point
MaxJlevel = prop.MaxJlevel;
Rmin = -1*ones(MaxJlevel+1,prop.ntmol);
for moltype=1:prop.ntmol
    switch moltype
        case 1
            mol = 2;
            mu  = prop.mu_bc;
        case 2
            mol = 3;
            mu  = prop.mu_ac;
        case 3
            mol = 1;
            mu  = prop.mu_ab;
    end
    for j = 0:MaxJlevel
        R_old     = 0.5*prop.Re(moltype);
        Rp        = 0.6*prop.Re(moltype);
        Error_old = Eprime(R_old,j,prop,par,mol,mu);
        Error_p   = Eprime(Rp   ,j,prop,par,mol,mu);
        for i = 1:100
            R_new     = Rp - Error_p*(Rp-R_old)/(Error_p-Error_old);
            Error_new = Eprime(R_new   ,j,prop,par,mol,mu);
            if abs(Error_new) <= par.tol2
                Rtest1 = R_new-0.005;
                Etest1 = Eprime(Rtest1,j,prop,par,mol,mu);
                
                Rtest2 = R_new+0.005;
                Etest2 = Eprime(Rtest2,j,prop,par,mol,mu);
                if Etest1 <0 && Etest2 >0 
                    Rp  = R_new;
                    break;
                else      %%shoot to infinity or maximum point
                    R_new = 0.2*prop.Re(moltype);
                    Error_new = Eprime(R_new   ,j,prop,par,mol,mu);
                end
            end
            if i == 100
                Rp = -1; %don't find satisfacotry result
                break;
            end
            R_old     = Rp;
            Error_old = Error_p;
            Rp        = R_new;
            Error_p   = Error_new;
        end
        Rmin(j+1,moltype) = Rp;
    end   
end

