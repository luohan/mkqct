function [out, error] = PostCollision(Y,out,prop, par, Caller)
% Post Collision Processing 
% Check for Reaction
% Reaction type:
%  - 1 No Reaction
%  - 2 A+BC = AB +C
%  - 3 A+BC = AC + B
%  - 4 A+BC = A + B + C

shell = par.rho;
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');  % No errors yet
React = struct('type', 5, 'mol'  ,  2, 'mu' , prop.mu_bc); % Default is no reaction, BC is stable molecule

Yp = zeros (1,12);
if sqrt(Y(1)^2 + Y(2)^2 + Y(3)^2) <= shell && sqrt(Y(4)^2 + Y(5)^2 + Y(6)^2) > shell
    
    Vr = Y(10:12)/prop.mu_a_bc;
    Test = Vr*Y(4:6)';
    if Test > 0
        React.type = 1; % No reaction
        Yp = Y;
        mu = prop.mu_a_bc;
    end
else % Check to see if products are AB+C 
    for i = 1:3
        Yp(i) = -prop.mc/(prop.mb+prop.mc) * Y(i) - Y(i+3);    %vector B->A
        Yp(i+3)= (prop.mb)*(prop.M)/(prop.ma+prop.mb)/(prop.mb+prop.mc)*Y(i)-prop.ma/(prop.ma+prop.mb)*Y(i+3); %vector mass center of (A,B)->C
        Yp(6+i) = - prop.ma/(prop.ma+prop.mb)*Y(6+i) - prop.mb*prop.M/(prop.ma+prop.mb)/(prop.mb+prop.mc)*Y(9+i);
        Yp(9+i) = Y(6+i) -prop.mc/(prop.mb+prop.mc)* Y(9+i);
    end
    
    Vr = Yp(10:12)/prop.mu_c_ab;
    Test = Vr*Yp(4:6)';
    
    if sqrt(Yp(1)^2 + Yp(2)^2 + Yp(3)^2) <= shell && sqrt(Yp(4)^2 + Yp(5)^2 + Yp(6)^2) > shell && Test > 0
        React.type = 2; % AB+C happened
        React.mu   = prop.mu_ab;
        React.mol  = 1;
        mu = prop.mu_c_ab;
        
    else % Check to see if products are AC+B
        for i = 1: 3
            Yp(i) = prop.mb/(prop.mb+prop.mc) *Y(i) - Y(i+3);
            Yp(i+3) = -prop.mc*(prop.M)/(prop.ma+prop.mc)/(prop.mb+prop.mc) * Y(i) - prop.ma/(prop.ma+prop.mc) * Y(i+3);
            Yp(6+i) = prop.ma/(prop.ma+prop.mc)*Y(6+i) - prop.mc*prop.M/(prop.ma+prop.mc)/(prop.mb+prop.mc)*Y(9+i);
            Yp(9+i) = -Y(6+i) -prop.mb/(prop.mb+prop.mc)* Y(9+i);
        end
        
        Vr = Yp(10:12)/prop.mu_b_ac;
        Test = Vr*Yp(4:6)';
    
        if sqrt(Yp(1)^2 + Yp(2)^2 + Yp(3)^2) <= shell && sqrt(Yp(4)^2 + Yp(5)^2 + Yp(6)^2) > shell && Test > 0
            React.type = 3; % AC+B happened
            React.mu   = prop.mu_ac;
            React.mol  = 3;
            mu = prop.mu_b_ac;
        
        else
     
            if sqrt(Y(1)^2 + Y(2)^2 + Y(3)^2) > 2*shell && sqrt(Y(4)^2 + Y(5)^2 + Y(6)^2) > shell
            React.type = 4; % A+B+C happened
            else
                React.type = 5; % Reaction did not finish
            end
        end
    end
end

if Caller == 1 % Script has been called during Integration and it doesn't need anything else 
    out.React = React.type;
    return
elseif Caller ~= 2 % Script has been called with a bad input
    error.code = 40; % Lets freak out.
    error.where = 'Post Collision';
    error.what ='Bad Caller';
    return
end

out.React=React.type;



%% Post collision relative velocity

    % Lets compute final relative velocity and scattering angle
    
    if React.type < 4 % We have a stable molecule
        direc = cross([0 0 1],Yp(10:12));
        if direc(1) < 0
            out.Chi= acos( Yp(12)/sqrt(sum(Yp(10:12).^2))); 
        else
            out.Chi = -acos( Yp(12)/sqrt(sum(Yp(10:12).^2))); 
        end
        out.Vr  = sqrt(Vr*Vr');
        [out.JLevel, out.VLevel, error] = QuantLevelDeconstruct (Yp, prop, par, React);
                
        if error.code ==55% This is actually not a stable molecule
            out.React = 4;
            [out.Chi, out.Vr, out.JLevel, out.VLevel] = deal (0); % Hopefully this won't happen often. I need to improve upon this later. 
     
            error.code = 41; % Lets freak out.
            error.where = 'Post Collision';
            error.what ='Reaction 4 occured. QCT can''t fully handle this yet';
            return
        end
        
    elseif React.type == 4 % We have a full dissociation
        [out.Chi, out.Vr, out.JLevel, out.VLevel] = deal (0); % Hopefully this won't happen often. I need to improve upon this later. 
     
        error.code = 42; % Lets freak out.
        error.where = 'Post Collision';
        error.what ='Reaction 4 occured. QCT can''t handle this yet';
        return
    else 
        [out.Chi, out.Vr, out.JLevel, out.VLevel] = deal (0); % Hopefully this won't happen often. I need to improve upon this later. 
     
        
        error.code = 43; % Lets freak out.
        error.where = 'Post Collision';
        error.what ='QCT can''t figure out what reaction occured';
        return
        
    end
        

return




