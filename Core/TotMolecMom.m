function [PP,Rp, error] = TotMolecMom (prop,par,vin,jin)
%% This function calculates initial molecular momentum and stretch based on
% v and j levels for bc molecule

% Function test parameters
% clear; clc;
% v = 1;
% j = 500;
% prop=MolecularProp();
v = double(vin);
j = double(jin);
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');

% Calculate energy of the current ro-vib level
[E1, error] = RoVibLevels(v,j,prop) ;  
    
if error.code ~= 0
    PP = 0;
    Rp = 0;
    return
end

if E1 > prop.RVmaxBC(j+1)
    PP = 0;
    Rp = 0;
    error.code = 12;
    error.where = 'ToTMolecMom';
    error.what  = 'No bound or qusi-bound state, atoms depart';
    return
end

%Use the Newton's Method to find the Max Turning point for this level
Rp = prop.Rmin(j+1,1)+0.1;  %calibrate 0.1 to get desired maximum stretch
w = 1;
for i = 1: 1000
    Error = E2 (Rp,j,prop,par)-E1;
    if abs(Error) <= par.tol2
        if Eprime(Rp,j,prop,par,2,prop.mu_bc) >0
            error.code = 0; % Everything is good to go
            break
        else             %converge to outer point or minimum
            Rp = prop.Rmin(j+1,1)+0.05*w;
            Error = E2 (Rp,j,prop,par)-E1;
            w=w+1;
        end
    elseif i == 1000
       error.code = 11; % Did not converge
       error.where = 'TotMolecMom';
       error.what = 'Did not converge';
    end
    Rp = Rp - Error/Eprime(Rp,j,prop,par,2,prop.mu_bc);
end

% Use secant method to find the Max turning point for this level
% R_old     = prop.Rmin(j+1,1)+0.01;
% Rp        = R_old+0.005;
% Error_old = E2(R_old,j,prop,par) - E1;
% Error_p   = E2(Rp   ,j,prop,par) - E1;
% for i = 1:1000
%     R_new     = Rp - Error_p*(Rp-R_old)/(Error_p-Error_old);
%     Error_new = E2(R_new   ,j,prop,par) - E1;
%     if abs(Error_new) <= par.tol2        
%        if Eprime(R_new,j,prop,par,2,prop.mu_bc) >0
%             Rp  = R_new;
%             break;
%         else      %%shoot to outer bound or minimum stretch
%             R_new = prop.Rmin(j+1,1)+0.01;
%             Error_new = E2(R_new   ,j,prop,par) - E1;
%         end
%     elseif i == 1000
%         error.code = 11; % Did not converge
%         error.where = 'TotMolecMom';
%         error.what = 'Did not converge';
%     end
%     R_old     = Rp;
%     Error_old = Error_p;
%     Rp        = R_new;
%     Error_p   = Error_new;
% end
% Final momentum can be calculated as

PP = sqrt(j*(j+1))*prop.hbar/Rp*par.conv2;

            
return
end
% Classical Vib+Ridgid Rotor Energy
function [E] = E2 (Rp,j,prop,par)
[pot,~] = PotentialEnergySurface([prop.Rinf, Rp, prop.Rinf]); 
E = j*(j+1)*prop.hbar^2/2/prop.mu_bc/Rp^2*par.conv1+ pot;
return
end