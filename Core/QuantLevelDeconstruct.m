function [JLevel, VLevel, error] = QuantLevelDeconstruct (Y0, prop, par, React)

%% This function computes rotational and vibrational levels of the molecules.
% Input:
% Y -> Q and Mom of the molecule
% First 3 are spatial coord of molecule, next 3 are spatial coord of atom.

% Initialize Variables
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');  % No errors yet
mol  = React.mol;                                                  % molecule id, index for Derivativs
moltype = prop.moltype(mol);                                       % molecule type
Vmax = prop.Vmax(moltype);                                         % Max # of V levels
mu   = React.mu;


[JLevel, VLevel, Rmax] = deal (0);
%[Jsqrd, R, Pv, Ev, Er, Ep] = deal (zeros(Imax,1));

%TE2= zeros(Imax,1); % Total energy vector

[~, VV] = Derivatives_Molec(Y0,prop,par,mol,mu); %Calculate energy
TE0 = 1/2/mu * (Y0(7:9)*Y0(7:9)')+VV;


%% Calculate J and V levels

% Calculate JLevel from molecular angular momentum

% Calculate Molecular Momentum

Jx = Y0(2).*Y0(9) - Y0(3).*Y0(8);
Jy = Y0(3).*Y0(7) - Y0(1).*Y0(9);
Jz = Y0(1).*Y0(8) - Y0(2).*Y0(7);
Jsqrd = (Jx.^2+ Jy.^2+ Jz.^2);

JLevel = -1/2+sqrt(1/4+Jsqrd/ prop.hbar^2/par.conv2^2);

if JLevel >prop.Jmax(1,moltype)
    error.code = 57; % Did not converge
    error.where = 'QuantLevelDeconstruct';
    error.what = ' Final Rot level is too high';
    return
end

E2 = TE0*par.conv3;

% Fit the total molecular energy to ro-vib levels
v = 0; v_old = 0; Error_old = 0;
for i = 1: 100;
    
    if i == 1  % Innitialized the Secant method
        v = 2; % Starting guess
        [Energy, ~]=RoVibLevels(v,JLevel,prop,moltype);
        Error_old = E2 - Energy;
        v_old = v;
        v=3; % Next Guess
    end
    [Energy, ~]=RoVibLevels(v,JLevel,prop,moltype);
    Error = E2 - Energy;
    if Error-Error_old == 0
        v=v_old+1e-10;
        [Energy, ~]=RoVibLevels(v,JLevel,prop,moltype);
        Error = E2 - Energy;
    end
                
    v_new = v - Error * ( v-v_old)/(Error-Error_old);
    v_old = v;
    v = v_new;
    Error_old = Error;
    
    % Conditions to modify or stop itteration
    if  isnan(v)  % Can't find v
        error.code = 53; % Did not converge
        error.where = 'QuantLevelDeconstruct';
        error.what = ' V level did not converge or it crashed';
        break
    elseif abs(v) > Vmax; % v level is way too high.
        for v = Vmax-1:-1:0
            if v ~= v_old
                break
            end
        end
    elseif v <= -1.0 % Lets try to avoid negative vib levels
        if v_old ~= -1*v;  % don't jump back to the old value, it will give a NAN
            v = -v;
        else
            v = 0.5*v_old;
        end
    elseif JLevel > 1.5*prop.Jmax(floor(abs(v))+1,moltype) || prop.Jmax(floor(abs(v))+1,moltype) == -1 % This uses some fluffyness. If the selected v is too high then this failed.
        v = 2;
        
    end
    
    if abs(Error/E2) <= par.tol2
        if abs(v) <= Vmax % Make sure the code doesn't try to use a vector that is too large
            if v > -1 && (JLevel- prop.Jmax(floor(abs(v))+1,moltype))<3; %
                break
            end
        end
        
    elseif i == 100
        error.code = 54; % Did not converge
        error.where = 'QuantLevelDeconstruct';
        error.what = ' V level did not converge';
        break
    end
    
    
end


VLevel = v; %Save our values

if VLevel <-1
    error.code = 56; % Did not converge
    error.where = 'QuantLevelDeconstruct';
    error.what = ' V converged to negative value';
end

%% Integrate molecule for 1 vibrational period
% There may be times when the molecule does not breakup right away.

if error.code == 53 || error.code == 54 || error.code == 56 % Lets check if the formed molecule is even stable
    
    Imax   = floor(par.tmax/par.dt/10); % Max number of timesteps
    Y = zeros(1,6);
    Y(1:3) = Y0(1:3); % First timestep
    Y(4:6) = Y0(7:9);
    [R] = deal (zeros(Imax,1));
    dt = par.dt;
    time = 0;
    [switch1, switch2] = deal(0);
    
    
    % RK constants
    Const1 = [1/6];
    Const2 = [4/75         16/75];
    Const3 = [5/6          -8/3        5/2];
    Const4 = [-165/64      55/6        -425/64         85/96];
    Const5 = [12/5         -8          4015/612        -11/36      88/255];
    Const6 = [-8263/15000  124/75      -643/680        -81/250     2484/10625];
    Const7 = [3501/1720    -300/43     297275/52632    -319/2322   24068/84065 3850/26703];
    Const8 = [-1/160       -125/17952  1/144           -12/1955    -3/44       125/11592   43/616];
    Const9 = [3/40         875/2244    23/72           264/1955    125/11592   43/616];
    
    for i = 1:Imax-1
        
        
        %      % Standard RK steps
        %      [dY, V] = QCTMolec(Y,prop);
        %      k1 = dt*dY;
        %
        %      [dY, ~] = QCTMolec(Y+k1/2,prop);
        %      k2 = dt*dY;
        %
        %      [dY, ~] = QCTMolec(Y+k2/2,prop);
        %      k3 = dt*dY;
        %
        %      [dY, ~] = QCTMolec(Y+k3,prop);
        %      k4 = dt*dY;
        %
        %      Y = Y + 1/6*(k1+2*k2+2*k3+k4);
        
        for j = 1: par.InnerLoops % Itterate until timesteps converged
            
            %         dtt(i) = dt; % For debugging
            
            % Standard RK5-6 steps
            [dY, V] = Derivatives_Molec(Y                                                                                                   ,prop, par, mol, mu);
            k1 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const1(1) *k1                                                                                    ,prop, par, mol, mu);
            k2 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const2(1) *k1  +Const2(2) *k2                                                                    ,prop, par, mol, mu);
            k3 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const3(1) *k1  +Const3(2) *k2  +Const3(3) *k3                                                    ,prop, par, mol, mu);
            k4 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const4(1) *k1  +Const4(2) *k2  +Const4(3) *k3  +Const4(4) *k4                                    ,prop, par, mol, mu);
            k5 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const5(1) *k1  +Const5(2) *k2  +Const5(3) *k3  +Const5(4) *k4  +Const5(5) *k5                    ,prop, par, mol, mu);
            k6 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const6(1) *k1  +Const6(2) *k2  +Const6(3) *k3  +Const6(4) *k4  +Const6(5) *k5                    ,prop, par, mol, mu);
            k7 = dt*dY;
            
            [dY, ~] = Derivatives_Molec(Y +Const7(1) *k1  +Const7(2) *k2  +Const7(3) *k3  +Const7(4) *k4  +Const7(5) *k5 +Const7(6) *k7     ,prop, par, mol, mu);
            k8 = dt*dY;
            
            %Calculate difference in solutions
            eps = (Const8(1)*k1 +Const8(2)*k3 +Const8(3)*k4 +Const8(4)*k5 +Const8(5)*k6 +Const8(6)*k7 +Const8(7)*k8)/dt;
            Ytest= Y +Const9(1)*k1 +Const9(2)*k3 +Const9(3)*k4 +Const9(4)*k5 +Const9(5)*k7 +Const9(6)*k8;
            
            % Determine if error in position or momentum is larger
            eps1 = sqrt(sum(eps(1:3).^2)/ sum(Ytest(1:3).^2));
            eps2 = sqrt(sum(eps(4:6).^2)/ sum(Ytest(4:6).^2));
            eps3 = max(eps1,eps2);
            
            % Calculate next timestep
            qq = (par.tol/2/eps3)^(1/5);
            
            
            if eps3<par.tol
                %Y= Y + 16/135*k1 + 6656/12825*k3 + 28561/56430*k4 - 9/50*k5 + 2/55*k6; % Advance the time step using 5th order rk
                Y= Y +Const9(1)*k1 +Const9(2)*k3 +Const9(3)*k4 +Const9(4)*k5 +Const9(5)*k7 +Const9(6)*k8; % Advance the time step using 6th order rk
                time = time + dt;
                if qq > par.MaxdtJump % Don't allow large jumps
                    qq = par.MaxdtJump;
                end
                
                dt = dt*qq;
                break
            elseif j < par.InnerLoops
                dt = dt*qq;
            else
                error.code = 50; % Not enough inner loops.
                error.where = 'Integration';
                error.what = 'Not enough Inner Loops';
                return
            end
            
            
        end
        %time(i+1) = time(i)+dt;
        %DTT(i) = dt;
        
        
        %      %Energy at the current time step
        %     [~, VV] = QCTMolec(Y,prop);
        %     for j = 1:3
        %        TE2(i) = TE2(i) + 1/2/prop.mu_bc * Y(j+3)^2; % Molecular Energy
        %     end
        %     TE2(i) = TE2(i) + VV;
        
        % Calculate Molecular Momentum
        %     Jx = Y(2).*Y(3+3) - Y(3).*Y(3+2);
        %     Jy = Y(3).*Y(3+1) - Y(1).*Y(3+3);
        %     Jz = Y(1).*Y(3+2) - Y(2).*Y(3+1);
        %     Jsqrd(i) = (Jx.^2+ Jy.^2+ Jz.^2);
        
        % Calculate Internal Momentum and Energies
        R(i) = sqrt(Y(1:3)*Y(1:3)'); % Molecular Sep
        %     Pv(i) = (Y(4:6)*Y(1:3)') / sqrt((Y(1:3)*Y(1:3)')); % Vib MOM
        %     Ev(i) = 0.5/prop.mu_bc*Pv(i)^2; % Vib Ke
        %     Er(i) = Jsqrd(i)/2/prop.mu_bc/R(i)^2; % Rot Eng.
        %     Ep(i) = prop.Dm(moltype)(1)/par.conv3*(1-exp(-prop.beta(moltype)*(R(i)-prop.Re)))^2; % Pot Eng.
        %     RovibPot(i) = JLevel*(JLevel+1)*prop.hbar^2/2/prop.mu_bc/R(i)^2*par.conv1+ prop.Dm(moltype)(1)*(1-exp(-prop.beta(moltype)*(R(i)-prop.Re)))^2;
        
        % Find the max molecular streach and 1 vibrational period
        if i > 2
            if R(i) < R(i-1) && R(i-1)> R(i-2) % First Max point
                if switch1 == 0
                    Rmax = R(i-1);
                    %                count1 = i-1;
                    switch1= 1;
                    %                timestart=time; %Should be using last time instead of current but eh...
                elseif abs(R(i-1)-Rmax)<par.tol0 % And then find the end of that vib. period
                    %                count2 = i-1;
                    %                timeend=time;
                    break
                    
                end
            end
        end
        
        if R(i)>par.rho
            error.code = 55; % This is actually not a stable molecule
            error.what = 'Unstable molecule formed';
            return
        end
        
        
        
        %     % Average out vib, rot and pot energies
        %     if switch2 == 0 && switch1 == 1;
        %         EvTOT = EvTOT + Ev(i)*dt; % Note, these will need to change if variable time stepping is used
        %         ErTOT = ErTOT + Er(i)*dt;
        %         EpTOT = EpTOT + Ep(i)*dt;
        %     end
        
        if i == Imax-1
            error.code = 51; % Did not converge
            error.where = 'QuantLevelDeconstruct';
            error.what = ' Did not find a full vibrational period';
            %return
        end
        
    end
    
    [~, VV] = Derivatives_Molec(Y,prop,par,mol,mu);
    TE2 = 1/2/mu * (Y(4:6)*Y(4:6)')+VV;
    
    if abs(TE2-TE0)/TE0>par.Etol
        error.code = 52; % Did not converge
        error.where = 'QuantLevelDeconstruct';
        error.what = ' Energy not conserved in integration';
    end
    
    
    
    
end


%% Use this to plot
% icount = linspace(0,1, count2-count1+1);
% icount = time(count1:count2);
% figure(5)
% plot(icount, Ev(count1:count2),'b-' ,icount, Ep(count1:count2), 'g-', icount, Er(count1:count2), 'r-', icount, Ev(count1:count2)+Ep(count1:count2),'c-', icount,TE2(count1:count2),'m', icount,EvAVG*ones(1,length(icount)),'b--',icount,EpAVG*ones(1,length(icount)),'g--',icount,ErAVG*ones(1,length(icount)),'r--', 'LineWidth', 2 )
% ylabel ('Energy')
% legend ('Ev', 'Ep', 'Er', 'Ev+Ep', 'Etot', 'Ev AVG', 'Ep AVG', 'Er AVG')
%
% figure(6)
% plot(icount,R(count1:count2))
%
% figure(7)
% plot(DTT(1:i))
%
% figure(8)
% plot((TE2(1:i)-TE2(1))/TE2(1))


end



