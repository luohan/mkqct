function CompileQCT()
%% Compile QCT code to MEX
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
V=1; J=1;
[RR, PP, IRUN, R_stretch, Pv_stretch,~] = RunVibPeriod(V,J,prop,par); % Calculate vib period
prop.IRUN = IRUN;
prop.R_stretch= R_stretch;
prop.Pv_stretch = Pv_stretch;
prop.RR = RR;
prop.PP = PP;
if exist('CComp','dir') == 7 % Lets clean up our files first
    rmdir('CComp', 's')
end

if exist('CComp2','dir') == 7 % Lets clean up our files first
    rmdir('CComp2', 's')
end

if  exist('Backbone_mex','file') == 3
    delete('Backbone_mex.mexa64')
end

if  exist('RunVibPeriod_mex','file') == 3
    delete('RunVibPeriod_mex.mexa64')
end
codegen -d ./Core/CComp Backbone -args {0 0 0 0 0 0 0 0 coder.typeof(prop) coder.typeof(par) coder.typeof(par.filename1) coder.typeof(par.filename2)}
codegen -d ./Core/CComp2 RunVibPeriod -args {0 0 coder.typeof(prop) coder.typeof(par)}
codegen -d ./Core/CComp BackboneTrj -args {0 0 0 0 0 0 0 0 coder.typeof(prop) coder.typeof(par)}
