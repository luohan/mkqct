function prime =Eprime(R,j,prop,par,mol,mu)
%% calculated derivative d(E_rot+E_pot)/dR(mol)
Rin = [prop.Rinf prop.Rinf prop.Rinf];
Rin(mol)=R;
[~,p] = PotentialEnergySurface(Rin);
prime = -j*(j+1)*prop.hbar^2/mu/R^3*par.conv1+ p(mol);
return
end