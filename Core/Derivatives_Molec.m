function [dY_dt, V] = Derivatives_Molec(Y, prop, par,mol,mu)
% This function calls the PES for a molecule and performs a chain rule to
% the integration coordinates. 
% mol is the molecule ID
if nargin<4
    mol = 2;
    mu  = prop.mu_bc;
end
% if mol = 1  AB is bonded Y(1:3) = A->B   Y(4:6) = conjugate momentum of AB  
% if mol = 2  BC is bonded Y(1:3) = B->C   Y(4:6) = conjugate momentum of BC  
% if mol = 3  CA is bonded Y(1:3) = A->C   Y(4:6) = conjugate momentum of AC
%% Calculate R coordinate

%Rlarge = 50; % Define Rinf value for the 3rd atom
R = sqrt(Y(1)*Y(1)+Y(2)*Y(2)+Y(3)*Y(3)); % Calc R for the molecule

% Derivatives to use in chain rule
dR_dQ = zeros (1,3);
Denom = 1/R;
dR_dQ(1) = Y(1)*Denom;
dR_dQ(2) = Y(2)*Denom;
dR_dQ(3) = Y(3)*Denom;

%% Call PES
Rin = ones(1,3)*prop.Rinf;
Rin(mol)=R;
[V, dV] = PotentialEnergySurface(Rin);
V = V/par.conv3; % Convert from eV to proper energy units
dV = dV/par.conv3;

%% Governing ODEs
dY_dt = zeros(1,6);
dY_dt(4:6) = -dR_dQ * dV(mol);
dY_dt(1:3) = [  Y(4); Y(5); Y(6) ]/mu;

return
