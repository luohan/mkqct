function [Y,out,error] = IntegrationTrj (Y0, out, prop, par)

% This function uses 5-6th order RK to calculate molecular trajectories. The
% script also checks to make sure that 1) total molecular energies are
% conserved and 2) trajectories are reversible (in the future)

% Inputs:
% YO -> Vector that provides initial position and momentum in generalized coordinates
% prop-> structured variable that has molecular properties
% par -> structured variable that contains numerical parameters

% Outputs:
% t -> time vector
% Y -> vector of molecular positions and momentum at each time step
% error -> structured variable describing encountered errors.

% Subscript:
% f -> forward
% r -> reverse

% Numerical Parameters
%  Etol   = 0.01; % Energy conservation tolerance
%  Ptol = 0.01; % Position tolerance for reversibility  (% of initial molecular spacing)
%  tmax   = 0.1e3; %fs
%  dt     = 1.0e-2; %fs



% tic

%% Innitialize the integration

error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
Imax   = floor(par.tmax/par.dt);
Y = Y0; % Initial conditions
[k, time, InteractStart, InteractEnd,TE] = deal (0);  % Initialize other random variables
R = zeros(1,3); %molecule distance
q = zeros(9,1);
qtest = zeros(11,Imax);


% For debugging use the following
%qtest = zeros(9,ceil(Imax/par.ots));
%TT = zeros(9,ceil(Imax/par.ots));
%DTT = zeros(9,ceil(100000));

% Compute the initial energy of the system.
[~, V0] = Derivatives_System(Y,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
TE0 = 1/2/prop.mu_bc *Y(7:9)*Y(7:9)'+1/2/prop.mu_a_bc *Y(10:12)*Y(10:12)' + V0;


% RK constants
Const1 = [1/6];
Const2 = [4/75         16/75];
Const3 = [5/6          -8/3        5/2];
Const4 = [-165/64      55/6        -425/64         85/96];
Const5 = [12/5         -8          4015/612        -11/36      88/255];
Const6 = [-8263/15000  124/75      -643/680        -81/250     2484/10625];
Const7 = [3501/1720    -300/43     297275/52632    -319/2322   24068/84065 3850/26703];
Const8 = [-1/160       -125/17952  1/144           -12/1955    -3/44       125/11592   43/616];
Const9 = [3/40         875/2244    23/72           264/1955    125/11592   43/616];



%% Forward Integration

dt = par.dt;
for i = 1:Imax-1
    % MAIN INTEGRATION SECTION
    %----------------------------------- save trjs -------------------------------------
    % A = [zeros(3,3), eye(3,3)*(prop.mb+prop.mc)/prop.M, eye(3,3); ...
    % -eye(3,3)*(prop.mc/(prop.mb+prop.mc)), -eye(3,3)*(prop.ma/prop.M),eye(3,3);...
    % eye(3,3)*(prop.mb/(prop.mb+prop.mc)), -eye(3,3)*(prop.ma/prop.M),eye(3,3)];
    %
    % B =  [zeros(3,3), -eye(3,3), eye(3,3); ...
    % eye(3,3), -eye(3,3)*(prop.mb/(prop.mb+prop.mc)), -eye(3,3)*(prop.mc/(prop.mb+prop.mc));...
    % eye(3,3)*(prop.ma/(prop.M)), eye(3,3)*(prop.mb/prop.M),eye(3,3)*(prop.mc/prop.M)];
    %
    % Z = A(:,1:6)*Y : convert back to cartesian
    % Y = B*Z;         convert to harmilton
    qtest(:,i) = [Y(1:6)';R';0; time];

    
    for j = 1: par.InnerLoops % Itterate until timesteps converged
        
        %         dtt(i) = dt; % For debugging
        
        
        % Standard RK5-6 steps
        [dY, V , R] = Derivatives_System(Y                                                                                                   ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k1 = dt*dY;
        qtest(7:10,i) = [R';V];
        
        [dY, ~] = Derivatives_System(Y +Const1(1) *k1                                                                                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k2 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const2(1) *k1  +Const2(2) *k2                                                                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k3 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const3(1) *k1  +Const3(2) *k2  +Const3(3) *k3                                                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k4 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const4(1) *k1  +Const4(2) *k2  +Const4(3) *k3  +Const4(4) *k4                                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k5 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const5(1) *k1  +Const5(2) *k2  +Const5(3) *k3  +Const5(4) *k4  +Const5(5) *k5                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k6 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const6(1) *k1  +Const6(2) *k2  +Const6(3) *k3  +Const6(4) *k4  +Const6(5) *k5                    ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k7 = dt*dY;
        
        [dY, ~] = Derivatives_System(Y +Const7(1) *k1  +Const7(2) *k2  +Const7(3) *k3  +Const7(4) *k4  +Const7(5) *k5 +Const7(6) *k7     ,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        k8 = dt*dY;
        
        %Calculate difference in solutions
        eps = (Const8(1)*k1 +Const8(2)*k3 +Const8(3)*k4 +Const8(4)*k5 +Const8(5)*k6 +Const8(6)*k7 +Const8(7)*k8)/dt;
        Ytest= Y +Const9(1)*k1 +Const9(2)*k3 +Const9(3)*k4 +Const9(4)*k5 +Const9(5)*k7 +Const9(6)*k8;
        
        % Determine if error in position or momentum is larger
        eps1 = sqrt(sum(eps(1:6).^2)/ sum(Ytest(1:6).^2));
        eps2 = sqrt(sum(eps(7:12).^2)/ sum(Ytest(7:12).^2));
        eps3 = max(eps1,eps2);
        
        % Calculate next timestep
        qq = (par.tol/2/eps3)^(1/5);
        
        if eps3<par.tol
            %Y= Y + 16/135*k1 + 6656/12825*k3 + 28561/56430*k4 - 9/50*k5 + 2/55*k6; % Advance the time step using 5th order rk
            Y= Y +Const9(1)*k1 +Const9(2)*k3 +Const9(3)*k4 +Const9(4)*k5 +Const9(5)*k7 +Const9(6)*k8; % Advance the time step using 6th order rk
            time = time + dt;
            if qq > par.MaxdtJump % Don't allow large jumps
                qq = par.MaxdtJump;
            end
            
            dt = dt*qq;
            break
        elseif j < par.InnerLoops
            dt = dt*qq;
        else
            error.code = 30; % Not enough inner loops.
            error.where = 'Integration';
            error.what = 'Not enough Inner Loops';
            return
        end
    end
    
    %    DTT(i) = dt;
    
    
    % INTERMEDIATE CALCULATION SECTION
    if mod(i-1,par.ots) == 0 % How often should we actually analize the data?
        
        
        %Energy at the current time step
        [~, V] = Derivatives_System(Y,prop.mb_mbmc, prop.mc_mbmc, prop.mu_bc, prop.mu_a_bc, par.conv3);
        TE = 1/2/prop.mu_bc *Y(7:9)*Y(7:9)'+1/2/prop.mu_a_bc *Y(10:12)*Y(10:12)' + V;
        
        
        % Check to see if we are finished
        if abs(1-TE/TE0)*100 >par.Etol % Check energy conservation
            error.code = 31; % Energy is not conserved.
            error.where = 'Integration';
            error.what = 'Energy is not conserved';
            return
        end
        
        %Find Interaction Duration
        if V < 1.95*prop.Dm(1)/par.conv3 && InteractStart == 0
            InteractStart=time;
        elseif InteractStart>0 &&  V < 1.95*prop.Dm(1)/par.conv3
            InteractEnd = time;
        end
        
        % CHECK TO SEE IF WE ARE FINISHED
        Case = PostCollision(Y,out,prop,par,1); % Check if the particles left the reacting shell
        
        if Case.React ~= 5 % Case 5 == Reaction did not finish
            break
        end
        
    end
end

out.Trj = qtest(:,1:i);
out.TrjLen = i;


% Check the final step before quiting again
Case = PostCollision(Y,out,prop,par,1); % Check if the particles left the reacting shell
if Case.React == 5
    error.code = 32; % Molecule did not leave collision shell after integration failed
    error.where = 'Integration';
    error.what = 'Molecule did not leave collision shell within Imax';
    return
end

out.Interaction = InteractEnd-InteractStart;
out.EtolInteg = abs(1-TE/TE0)*100;

return % It's closing time. You don't have to go home but you can't stay here.
