function [output, EC] = CrossSection_Rate_Calc_Ecol_Standby(input, prop, par)
%% Monte Carlo Quasi-Classical Trajectories
% This script runs MC on QCT to calculate State to State Cross Sections or
% tates

% Input Variables:
% B  - Impact parameter (A)
% Vr - Relative velocity (A/fs=1e-10/()

% J  - Rotational level
% V  - Vibrational level

% Calculation Variables
% Th,Phi - Molecular orientation angles
% Eta - Rotational momentum angle

%clear; clc; close all; % Nothing else on the screen is as important as this so I took the liberty of closing it.

%% Flags
WorkersFlag = 0; % Should the code hire and then fire workers or just keep the current workforce. The current workforce is hired by the Sweep_Something function
SaveData = input.SaveData;  % Should the code save intermediate data

%% Inputs

% Use these for debugging
% Bmax = 2; % A
% Vrel =0.07;  % A/fs
%  V=10;       % v'
%  J=11;        % J'
% Calc = 4; % Type 1 is X-sect calc, Type 2 is state specific rate calc.
% Ttrans = 500;
% Ncalls = 10000;
% InnerLoops = 1;
%  prop = MolecularProp();          % grab needed molecular properties
%  par = NumericalPar();            % also look up numerical parameters
% [Jmax] = MaxLevels(prop,par);    % and max ro-vibrational levels
% prop.Jmax = Jmax;
%  Vmax = 10;
%  Jmax = 10;

% Use these to run
Bmax = input.Bmax; % A
Vrel =input.Vrel;  % A/fs
V=input.V;       % v'
J=input.J;        % J'
Calc = input.Calc; % Type 1 is X-sect calc, Type 2 is state specific rate calc., Type 3 is monoquantum Deex
Ttrans = input.Ttrans;
InnerLoops=input.InnerLoops;    % Number of Innerloops
Ncalls = input.Ncalls;  % Total number of samples

%% Trajectory Save Prepare
if (SaveData)
    % open file
    if (Calc == 2)    % Rate calculation
        h5name = strcat(input.TrjLoc,'/T=',num2str(input.Ttrans),'.h5');
    elseif (Calc == 1) %Xsec calculation
        h5name = strcat(input.TrjLoc,'/Ecol=',num2str(input.Ecol),'.h5');
    end
    if (exist(h5name,'file') == 0)
        h5fid = H5F.create(h5name);
    else
        h5fid = H5F.open(h5name,'H5F_ACC_RDWR','H5P_DEFAULT');
    end
    %create group
    Vlink = strcat('/V=',num2str(input.V));
    if H5L.exists(h5fid,Vlink,'H5P_DEFAULT')
        gid=H5G.open(h5fid,Vlink,'H5P_DEFAULT');
    else
        gid=H5G.create(h5fid,Vlink,'H5P_DEFAULT','H5P_DEFAULT','H5P_DEFAULT');
    end
    % create dataset
    h5dataset=strcat(Vlink,'/J=',num2str(input.J));
    create = false; 
    h5col = 0;
    if H5L.exists(h5fid,h5dataset,'H5P_DEFAULT')
        did = H5D.open(gid,['J=',num2str(input.J)]);
        st = H5D.get_space_status(did);
        if st == 0
            create = true;
            H5L.delete(h5fid,h5dataset,'H5P_DEFAULT')
        else
            sp = H5D.get_space(did);
            [~,temp]=H5S.get_simple_extent_dims(sp);
            h5col = temp(1);
        end
        H5D.close(did);
    else
        create = true;
    end
    H5G.close(gid);
    H5F.close(h5fid);
    
    if create
      h5create(h5name,h5dataset,[14 Inf],'Datatype','double','ChunkSize',[14,100],'Deflate',9);
    end

    if Calc == 1
        TempSaveHead = [input.TrjLoc,'/Ecol=',num2str(input.Ecol),'V=',num2str(V),'J=',num2str(J),'_Temp_'];
    else
        TempSaveHead = [input.TrjLoc,'/T=',num2str(input.Ttrans),'V=',num2str(V),'J=',num2str(J),'_Temp_'];
    end
    TempSaveOne = [TempSaveHead,'1.mat'];
    if ~exist(TempSaveOne)
       copyfile(h5name,[h5name,'.bak']);
    end
    %in{B,Vr,Th,Phi,Psi,Eta},out{Vr,J,V,Chi,React,InteractionTime,EtolInteg,EtolTot}
end

%% Initialize Properties and Parameters
[RR, PP, IRUN, R_stretch, Pv_stretch,error] = RunVibPeriod_mex(V,J,prop,par); % Calculate vib period
if error.code ~=0
    output=0;
    EC.Count=Ncalls;
    EC.ListOfErrors = error;
    return
end
prop.IRUN = IRUN;
prop.R_stretch= R_stretch;
prop.Pv_stretch = Pv_stretch;
prop.RR = RR;
prop.PP = PP;

Jmax = ceil(prop.Jmax(1,1))+10; %Added a few extra levels for quantum to classical conversion errors
Vmax = prop.Vmax(1)+10;



%% Initialize the code

% Lets calculate how many workers we need to hire for this job
if WorkersFlag == 1 &&  matlabpool ('size') == 0
    matlabpool
end

%Clear the variables
ChiIntr = 180; %Number of intervals the scattering angle is broken into
%ColIntr = 100;  %Number of intervals sampling duration is broken into
ChiLvls = linspace(0,pi, ChiIntr);

[H11, H12, H13, H22, H23] = deal(0); % Innitialize collision integrals

RoVibLevel = zeros(4,Vmax,Jmax);
Chi = zeros(4,Vmax,Jmax,ChiIntr);
%Intr= zeros(4,Vmax,Jmax,ColIntr-1);
%IntrTest=zeros(1,ColIntr-1);
%ColLvls=linspace(0,1000,ColIntr);
B = Bmax*ones(Ncalls/InnerLoops,1); % Use constant B unless otherwise specified
Vr = Vrel*ones(Ncalls/InnerLoops,1); % Use constant relative velocity, unless this is a rate calculation
Th = zeros(Ncalls/InnerLoops,1);
Phi = zeros(Ncalls/InnerLoops,1);
Psi = zeros(Ncalls/InnerLoops,1);
Eta = zeros(Ncalls/InnerLoops,1);
gam = zeros(Ncalls/InnerLoops,1);

%Output vectors
out(Ncalls/InnerLoops) = struct( 'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0);
error(Ncalls/InnerLoops) = struct('code', 0, 'where', 0, 'what', 0);

%% Run Trajectories
ErrorCount = 0; % Count of number of failed calculations
ColReact = 0; % Number of collisions that should be skipped by Collision Integral Calc.
if (SaveData)
    if Calc == 1
        TempSaveHead = [input.TrjLoc,'/Ecol=',num2str(input.Ecol),'V=',num2str(V),'J=',num2str(J),'_Temp_'];
    else
        TempSaveHead = [input.TrjLoc,'/T=',num2str(input.Ttrans),'V=',num2str(V),'J=',num2str(J),'_Temp_'];
    end
end
for j = 1:InnerLoops % Sweep over Inner Loops. This is used to reduce required memory. Of course if there are too many inner loops then you'll lose parallel speedup
    if j == ceil(0.5*InnerLoops)
      copyfile(h5name,[h5name,'.half.bak']);
    end
    TempSave = [TempSaveHead,num2str(j),'.mat'];
    if SaveData
        if exist(TempSave,'file')
            load(TempSave,'ErrorCount','RoVibLevel','Chi','H11','H12','H13','H22','H23','ColReact','h5pos')
            if ErrorCount ~=0
                load(TempSave,'ListOfErrors')
            end
            h5col = h5pos;
            continue
        end
    end
    %% Generate random sample
    for i = 1:Ncalls/InnerLoops
        % We can use parfor loops here but based on matlab version each
        % thread may start with same seed. We can fix that but in
        % general this part is quick anyway.
        if Calc == 1 % Calc Normal Cross Sections, for fixed V, J, Vr
            %Generate Random Numbers
            B(i) = Bmax*sqrt(rand());
            Th(i) = acos(1-2*rand());
            Phi(i) = 2*pi*rand();
            Psi(i) = 2*pi*rand();
            Eta(i) = 2*pi*rand();
        elseif Calc == 2 % Calc Rates or Collision Integrals
            B(i) = Bmax*sqrt(rand());
            Th(i) = acos(1-2*rand());
            Phi(i) = 2*pi*rand();
            Psi(i) = 2*pi*rand();
            Eta(i) = 2*pi*rand();
            gam(i) = -log(rand()*rand());
            Vr(i) =  sqrt(2*prop.k*Ttrans*gam(i)/prop.mu_a_bc/par.conv4);
        elseif Calc == 3; % Calc QCT with const Bmax
            Th(i) = acos(1-2*rand());
            Phi(i) = 2*pi*rand();
            Psi(i) = 2*pi*rand();
            Eta(i) = 2*pi*rand();
        end
        
    end
    
    
    
    
    %% Run the QCT calculation
    parfor i = 1: Ncalls/InnerLoops
        [out(i), error(i)] = Backbone_mex(B(i),Vr(i),Th(i),Phi(i),Psi(i),Eta(i),J,V,prop,par,par.filename1,par.filename2);
        %             [out(i),error(i)] = TestChi(B(i),Vr(i),Th(i),Phi(i),Psi(i),Eta(i),J,V,prop,par,par.filename1,par.filename2);
    end
    
    %% Postprocess the QCT Calculation (In serial)
    for i = 1 : Ncalls/InnerLoops
        
        %%%%%%%%%%%%%%%%%%
        if error(i).code == 0 || error(i).code ==41 || error(i).code ==42 % Lets log the good calculations
            
            
            if out(i).VLevel <0  % This is were the round will fail.
                out(i).VLevel = 0;
            end
            
            % Sum up QCT results (Reaction type, Final Vib
            % Level, Final Rot Level)
            RoVibLevel(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1) = RoVibLevel(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1)+1; %Note, first index is 0th level
            
            if Calc == 1 % We care about individual cross sections
                % Lets bin the scattering Angles
                for jj = 1: ChiIntr-1
                    if out(i).Chi <= ChiLvls(jj+1)
                        Chi(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,jj)= Chi(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,jj)+1;
                        break
                    end
                end
                
            elseif Calc == 2 % We want rates and collision integrals
                if out(i).React ~= 4 % Don't sum up the Chi for RXNs
                    % This is a nifty way to avoid different
                    % collocation points for each collision
                    % integration
                    H11 = H11 + (1-cos(out(i).Chi)   )*gam(i)    ;
                    H12 = H12 + (1-cos(out(i).Chi)   )*gam(i)^2  ;
                    H13 = H13 + (1-cos(out(i).Chi)   )*gam(i)^3  ;
                    H22 = H22 + (1-cos(out(i).Chi)^2 )*gam(i)^2  ;
                    H23 = H23 + (1-cos(out(i).Chi)^2 )*gam(i)^3  ;
                else
                    ColReact = ColReact + 1;
                end
            end
            
            
            %                    % Lets bin Energy Errors  and collision durations.
            %                    This is actually computationally expensive so I
            %                    commented this out for now. The code is good though and it was used for an ASM paper
            
            %                    for jj = 1: length(EtolIntegLev)
            %                        if out(i).EtolInteg >= EtolIntegLev(jj)
            %                            EtolIntegBin(jj) = EtolIntegBin(jj)+1;
            %                            break
            %                        end
            %                    end
            
            %                    for jj = 1: length(EtolTotLev)
            %                        if out(i).EtolTot >= EtolTotLev(jj)
            %                            EtolTotBin(jj) = EtolTotBin(jj)+1;
            %                            break
            %                        end
            %                    end
            
            
            
            
            %                 % Lets bin the Collision times
            %                 for kk = 1: ColIntr-1
            %                     if out(i).Interaction <= ColLvls(kk+1)
            %                        Intr(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,kk)= Intr(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,kk)+1;
            %                        IntrTest(kk) = IntrTest(kk)+1;
            %                        break
            %                     end
            %
            %                     if kk == ColIntr-1 % We need to resize the collision duration vector because new Interaction time is larger than all the bins
            %                        Step = ceil(out(i).Interaction/ColLvls(end)); % How much larger should our new bins be
            %                        ColLvls = Step*ColLvls; % Rescale the bins
            %                        for ll = 1: ColIntr-1
            %
            %                            if ll <= ceil((ColIntr-1)/Step) % Updates the new bins
            %                                    upperlimit = min(Step*ll,ColIntr-1);
            %                                for mm = 1:4
            %                                    for nn=1:Vmax
            %                                        for oo=1:Jmax
            %                                             Intr(mm,nn,oo,ll) = sum(Intr(mm,nn,oo,Step*(ll-1)+1:upperlimit)) ;
            %                                        end
            %                                    end
            %                                end
            %
            %                                 IntrTest(ll) = sum(IntrTest(Step*(ll-1)+1:upperlimit));
            %                            else %clear old bins
            %                                 IntrTest(ll) = 0;
            %
            %                                 for mm = 1:4
            %                                    for nn=1:Vmax
            %                                        for oo=1:Jmax
            %                                             Intr(mm,nn,oo,ll) = 0 ;
            %                                        end
            %                                    end % Pick Vrel from a G
            %                                end
            %
            %
            %                            end
            %
            %                        end
            %
            %                            for ll = 1: ColIntr-1 % Now lets find which bin the high collision value goes into
            %                                 if out(i).Interaction <= ColLvls(ll+1)
            %                                     Intr(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,ll)= Intr(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,ll)+1;
            %                                     IntrTest(ll) = IntrTest(ll)+1;
            %                                     break
            %                                 end
            %                            end
            %
            %
            %                     end
            %
            %                 end
            
            %%%%%%%%%%%%%%%%%%
            
            
            
            
        else %Lets also log the errors
            
            ErrorCount = ErrorCount + 1;
            ListOfErrors(ErrorCount).code = error(i).code;
            ListOfErrors(ErrorCount).Th = Th(i);
            ListOfErrors(ErrorCount).Phi = Phi(i);
            ListOfErrors(ErrorCount).Psi = Psi(i);
            ListOfErrors(ErrorCount).Eta = Eta(i);
            ListOfErrors(ErrorCount).B = B(i);
            ListOfErrors(ErrorCount).Vr = Vr(i);
            ListOfErrors(ErrorCount).V = V;
            ListOfErrors(ErrorCount).J = J;
            ListOfErrors(ErrorCount).out = out(i);
            ListOfErrors(ErrorCount).error = error(i);
            
        end
        
    end
    
    %% Save Trajectories
    if (SaveData)
        tempsave = zeros(Ncalls/InnerLoops,14);
        for i = 1 : Ncalls/InnerLoops
            if error(i).code == 0 || error(i).code ==41 || error(i).code ==42
                h5col = h5col+1;
                tempsave(i,:)=[B(i) Vr(i) Th(i) Phi(i) Psi(i) Eta(i) out(i).Vr out(i).VLevel out(i).JLevel out(i).Chi out(i).React out(i).Interaction out(i).EtolInteg out(i).EtolTot];
                h5write(h5name,h5dataset,tempsave(i,:)',[1,h5col],[14 1]);
            end
        end
        fprintf (input.TrjFid,'-------Inner Loop: %5.0f of %5.0f \n', j, InnerLoops);
        h5pos = h5col;
        save(TempSave,'ErrorCount','RoVibLevel','Chi','H11','H12','H13','H22','H23','ColReact','h5pos','tempsave')
        if ErrorCount ~=0
            save(TempSave,'ListOfErrors','-append')
        end
    end
    fprintf (' Inner Loop: %5.0f of %5.0f \n', j, InnerLoops)


    %     if Calc == 1 && SaveData == 1
    %         save(strcat('DATA/MC_Samples=',num2str(j), '_V=',num2str(V), '_J=',num2str(J),'_Vr=', num2str(Vrel),'_tol=',num2str(par.tol),'.mat'))
    %     elseif Calc == 2 && SaveData == 1
    %         save(strcat('DATA/MC_Samples=',num2str(j), '_V=',num2str(V), '_J=',num2str(J),'_Temp=', Ttrans,'_tol=',num2str(par.tol),'.mat'))
    %     end
    
end

%% Prepare Outputs

% Log Errors
EC.Count = ErrorCount;

if ErrorCount == 0
    EC.ListOfErrors = 0;
else
    EC.ListOfErrors=ListOfErrors;
end


% Save the rest of data
if Calc == 1 % This was a cross-section calculation
    
    XSect = RoVibLevel * pi * Bmax^2 / (Ncalls-ErrorCount);
    output = XSect;
    
    
elseif Calc == 2 % This was a state-specific rate/collision integral calculation
    
    % Calculate state-to-state rates ( m^3/s)
    output.RATE = sqrt(8*pi*prop.k*Ttrans/prop.mu_a_bc/par.conv5)*(Bmax*par.conv6)^2 * RoVibLevel/(Ncalls-ErrorCount);
    
    % Collision Integ Here = Collision Integ/CI_HS*sigma^2 = Norm CI * sigma^2
    % Ignore failed collisions and reacting collisions
    % The results are in A^2, See my derivations
    output.CI11 = 0.5 *(Bmax)^2*H11/(Ncalls-ErrorCount-ColReact);
    output.CI12 = 1/6 *(Bmax)^2*H12/(Ncalls-ErrorCount-ColReact);
    output.CI13 = 1/24*(Bmax)^2*H13/(Ncalls-ErrorCount-ColReact);
    output.CI22 = 0.25*(Bmax)^2*H22/(Ncalls-ErrorCount-ColReact);
    output.CI23 = 1/16*(Bmax)^2*H23/(Ncalls-ErrorCount-ColReact);
    
end

if (SaveData)
    cflag = 1;
    try
       data=h5read(h5name,h5dataset);
    catch
       cflag = 0;
    end
    if cflag == 1
      for i=1:InnerLoops
          TempSave = [TempSaveHead,num2str(i),'.mat'];
          delete(TempSave);
      end
   end
end


if WorkersFlag == 1
    matlabpool close %No severance package for you!
end


%% Plots used for debuging

% %Reaction Type
% for i = 1: 4
%     Sum = 0;
%     for j = 1: Vmax
%         for k = 1: Jmax;
%             Sum = Sum + RoVibLevel(i,j,k);
%         end
%     end
%     TotalReact(i) = Sum;
% end
%
% figure(1)
% bar(TotalReact/Ncalls)
% xlabel ('Reaction Type')
% ylabel ('% Outcome')
% %
% % Vibration Level
% for i = 1: Vmax
%    Sum = 0;
%    for j = 1: 3
%        for k = 1: Jmax;
%            Sum = Sum + RoVibLevel(j,i,k);
%            %Sum = Sum + XSect(j,i,k);
%        end
%    end
%    TotalVib8(i) = Sum;
% end
% %
% figure(2)
% % bar([0:Vmax-1], TotalVib/Ncalls)
%
% plot([0:Vmax-1], TotalVib12/Ncalls,[0:Vmax-1], TotalVib10/Ncalls)
% xlabel ('Final Vibrational Level')
% ylabel ('% Outcome')
% xlim ([0 Vmax-1])
%
% %
% % Rotation Level
% for i = 1: Jmax
%    Sum = 0;
%    for j = 1: 3
%        for k = 1: Vmax;
%            Sum = Sum + RoVibLevel(j,k,i);
%        end
%    end
%    TotalRot(i) = Sum;
% end
%
% figure(3)
% bar([0:Jmax-1],TotalRot/Ncalls)
% xlabel ('Final Rotational Level')
% ylabel ('% Outcome')
% xlim ([0 Jmax-1])
%
% % Scattering Angle
% for i = 1: ChiIntr
%     Sum = 0;
%     for j = 1: 4
%         for k = 1: Vmax
%             for l = 1: Jmax
%                 Sum = Sum + Chi(j,k,l,i);
%            end
%         end
%     end
%     TotalChi(i) = Sum;
% end
% figure(4)
% bar(ChiLvls,TotalChi/Ncalls)
% xlabel ('Chi')
% ylabel ('% Outcome')
%
% if ErrorCount == 0
%     ' There were no errors'
%     SuccessLog
% else
%     fprintf (' Total Number of Errors: %5.0f \n', ErrorCount);
%     ErrorLog
% end
%
% %Other
% %Vibration Level from React 1
% for i = 1: Vmax
%     Sum = 0;
%     for j = 1: 1
%         for k = 1: Jmax;
%             Sum = Sum + RoVibLevel(j,i,k);
%         end
%     end
%     TotalVibR1(i) = Sum;
% end
%
% figure(5)
% bar([0:Vmax-1], TotalVibR1/TotalReact(1))
% xlabel ('Final Vibrational Level')
% ylabel ('% Outcome')
% xlim ([0 Vmax-1])
%
% % Vibration Level from React 2
% for i = 1: Vmax
%     Sum = 0;
%     for j = 2: 2
%         for k = 1: Jmax;
%             Sum = Sum + RoVibLevel(j,i,k);
%         end
%     end
%     TotalVibR2(i) = Sum;
% end
%
% figure(2)
% bar([0:Vmax-1], TotalVibR2/TotalReact(2))
% xlabel ('Final Vibrational Level')
% ylabel ('% Outcome')
% xlim ([0 Vmax-1])
%
% % Vibration Level from React 2
% for i = 1: Vmax
%     Sum = 0;
%     for j = 3: 3
%         for k = 1: Jmax;
%             Sum = Sum + RoVibLevel(j,i,k);
%         end
%     end
%     TotalVibR3(i) = Sum;
% end
%
% figure(2)
% bar([0:Vmax-1], TotalVibR3/TotalReact(3))
% xlabel ('Final Vibrational Level')
% ylabel ('% Outcome')
% xlim ([0 Vmax-1])
%
% % Rotation Level, V = 0
% for i = 1: Jmax
%     Sum = 0;
%     for j = 1: 1
%         for k = 1: 1;
%             Sum = Sum + RoVibLevel(j,k,i);
%         end
%     end
%     TotalRot1(i) = Sum;
% end
%
% figure(3)
% bar([0:Jmax-1],TotalRot1/sum(TotalRot1))
% xlabel ('Final Rotational Level')
% ylabel ('% Outcome')
% xlim ([0 Jmax-1])
%
% % Rotation Level, V = 0
% for i = 1: Jmax
%     Sum = 0;
%     for j = 1: 1
%         for k = 3: 3;
%             Sum = Sum + RoVibLevel(j,k,i);
%         end
%     end
%     TotalRot3(i) = Sum;
% end
%
% figure(3)
% bar([0:Jmax-1],TotalRot2/sum(TotalRot2))
% xlabel ('Final Rotational Level')
% ylabel ('% Outcome')
% xlim ([0 Jmax-1])
