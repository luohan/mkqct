function [dY_dt, V, R] = Derivatives_System(Y,mb_mbmc, mc_mbmc, mu_bc, mu_a_bc, conv3)
% This function calls he PES for a 3 particle system and performs a chain
% rule

%% Q coordinates to R coordinate 
%Transformations  to/fron generalized coordinates to potential coordinates  (A) 

R = zeros(1,3);
%dY_dt = zeros(1,12);


R(1) = sqrt((mc_mbmc*Y(1)+Y(4))^2+(mc_mbmc*Y(2)+Y(5))^2+(mc_mbmc*Y(3)+Y(6))^2);
R(2) = sqrt(Y(1)^2+Y(2)^2+Y(3)^2);
R(3) = sqrt((mb_mbmc*Y(1)-Y(4))^2+(mb_mbmc*Y(2)-Y(5))^2+(mb_mbmc*Y(3)-Y(6))^2);


%% Call PES
[V, dV] = PotentialEnergySurface(R);
V  = V/conv3;
dV = dV/conv3; 

V1 = (mc_mbmc*Y(1)+Y(4));
V2 = (mc_mbmc*Y(2)+Y(5));
V3 = (mc_mbmc*Y(3)+Y(6));

V4 = (mb_mbmc*Y(1)-Y(4));
V5 = (mb_mbmc*Y(2)-Y(5));
V6 = (mb_mbmc*Y(3)-Y(6));

Factor11 = -dV(1)*mc_mbmc/R(1);
Factor21 = -dV(2)/R(2);
Factor31 = -dV(3)*mb_mbmc/R(3);
Factor41 = -dV(1)/R(1); 
Factor51 = +dV(3)/R(3);

%% Governing ODEs

dY_dt=[ Y(7)/mu_bc...
       Y(8)/mu_bc...
       Y(9)/mu_bc...
       Y(10)/mu_a_bc...
       Y(11)/mu_a_bc...
       Y(12)/mu_a_bc...   
       Factor11*V1+ Factor21*Y(1) + Factor31*V4,...
       Factor11*V2+ Factor21*Y(2) + Factor31*V5,...
       Factor11*V3+ Factor21*Y(3) + Factor31*V6,...
       Factor41*V1+                 Factor51*V4,...
       Factor41*V2+                 Factor51*V5,...
       Factor41*V3+                 Factor51*V6];

end
