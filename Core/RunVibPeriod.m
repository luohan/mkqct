function [RR, PP, IRUN, R, Pv,error] = RunVibPeriod(V,J,prop,par)
% This script calculates maximum molecular stretch, rotational momentum and
% integrates molecular trajectory for 1 vibrational period to extract
% vibtrational times. 

%Changed 4/26/2014
%% Initialize

% V = 10
% J = 10


error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
[R, Pv, RR, PP, IRUN] = deal (0);
% Check to make sure that initial vibrational and rotational level is
% realistic
if prop.Vmax(1) < V || J > prop.Jmax(V+1,1)
    error.code = 1;
    error.where = 'RunVibPeriod';
    error.what = ' Starting V or J level is too high';
    return
end


%% Calc. max molecular stretch and rotational mom
[PP, RR,error] = TotMolecMom (prop,par,V,J); 

if error.code ~=0 % Simple Error checking 
    return
end

%% Get ready to integrate the trajectory for 1 vib period

% Initialize new vectors
%Imax   = floor(par.tmax/par.dt/40); % Max number of timesteps
Imax = length(prop.R_stretch); 
Y = zeros (1,6);
R = zeros(Imax,1);
Pv = zeros(Imax,1);

% Initial reference orientation. The values of these should not matter much
th2 = 0;
phi2= 0;
eta2= 0;

% Common angles
ct2 = cos(th2);
stcp2 = sin(th2)*cos(phi2);
stsp2 = sin(th2)*sin(phi2);

% Molecular Coords, starting at max streatch

Y(1) = RR*stcp2; % A
Y(2) = RR*stsp2; % A
Y(3) = RR*ct2;   % A

% Molecular Ro-vibrational mom.
Y(4) = -PP*(sin(phi2)*cos(eta2)+cos(phi2)*cos(th2)*sin(eta2)); %(amu * A / fs)
Y(5) =  PP*(cos(phi2)*cos(eta2)-sin(phi2)*cos(th2)*sin(eta2)); %(amu * A / fs)
Y(6) =  PP*(sin(th2)*sin(eta2)); %(amu * A / fs)


 %Energy at the innitial time step
    [~, EV] = Derivatives_Molec(Y,prop,par); 
    TE0 =0;
     for j = 1:3
        TE0 = TE0 + 1/2/prop.mu_bc * Y(j+3)^2; % Molecular Energy
     end
     TE0 = TE0 + EV;
     
  %% Integrate the molecule for 1 vibrational period
% We need to use constant time steps here because we will use them to
% calculate vib streatch 
%dt = par.dt;
for i = 1:Imax-1
    
     % Standard RK4 steps
     [dY, EV] = Derivatives_Molec(Y,prop,par);
     k1 = par.dt*dY; 
     
     [dY, ~] = Derivatives_Molec(Y+k1/2,prop,par);
     k2 = par.dt*dY;    
     
     [dY, ~] = Derivatives_Molec(Y+k2/2,prop,par);
     k3 = par.dt*dY;    
     
     [dY, ~] = Derivatives_Molec(Y+k3,prop,par);
     k4 = par.dt*dY;
     
     Y = Y + 1/6*(k1+2*k2+2*k3+k4);

% % Variable timestepping should not be used because we need constant time steps later.      
%     for j = 1: par.InnerLoops % Itterate until timesteps converged
% 
%      % Standard RK5-6 steps
%          [dY, ~] = QCTMolec(Y                                                                                                            );
%          k1 = dt*dY; 
% 
%          [dY, ~] = QCTMolec(Y +1/6 *k1                                                                                                  );
%          k2 = dt*dY;    
% 
%          [dY, ~] = QCTMolec(Y +4/75 *k1         +16/75 *k2                                                                              );
%          k3 = dt*dY;    
% 
%          [dY, ~] = QCTMolec(Y +5/6 *k1          -8/3 *k2        +5/2 *k3                                                                );
%          k4 = dt*dY;
% 
%          [dY, ~] = QCTMolec(Y -165/64 *k1       +55/6 *k2       -425/64 *k3         +85/96 *k4                                          );
%          k5 = dt*dY;
% 
%          [dY, ~] = QCTMolec(Y +12/5 *k1         -8 *k2          +4015/612 *k3       -11/36 *k4      +88/255 *k5                         );
%          k6 = dt*dY;
%          
%          [dY, ~] = QCTMolec(Y -8263/15000 *k1   +124/75 *k2     -643/680 *k3        -81/250 *k4     +2484/10625 *k5                     );
%          k7 = dt*dY;
%          
%          [dY, ~] = QCTMolec(Y +3501/1720 *k1    -300/43 *k2     +297275/52632 *k3   -319/2322 *k4   +24068/84065 *k5    +3850/26703 *k7 );
%          k8 = dt*dY;
%          
%          eps1 = (-1/160*k1 -125/17952*k3 +1/144*k4 -12/1955*k5 -3/44*k6 +125/11592*k7 +43/616*k8)/dt;
%          Ytest= Y +3/40*k1 +875/2244*k3 +23/72*k4 +264/1955*k5 +125/11592*k7 +43/616*k8;
%          eps2 = sqrt(eps1*eps1' / (Ytest*Ytest'));
%          
%                   
%          qq = (par.tol/eps2)^(1/5); % calculate how much to cut time based on momentum and position
%       
%          if eps2<par.tol %&& eps3<par.tol2
%              %Y= Y +3/40*k1 +875/2244*k3 +23/72*k4 +264/1955*k5 +125/11592*k7 +43/616*k8; % Advance the time step using 6th order rk 
%              Y = Ytest;
%              %Y = YY; 
%              time = time + dt; 
%              if qq > par.MaxdtJump
%                  qq = par.MaxdtJump;
%              end
%              dt = dt*qq;
%              break
%          elseif j < par.InnerLoops
%             
%              if qq <1e-3
%                  qq = 1e-3;
%              end
%              
%              dt = dt*qq;
%          else
%             error.code = 50; % Not enough inner loops.
%              error.where = 'QuantLevelDeconstruct';
%              error.what = 'Not enough Inner Loops';
%              return
%              
%          end
% 
%          
%     end

%      TT(i) = time;
%      DT(i) = dt;
     R(i) = sqrt(Y(1:3)*Y(1:3)'); % Molecular Sep
     Pv(i) = (Y(4:6)*Y(1:3)') / R(i); % Vib MOM
     
     % Find the max molecular streach
     if i > 10
         if R(i-1) > R(i) && R(i-2)<R(i-1)
             IRUN = i-2; % Last point before the max
             break
         elseif i == Imax-1
             
             error.code = 3;
             error.where = 'RunVibPeriod';
             error.what = ' Initial molecular vibration was too slow';
             return
         end
         
     end
     
%     % Keep track of momentum changes for debuging
%     Jxx(i) = Yin(i,2).*Yin(i,3+3) - Yin(i,3).*Yin(i,3+2);
%     Jyy(i) = Yin(i,3).*Yin(i,3+1) - Yin(i,1).*Yin(i,3+3);
%     Jzz(i) = Yin(i,1).*Yin(i,3+2) - Yin(i,2).*Yin(i,3+1);
%          
end   
 %Check Final molecular energy
[~, EV] = Derivatives_Molec(Y,prop,par); 
TE2 =0;
 for j = 1:3
    TE2 = TE2 + 1/2/prop.mu_bc * Y(j+3)^2; % Molecular Energy
 end
 TE2 = TE2 + EV;

 if 100*abs((TE2-TE0)/TE0) > par.Etol
    error.code = 2;
    error.where = 'RunVibPeriod';
    error.what = ' Molecular vibration integration failed';
    return
 end
  
 
end


