function [ output,EC ] = CorrectXsecEt( CurrentOut,CurrentError,prop,input,par )


%% Flags
WorkersFlag = 0; % Should the code hire and then fire workers or just keep the current workforce. The current workforce is hired by the Sweep_Something function
SaveData = input.SaveData;  % Should the code save intermediate data
% Use these to run
Bmax = input.Bmax; % A
V=input.V;       % v'
J=input.J;        % J'
Calc = input.Calc; % Type 1 is X-sect calc, Type 2 is state specific rate calc., Type 3 is monoquantum Deex 
Ttrans = input.Ttrans;
InnerLoops=input.InnerLoops;    % Number of Innerloops
Ncalls = input.Ncalls;  % Total number of samples
if Calc ~= 1
    warning('Incorrect Call of CorrectXsec');
end
%% Trajectory Save Prepare
 if (SaveData)
     % open file
     if (Calc == 2)    % Rate calculation
         h5name = strcat(input.TrjLoc,'/T=',num2str(input.Ttrans),'.h5');
     elseif (Calc == 1) %Xsec calculation
         h5name = strcat(input.TrjLoc,'/Et=',num2str(input.Et),'.h5');
     end
     if (exist(h5name,'file') == 0)
         h5fid = H5F.create(h5name);
     else
         h5fid = H5F.open(h5name,'H5F_ACC_RDWR','H5P_DEFAULT');
     end 
     %create group
     Vlink = strcat('/V=',num2str(input.V));
     if H5L.exists(h5fid,Vlink,'H5P_DEFAULT')
         gid=H5G.open(h5fid,Vlink,'H5P_DEFAULT');
     else
         gid=H5G.create(h5fid,Vlink,'H5P_DEFAULT','H5P_DEFAULT','H5P_DEFAULT');
     end
     h5dataset=strcat(Vlink,'/J=',num2str(input.J));
     if H5L.exists(h5fid,h5dataset,'H5P_DEFAULT')
       H5G.close(gid);
       H5F.close(h5fid);
       info = h5info(h5name,h5dataset);
       h5col = info.Dataspace.Size(2)-1;
       if h5col == -1
           h5col = 0;
       end
     else
       H5G.close(gid);
       H5F.close(h5fid);
       h5create(h5name,h5dataset,[14 Inf],'Datatype','double','ChunkSize',[14,100],'Deflate',9);
       h5col = 0;
       copyfile(h5name,[h5name,'.bak']);
     end
     %in{B,Vr,Th,Phi,Psi,Eta},out{Vr,J,V,Chi,React,InteractionTime,EtolInteg,EtolTot}
 end

%% Initialize Properties and Parameters
[RR, PP, IRUN, R_stretch, Pv_stretch,error] = RunVibPeriod_mex(V,J,prop,par); % Calculate vib period 
if error.code ~=0
	output=0;
 	EC.Count=Ncalls;
	EC.ListOfErrors = error;  
	return
end
prop.IRUN = IRUN;
prop.R_stretch= R_stretch;
prop.Pv_stretch = Pv_stretch;
prop.RR = RR;
prop.PP = PP; 

Jmax = ceil(prop.Jmax(1,1))+10; %Added a few extra levels for quantum to classical conversion errors
Vmax = prop.Vmax(1)+10; 


%% recover the data

RoVibLevel = round(CurrentOut * (Ncalls - CurrentError.Count) / Bmax^2/pi);
Ncorrect = CurrentError.Count;
Npar = floor(Ncalls/InnerLoops);

Nrest = mod(Ncorrect,Npar);
InnerLoops = floor(Ncorrect/Npar);


%Clear the variables
ChiIntr = 180; %Number of intervals the scattering angle is broken into
%ColIntr = 100;  %Number of intervals sampling duration is broken into
ChiLvls = linspace(0,pi, ChiIntr);

[H11, H12, H13, H22, H23] = deal(0); % Innitialize collision integrals 

Chi = zeros(4,Vmax,Jmax,ChiIntr);

ErrorList = CurrentError.ListOfErrors;
ErrorList = squeeze(struct2cell(ErrorList));
ErrorList = ErrorList(2:7,:);
ErrorList = cell2mat(ErrorList);
ErrorList = ErrorList';


%Intr= zeros(4,Vmax,Jmax,ColIntr-1);
%IntrTest=zeros(1,ColIntr-1);
%ColLvls=linspace(0,1000,ColIntr);
Ball = ErrorList(:,5);
Vrall = ErrorList(:,6);% Use constant relative velocity, unless this is a rate calculation
Thall =  ErrorList(:,1);
Phiall = ErrorList(:,2);
Psiall = ErrorList(:,3);
Etaall = ErrorList(:,4);

%Output vectors
out(Ncorrect) = struct( 'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0);
error(Ncorrect) = struct('code', 0, 'where', 0, 'what', 0);     

%% Run Trajectories
ErrorCount = 0; % Count of number of failed calculations
ColReact = 0; % Number of collisions that should be skipped by Collision Integral Calc. 


for j = 1:InnerLoops+1 % Sweep over Inner Loops. This is used to reduce required memory. Of course if there are too many inner loops then you'll lose parallel speedup
    %% Generate random sample
    if j <= InnerLoops
        NinLoop = Npar;
    else
        NinLoop = Nrest;
    end
    
    indexstart = (j-1)*Npar+1;
    indexend   = (j-1)*Npar+NinLoop;
    indexrange = [indexstart:indexend];
    
    B  = Ball(indexrange);
    Vr = Vrall(indexrange);
    Th = Thall(indexrange);
    Phi = Phiall(indexrange);
    Psi = Psiall(indexrange);
    Eta = Etaall(indexrange);

    parfor i = 1: NinLoop
        [out(i), error(i)] = Backbone_mex(B(i),Vr(i),Th(i),Phi(i),Psi(i),Eta(i),J,V,prop,par,par.filename1,par.filename2);
        %             [out(i),error(i)] = TestChi(B(i),Vr(i),Th(i),Phi(i),Psi(i),Eta(i),J,V,prop,par,par.filename1,par.filename2);
    end
    
    %% Postprocess the QCT Calculation (In serial)
    for i = 1 : NinLoop
        
        %%%%%%%%%%%%%%%%%%
        if error(i).code == 0 || error(i).code ==41 || error(i).code ==42 % Lets log the good calculations
            
            
            if out(i).VLevel <0  % This is were the round will fail.
                out(i).VLevel = 0;
            end
            
            % Sum up QCT results (Reaction type, Final Vib
            % Level, Final Rot Level)
            RoVibLevel(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1) = RoVibLevel(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1)+1; %Note, first index is 0th level
            
            if Calc == 1 % We care about individual cross sections
                % Lets bin the scattering Angles
                for jj = 1: ChiIntr-1
                    if out(i).Chi <= ChiLvls(jj+1)
                        Chi(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,jj)= Chi(out(i).React,round(out(i).VLevel)+1,round(out(i).JLevel)+1,jj)+1;
                        break
                    end
                end
                
            elseif Calc == 2 % We want rates and collision integrals
                if out(i).React ~= 4 % Don't sum up the Chi for RXNs
                    % This is a nifty way to avoid different
                    % collocation points for each collision
                    % integration
                    H11 = H11 + (1-cos(out(i).Chi)   )*gam(i)    ;
                    H12 = H12 + (1-cos(out(i).Chi)   )*gam(i)^2  ;
                    H13 = H13 + (1-cos(out(i).Chi)   )*gam(i)^3  ;
                    H22 = H22 + (1-cos(out(i).Chi)^2 )*gam(i)^2  ;
                    H23 = H23 + (1-cos(out(i).Chi)^2 )*gam(i)^3  ;
                else
                    ColReact = ColReact + 1;
                end
            end
            
   
            
            
            
        else %Lets also log the errors
            
            ErrorCount = ErrorCount + 1;
            ListOfErrors(ErrorCount).code = error(i).code;
            ListOfErrors(ErrorCount).Th = Th(i);
            ListOfErrors(ErrorCount).Phi = Phi(i);
            ListOfErrors(ErrorCount).Psi = Psi(i);
            ListOfErrors(ErrorCount).Eta = Eta(i);
            ListOfErrors(ErrorCount).B = B(i);
            ListOfErrors(ErrorCount).Vr = Vr(i);
            ListOfErrors(ErrorCount).V = V;
            ListOfErrors(ErrorCount).J = J;
            ListOfErrors(ErrorCount).out = out(i);
            ListOfErrors(ErrorCount).error = error(i);
            
        end
        
    end
    
    %% Save Trajectories
    if (SaveData)
        for i = 1 : NinLoop
            if error(i).code == 0 || error(i).code ==41 || error(i).code ==42
                h5col = h5col+1;
                tempsave=[B(i) Vr(i) Th(i) Phi(i) Psi(i) Eta(i) out(i).Vr out(i).VLevel out(i).JLevel out(i).Chi out(i).React out(i).Interaction out(i).EtolInteg out(i).EtolTot];
                h5write(h5name,h5dataset,tempsave',[1,h5col],[14 1]);
            end
        end
           fprintf (input.TrjFid,'-------Inner Loop: %5.0f of %5.0f \n', j, InnerLoops);		
    end
    fprintf (' Inner Loop: %5.0f of %5.0f \n', j, InnerLoops)

%     if Calc == 1 && SaveData == 1
%         save(strcat('DATA/MC_Samples=',num2str(j), '_V=',num2str(V), '_J=',num2str(J),'_Vr=', num2str(Vrel),'_tol=',num2str(par.tol),'.mat'))
%     elseif Calc == 2 && SaveData == 1
%         save(strcat('DATA/MC_Samples=',num2str(j), '_V=',num2str(V), '_J=',num2str(J),'_Temp=', Ttrans,'_tol=',num2str(par.tol),'.mat'))
%     end
    
end

%% Prepare Outputs    

% Log Errors
EC.Count = ErrorCount; 

if ErrorCount == 0
 	EC.ListOfErrors = 0;
else
    EC.ListOfErrors=ListOfErrors;
end


% Save the rest of data
if Calc == 1 % This was a cross-section calculation
    
    XSect = RoVibLevel * pi * Bmax^2 / (Ncalls-ErrorCount);
    output = XSect;


elseif Calc == 2 % This was a state-specific rate/collision integral calculation
    
    % Calculate state-to-state rates ( m^3/s)
    output.RATE = sqrt(8*pi*prop.k*Ttrans/prop.mu_a_bc/par.conv5)*(Bmax*par.conv6)^2 * RoVibLevel/(Ncalls-ErrorCount);
    
    % Collision Integ Here = Collision Integ/CI_HS*sigma^2 = Norm CI * sigma^2 
    % Ignore failed collisions and reacting collisions
    % The results are in A^2, See my derivations
    output.CI11 = 0.5 *(Bmax)^2*H11/(Ncalls-ErrorCount-ColReact);
    output.CI12 = 1/6 *(Bmax)^2*H12/(Ncalls-ErrorCount-ColReact); 
    output.CI13 = 1/24*(Bmax)^2*H13/(Ncalls-ErrorCount-ColReact); 
    output.CI22 = 0.25*(Bmax)^2*H22/(Ncalls-ErrorCount-ColReact); 
    output.CI23 = 1/16*(Bmax)^2*H23/(Ncalls-ErrorCount-ColReact); 

end    
    


end

