function [Y0,error] = PreCollision (prop,input, par)
% This function establishes the initial conditions for the integration
% scrip. 

% Changed 4/26/14
%% Innitial Position and momentum Tests

% Test Parameters
% input.b = .30;        % A, Impact Parameter rho = 10*1.2; 
% input.rho =20*1.2;    % Reaction shell 
% input.Vr =0.0077;      % A/fs, Relative Velocity (500K -> Vr = 0.0077)
% input.th = pi/2;         % angle between BC bond and Z axis
% input.phi =0;      % angle between BC bond and X axis
% input.eta = 0;     % angle of the rotational momentum 
% input.psi = pi;
% input.j = 11;         % Rotational quantum number
% input.v = 10;         % Vibrational quantum number
% prop = MolecularProp;
% par = NumericalPar;
% [Jmax] = MaxLevels();            % and max ro-vibrational levels
% prop.Jmax = Jmax;

error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
%% Set up Fully Stretched Coordinates

ct = cos(input.th);
stcp = sin(input.th)*cos(input.phi);
stsp = sin(input.th)*sin(input.phi);

% Molecular Coords.
Y0 = zeros (1,12);
Y0(1) = prop.RR*stcp; % A
Y0(2) = prop.RR*stsp; % A
Y0(3) = prop.RR*ct;   % A

% Molecular Ro-vibrational mom.
Y0(4) = -prop.PP*(sin(input.phi)*cos(input.eta)+cos(input.phi)*cos(input.th)*sin(input.eta)); %(amu * A / fs)
Y0(5) =  prop.PP*(cos(input.phi)*cos(input.eta)-sin(input.phi)*cos(input.th)*sin(input.eta)); %(amu * A / fs)
Y0(6) =  prop.PP*(sin(input.th)*sin(input.eta)); %(amu * A / fs)

% Calculate initial molecular angular momentum
Jx = Y0(2).*Y0(3+3) - Y0(3).*Y0(3+2);
Jy = Y0(3).*Y0(3+1) - Y0(1).*Y0(3+3);
% Jz = Y(1).*Y(3+2) - Y(2).*Y(3+1);


%Energy at the initial time step
[~, V] = Derivatives_Molec(Y0,prop,par); 
TE0 =0;
 for j = 1:3
    TE0 = TE0 + 1/2/prop.mu_bc * Y0(j+3)^2; % Molecular Energy
 end
TE0 = TE0 + V;

%% Calculate streach and vib coordinates during phase angle psi

% index corresponding to phase angle
irun = round((prop.IRUN-1)*input.psi/2/pi)+1; 

%% Recompute the new coordinates

% New Molecular Coords.

Y0(1) = prop.R_stretch(irun)*stcp; % A
Y0(2) = prop.R_stretch(irun)*stsp; % A
Y0(3) = prop.R_stretch(irun)*ct;   % A


% Use angular momentum conservation to calculate linear momentum at new
% coordinate (See notes on PreCollision, phase shift)

A = [   ct      0       -stcp;
        0       ct      -stsp;
        stcp    stsp    ct  ];
    
B = [ Jy/prop.R_stretch(irun); -Jx/prop.R_stretch(irun); prop.Pv_stretch(irun)];

P2 = A\B;

if sum(isnan(P2)) > 0 % Since we are using Euler angles, the transformation can fail. 
    error.code = 4;
    error.where = 'PreCollision';
    error.what = ' The initial molecular momentum phase shift failed , Man....';
    return
else
    Y0(7:9) = P2;
end

%Check energy at new transformed time step
[~, V] = Derivatives_Molec([Y0(1:3) P2'],prop,par); 
TEF =0;
 for j = 1:3
    TEF = TEF + 1/2/prop.mu_bc * P2(j)^2; % Molecular Energy
 end
 TEF = TEF + V;

if 100*abs(TEF/TE0-1)> par.Etol
    error.code = 4;
    error.where = 'PreCollision';
    error.what = 'Energy not conserved after the transformation';
    return
end
 
 
%% Atomic Initial conditions
  
% Atomic Coords.
Y0(4) = 0;                                  % A
Y0(5) = input.b;                            % A
Y0(6) = -sqrt(par.rho^2-input.b^2);         % A

% Atomic-Molecular mom. 
Y0(10) = 0;                                 %(amu * A / fs)
Y0(11) = 0;                                 %(amu * A / fs)
Y0(12) = prop.mu_a_bc*input.Vr;             %(amu * A / fs)

end

