function [out, error] = BackboneTrj (B,Vr,Th,Phi,Psi,Eta,J,V,prop,par)
% % This is the main QCT code
%Version 38 - June 19 2014
% % Marat Kul.

% %% Read inputs

input.b = B;        % A, Impact Parameter rho = 10*1.2;
input.Vr =Vr;       % A/fs, Relative Velocity (500K -> Vr = 0.0077)
input.th = Th;      % angle between BC bond and Z axis
input.phi =Phi;     % angle between BC bond and X axis
input.psi =Psi;     % vibrational streatch coordinate
input.eta = Eta;    % angle of the rotational momentum
input.j = J;        % Rotational quantum number
input.v = V;        % Vibrational quantum number

% For debugging use the data below
% prop = MolecularProp;
% par = NumericalPar;
% [Jmax] = MaxLevels();            % and max ro-vibrational levels
% prop.Jmax = Jmax;

%  [RR, PP, IRUN, R_stretch, Pv_stretch,~] = RunVibPeriod_mex(V,J,prop,par);



% par.filename1=filename1;
% par.filename2=filename2;
%% Test inputs
%  input.b =6;        % A, Impact Parameter rho = 10*1.2;
% input.Vr =0.077;       % A/fs, Relative Velocity (500K -> Vr = 0.0077)
% input.th = pi/2;      % angle between BC bond and Z axis
% input.phi =pi/2;     % angle between BC bond and X axis
% input.psi =pi/4;     % vibrational streatch coordinate
% input.eta =pi/4;    % angle of the rotational momentum
%  input.j = 10;        % Rotational quantum number
%  input.v =5;        % Vibrational quantum number


error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
out = struct('B',B,'Th',Th,'Phi',Phi,'Psi',Psi,'Eta',Eta,'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0,'Trj',[],'TrjLen',0,'Mode',0);
coder.varsize('out(:).Trj',[],[1 1]);

for RhoLoop = 1: par.RhoRetry % Try changing Rho if calculation did not converge
    
    %% Initialize the rest of the data
    error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
    
    
    %% Pre collision
    
    [Y0,error] = PreCollision (prop, input, par);   % setup the innitial conditions
    
    if error.code ~= 0 % If there is an error, throw in the towel and give up.
        return
    end
    
    %% Integration
    
    Yf = Y0; % This won't be used but the code doesn't compile without
    for i = 1: par.IntegRetry % Try integrating a few times with different tol values
        
        [Yf,out, error] = IntegrationTrj (Y0, out, prop,par ); % Integrate the trajectories
        
        if error.code == 0 % If there are no errors, break.
            break
        elseif error.code == 31 && i<par.IntegRetry % Interation failed to conserve energy
            par.tol = par.tol/par.CutTol;
        else % Integration failed for some other reason
            return
        end
    end
    
    
    %% Post collision
    [out,error] = PostCollision (Yf,out,prop,par, 2); % Lets figure out what we have now
    
    if error.code ~= 0
        return % Failed now? But you were so close to finishing.... Ahhh!
    end
    
    
    %% Final Calc Test (we might have to replace this)
    
    [error, out] = Etest (input,out,prop,par);
    
    
    if error.code == 0 % If there are no errors, break.
        break
    elseif error.code == 60 && i<par.IntegRetry % Interation failed to conserve energy
        par.rho = par.rho*par.MultRho;
    else % Integration failed for some other reason
        return
    end
    
end




%% List of Changes:
%V40 - Change Backbone_system to get 25% speedup. 10/9/14

%V38 - automatically adjustable rho, Fixed Imax-1 in RunVibperiod

%V37 Edit output from the MonoQuantumDeEx script, remove error=43 (integration exceeded Nmax) as
%acceptable error, track integration and total energy error, bin energy
%errors, time only integration parts, program now catches integration error
%without continuing on to PostCollision, Allow v'=-1, this reduces the
%number of code 53 errors.

%V36 In integration consider position and momentum errors seperatley.
%Select the next timestep based on the larger of the errors.
% Re-try integration a few times.

%V35 Add selectable potentials and ro-vib level models

%V34 First implementation (hard coded) of Varandas Potential

%V33 Combined all sampling inside CrossSection_Rate_calc, use efficient
%Gamma sampling

%V32 Moved PreCollision outside of backbone

%V29 Verified version of the code of 4/4/2014

%V28 Used for code verification against Venus

%V27 optimized QCT_new, QCT_molec and QuantumLevelDeconstruct for speed

%V26 Rate calculation script added

%V25 fixed indecies error in QuantumLevelDeconstruct

%V22 lowered tolerance on v convergance in QuantumLevelDeconstruct. Now the
%code will not crash if J>Jmax(V), now all trajectories are written,

% V21 - Fixed MaxLevels script. Initially, rotational streatching was not
% considered

% V20 - Fixed PostCollision to handle cases where a full dissociation
% occured but two of the molecules stayed close. Ie the atoms from the
% broken molecule departed at a shallow angle.

