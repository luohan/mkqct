function [Out,Error] = CalculateTrj( IC,ICLen,I0Loop,prop,par,Vrel,CV,CJ,postfix,savefolder)
%% This script is for calculating trajectories and saved trajectories
% -- IC : initial condition of trajectories, it should have 6 rows with
%         ICLen columns
% -- I0Loop: how many trjs are calculated once
% -- IIndex: index of trajectory in original trj file
if isunix
    slash = '/';
else
    slash = '\';
end
if ICLen > I0Loop
    Remain = mod(ICLen,I0Loop);
    Ncalls = (ICLen-Remain)/I0Loop+1;
    if Ncalls > 3
       Ncalls = 3;  %only calculate 2*I0Loop+Remain
    end
else
    Remain = ICLen;
    Ncalls = 1;
end
TrjInput = struct('Vrel',Vrel,'V',CV,'J',CJ);
if strcmp(postfix,'Ex')
    TrjTypeID = [2 3];
elseif strcmp(postfix,'Dis')
    TrjTypeID = 4;
else
    TrjTypeID = 1;
    savefolder = [savefolder,postfix,slash];
    if ~exist(savefolder,'dir')
        mkdir(savefolder);
    end
end

% Just for compiling use
Out = struct('B',0,'Th',0,'Phi',0,'Psi',0,'Eta',0,'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0,'Trj',[],'TrjLen',0,'Mode',0);
Error = struct('B',0,'Vr',0,'Th',0,'Phi',0,'Psi',0,'Eta',0,'V',0,'J',0,'code', 0, 'error',[]);
for i = 1:Ncalls
    if i == 1
        name2 = strcat(savefolder,'Trj_Vr=',num2str(Vrel),'V=',num2str(CV),'J=',num2str(CJ),'_',postfix,'.mat');
    else
        name2 = strcat(savefolder,'Trj_Vr=',num2str(Vrel),'V=',num2str(CV),'J=',num2str(CJ),'_',postfix,'_',num2str(i-1),'.mat');
    end
    
    if exist(name2,'file')
        try
            load(name2)  %make sure the file is not corrupted
            continue
        catch
        end
    end
    
    
    if i == 1
        ILoop = Remain;
%         B = IC(1,1:ILoop);
%         Vr = IC(2,1:ILoop);
%         Th = IC(3,1:ILoop);
%         Phi = IC(4,1:ILoop);
%         Psi = IC(5,1:ILoop);
%         Eta = IC(6,1:ILoop);
        TrjIC = IC(:,1:ILoop);
    else
        ILoop = I0Loop;
        istart = Remain+(i-2)*ILoop+1;
        iend   = Remain+(i-1)*ILoop;
        
%         B   = IC(1,istart:iend);
%         Vr  = IC(2,istart:iend);
%         Th  = IC(3,istart:iend);
%         Phi = IC(4,istart:iend);
%         Psi = IC(5,istart:iend);
%         Eta = IC(6,istart:iend);
        TrjIC = IC(:,istart:iend);
    end
    
    Out = repmat(struct('B',0,'Th',0,'Phi',0,'Psi',0,'Eta',0,'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0,'Trj',[],'TrjLen',0,'Mode',0),ILoop,1);
    Error = repmat(struct('code', 0, 'where', 0, 'what', 0),ILoop,1);
    
    
    
    parfor j = 1:ILoop
        CIC = TrjIC(:,j);
        [Out(j), Error(j)] = BackboneTrj(CIC(1),CIC(2),CIC(3),CIC(4),CIC(5),CIC(6),CJ,CV,prop,par);
    end
    
    % remove wrong type trjs : hope not alot.. we don't save them 
    CorrectTyeIndex = ismember([Out.React],TrjTypeID);
    if sum(CorrectTyeIndex) ~= length(Out)
        Out2 = Out(~CorrectTyeIndex);   %get indirect reaction tyoe
        save([savefolder,'Trj_Vr=',num2str(Vrel),'V=',num2str(CV),'J=',num2str(CJ),'_',postfix,'_Wrong.mat'],'Out2');
    end
        
    Out = Out(CorrectTyeIndex);
    Error = Error(CorrectTyeIndex);

    ECode = [Error.code];
    ErrorIndex = find(~ismember(ECode,[0 41 42]));
    ErrorCount = length(ErrorIndex);
    ErrorList = struct('B',0,'Vr',0,'Th',0,'Phi',0,'Psi',0,'Eta',0,'V',0,'J',0,'code', 0, 'error',[]);
    
    if ErrorCount ~= 0 
        ErrorList = repmat(ErrorList,ErrorCount,1);
        for EC = 1:ErrorCount
            ErrorList(EC).code = Error(ErrorIndex(EC)).code;
            ErrorList(EC).B = TrjIC(1,EC);
            ErrorList(EC).Vr = TrjIC(2,EC);
            ErrorList(EC).Th = TrjIC(3,EC);
            ErrorList(EC).Phi = TrjIC(4,EC);
            ErrorList(EC).Psi = TrjIC(5,EC);
            ErrorList(EC).Eta = TrjIC(6,EC);
            ErrorList(EC).V = CV;
            ErrorList(EC).J = CJ;
            ErrorList(EC).error = Error(ErrorIndex(EC));
        end
        CorrectIndex = setdiff(1:ILoop,ErrorIndex);
        Out = Out(CorrectIndex);
    end
    TrjType = postfix;
     
    temp = whos('Out');
    tosave = temp.bytes;
    temp = whos('ErrorList');
    tosave = tosave +temp.bytes;
     
    if tosave/1024/1024/1024 > 1.9
      save(name2,'Out','ErrorList','ErrorCount','TrjType','-v7.3');
    else
      save(name2,'Out','ErrorList','ErrorCount','TrjType');
    end

end
end

