function [E, error] = RoVibLevels(v,j,prop,moltype)
% Calculate energy (in eV) of a ro-vibrational level (v,j)
% May 22 2013
% Oct 7  2015 Add Rovibrational energy for different diatomic molecule
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
if nargin<4
    moltype = 1;
end
switch prop.LevType 
    case 1
        % See Laurendeau for derivation 
        % wellDepth is nessisary if potential min is < 0 eV
        Bv =prop.Be(moltype)-prop.ae(moltype)*(v+1/2);    %eV
        E = prop.we(moltype)*(v+1/2)-prop.wexe(moltype)*(v+1/2)^2+Bv*j*(j+1)-prop.De(moltype)*j^2*(j+1)^2-prop.wellDepth(moltype);    %eV
    case 2 % Vib levels are tabulated but rot levels are from the complex model
        if v>=0
            % Note, here the wellDepth is taken into account within Ev
            Ev = interp1(prop.EvL(:,moltype), prop.Ev(:,moltype), v); % Interp from a table. 
            Bv =prop.Be(moltype)-prop.ae(moltype)*(v+1/2);    %eV
            E = Ev + Bv*j*(j+1)-prop.De(moltype)*j^2*(j+1)^2;
        else % If the selected level is lower than 0. This may happen after a classical interation. 
            dEv = prop.Ev(2,moltype)-prop.Ev(1,moltype); 
            Ev = prop.Ev(1,moltype)+dEv*v;
            Bv =prop.Be(moltype)-prop.ae(moltype)*(v+1/2);    %eV
            E = Ev + Bv*j*(j+1)-prop.De(moltype)*j^2*(j+1)^2;
        end
        
    case 3
        E=0;
        error.code = 70;
        error.where = 'RoVibLevels';
        error.what =' TableRoVib option is not finished';
        
    case 4
        maxv = sum(prop.Ev(:,moltype) ~= 0)-1;
        Ev = 0.0;
        if ( v>= 0 && v <= maxv)
            Ev = interp1q(prop.EvL(1:(maxv+1),moltype), prop.Ev(1:(maxv+1),moltype), v);
        elseif v < 0
            dEv = prop.Ev(2,moltype)-prop.Ev(1,moltype);
            Ev = prop.Ev(1,moltype)+dEv*v;
        elseif v > maxv  %This actually won't happen
            dEv = prop.Ev(maxv,moltype)-prop.Ev(maxv-1,moltype);
            Ev = prop.Ev(maxv,moltype)+dEv*(v-maxv);
        end
        
        jj = j*(j+1);
        vl = max(floor(v),0)+1;   vh  = ceil(v)+1;
        Elow = prop.Par(vl,1,moltype)*jj+prop.Par(vl,2,moltype)*jj^2+prop.Par(vl,3,moltype)*jj^3;
        if vl ~= vh
            Ehigh = prop.Par(vh,1,moltype)*jj+prop.Par(vh,2,moltype)*jj^2+prop.Par(vh,3,moltype)*jj^3;
            Ej = Elow + (Ehigh - Elow)/(vh-vl)*(v - vl +1);
%             Ej = interp1([vl-1 vh-1], [Elow Ehigh], v);
        else
            Ej = Elow;
        end
        E = Ev + Ej;
    otherwise
        E =0; 
        error.code = 71;
        error.where = 'RoVibLevels';
        error.what  = 'Check your RoVibLevels option';
end

        
end