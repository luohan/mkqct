function [ par ] = NumericalParTrj ()

%% This function stores all of the main numerical values
% Trj post fix is added since this one is specifically used for detailed
% trajectory calculations
%% Calculation Parameters
par.vis = 1; % 1 to visualize trajectories, 2 to not visualize
par.ots = 100; % time steps between when trajectories are printed and checked for errors. 
par.filename2 = 'TEST.XYZ';  % file name for the xMOL trajectory
par.filename1 = 'TEST.TXT';  % file name for data dump. 

%% Variable Timesteping Parameters
par.tmax        = 1.0e4; % Maximum integration time, fs. This is used to guess max number of timesteps tmax/dt = max number of timesteps
par. dt         = 1.0e-2; % fs, initial timesteps. This value matters more for RunVibPeriod, that uses constant timesteps than Integration
par.InnerLoops  = 30    ; % Max number of inner loops for optimizing dt, Can keep at 30
par.MaxdtJump   = 1.2   ; % Max increase in dt, prevents the code from picking large dt too quickly and failing 

par.tol         = 1e-10 ; % tolarance on 5th and 6th order RK comparison on position/momentum
par.tol2        =1e-12  ; % tolarance on energy in QuantLevelDeconstruct for secant loop convergences 
par.IntegRetry  = 5     ; % How many times are we willing to re-try integrating 
par.CutTol      = 3   ; % How much smaller should the tolerance be when we re-try integration

%% Conversion checks
par.Etol = 1e-6; % Energy conservation tolerance in integration, percent
par.Etol2= 1e-2; % Energy conservation between initial and final states, percent
par.rho =15;    % Reaction shell diameter
par.tol0 = 1e-4; % Definition of 0
par.RhoRetry = 2 ; 
par.MultRho = 4; 

%% Unit Conversions
par.conv1 = 3.758725110833465e+65  ; % eV/ [(m^2*kg/s)^2/(amu*A^2)]
par.conv2 = 6.022141290000000e+31   ; % A^2*amu*s/m^2/kg/fs
par.conv3 = 103.642692; % eV / [ amu*A^2/fs^2]
par.conv4 = 1.66053892e-17; % (m^2*kg*fs^2)/(A^2*amu*s^2)
par.conv5 = 1.66053892e-27; % kg/amu
par.conv6 = 1e-10; %m / A
