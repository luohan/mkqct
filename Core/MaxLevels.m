function [Jmax,PotMAXBC,Vmax,MaxJlevel] = MaxLevels(prop,par)
%% Calculate Max v and j levels
% Jmax is stored in column
% w = 1 mol = BC = 2
% w = 2 mol = CA = 3
% w = 3 mol = AB = 1
Jmax=-1*ones(prop.MVmax+1,prop.ntmol);
MaxJlevel = prop.MaxJlevel;
for moltype=1:prop.ntmol
    switch moltype
        case 1
            mol = 2;
            mu  = prop.mu_bc;
        case 2
            mol = 3;
            mu  = prop.mu_ac;
        case 3
            mol = 1;
            mu  = prop.mu_ab;
    end
    
    
    %clear;clc;
    %Numeric parameters
    % prop = MolecularProp;
    % par = NumericalPar;
    RInf =prop.Rinf;
    Rin = ones(1,3)*RInf;
    
    [PotMax] = deal(zeros(MaxJlevel+1,1));
    [Error_old] = deal(0);
    [DissLev, ~] = PotentialEnergySurface([RInf, RInf, RInf]);
    %%  Lets find potential Max
    
    R = 15;
    RR = zeros(MaxJlevel+1,1);
    for j = 1: MaxJlevel+1
        if prop.Rmin(j,moltype) == -1 %skip non-bounded state
            continue
        end
        J = j-1;
        
        R_old     = R;
        Rp        = R - 0.05;
        Error_old = Eprime(R_old,J,prop,par,mol,mu);
        Error_p   = Eprime(Rp   ,J,prop,par,mol,mu);
        
        for i = 1:200
            R_new     = Rp - Error_p*(Rp-R_old)/(Error_p-Error_old);
            if abs(R_new/R_old) > 1.5
                R_new = 1.5*R_old;
            end
            Error_new = Eprime(R_new   ,J,prop,par,mol,mu);
            if abs(Error_new) <= par.tol2
                if R_new > 30*prop.Re(moltype)  %converge to infinity
                    R_new = 3.0*prop.Rmin(j,moltype);
                else
                    Rtest1 = R_new-0.005;
                    Etest1 = Eprime(Rtest1,J,prop,par,mol,mu);
                    
                    Rtest2 = R_new+0.005;
                    Etest2 = Eprime(Rtest2,J,prop,par,mol,mu);
                    if Etest1 >0 && Etest2 < 0 %good result
                        R  = R_new;
                        Rin(mol) = R;
                        [V, ~] = PotentialEnergySurface(Rin);
                        PotMax(j) = V + J*(J+1)*prop.hbar^2/(2*mu*R^2)*par.conv1;
                        RR(j) = R;
                        break;
                    else      %%converge to min
                        R_new = 3.0*prop.Rmin(j,moltype);
                    end
                end
                Error_new = Eprime(R_new   ,J,prop,par,mol,mu);
            end
            if isnan(R)
                R_new = 3.0*prop.Rmin(j,moltype);
                Error_new = Eprime(R_new   ,J,prop,par,mol,mu);
            end
            
            if i == 200
                PotMax(j) = DissLev;
                R = 3.0*prop.Rmin(j,moltype);
            end
            R_old     = Rp;
            Error_old = Error_p;
            Rp        = R_new;
            Error_p   = Error_new;
        end
        
    end
    
    if RR(MaxJlevel+1) ~=0
        warning('The prop.MaxJlevle might be too samll');
    end
    
    %% Now lets find the max J level for each vib level
    for i = 0: prop.Vmax(moltype)
        for j = 0: MaxJlevel
            if prop.Rmin(j+1,moltype) == -1
                continue   %skip unbounded state
            end
            E1 = RoVibLevels(i,j,prop,moltype);
            %         j
            %         E1-PotMax(j)
            
            if j == 0 && E1>PotMax(1)
                Jmax(i+1,moltype) = -1;
                break
            elseif E1 > PotMax(j+1);
                Jmax(i+1,moltype) = j-1;
                break
            end
        end
    end
    if moltype == 1
        PotMAXBC = PotMax;
    end
   % plot(Jmax)
end
if sum(sum(Jmax~=-1) ~= prop.Vmax+1)
    warning('prop.Vmax needs to changed outside function');
    Vmax = sum(Jmax~=-1)-1;
    MVmax = max(Vmax);
    if MVmax ~= prop.MVmax
        warning('prop.MVmax needs to be changed outside function');
        Jmax = Jmax(1:MVmax+1,:);
    end
else
    Vmax = prop.Vmax;
    MVmax = max(Vmax);
end
if max(max(Jmax))~= prop.MaxJlevel
    MaxJlevel= max(max(Jmax));
    warning('prop.MaxJlevel needs to be changed outside function');
else
    MaxJlevel = prop.MaxJlevel;
end
return

