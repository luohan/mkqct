function [error,out] = Etest (input,out,prop, par)
% This script tests the total energy conservation within the trajectory
% code
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');

switch out.React
    case 1
        mol = 2;
        mu_post = prop.mu_a_bc;
    case 2
        mol = 1;
        mu_post = prop.mu_c_ab;
    case 3
        mol = 3;
        mu_post = prop.mu_b_ac;
    otherwise
        error.code = 100;
        error.where = 'Etest';
        error.what = 'Impossible reaction type comes into Etest'; %this won't happen
        return
end

moltype = prop.moltype(mol);

%% Original Energy
Tr0 = 1/2 * prop.mu_a_bc*input.Vr^2;

% Bv =prop.Be-prop.ae*(input.v+1/2);    %eV
% E1 = prop.we*(input.v+1/2)-prop.wexe*(input.v+1/2)^2+Bv*input.j*(input.j+1)-prop.De*input.j^2*(input.j+1)^2-prop.wellDepth;    %eV
E1 = RoVibLevels(input.v,input.j, prop);
E0int = E1/par.conv3;
E0Tot = E0int + Tr0;


%% Final Energy;
TrF = 1/2 * mu_post *out.Vr^2; % This mu value may have to change if we have other products

%Bv =prop.Be-prop.ae*(out.VLevel+1/2);    %eV
%E2 = ( prop.we*(out.VLevel+1/2)-prop.wexe*(out.VLevel+1/2)^2+Bv*out.JLevel*(out.JLevel+1)-prop.De*out.JLevel^2*(out.JLevel+1)^2)-prop.wellDepth;
E2 = RoVibLevels(out.VLevel, out.JLevel,prop,moltype); 
EFint = E2/par.conv3;
EFTot = TrF + EFint;

%% Energy Change
if abs((E0Tot-EFTot)/E0Tot)*100 > par.Etol2 % Note this test is not in percent to compensate for the fact that the two particles do not start inifinitley far from each other
    error.code = 60;
    error.where = 'Etest';
    error.what =' Final and Initial energies do not match';
end 

out.EtolTot = abs((E0Tot-EFTot)/E0Tot); % Note this is not in percent. 
return
    
