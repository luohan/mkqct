function [V,dV] = PotentialEnergySurface( R )
persistent c g;
%% Triple A prime Gamallo's PES This is the potential surface for the main collision
% R(1), R(3) = N-O N-O
% R(2) = N-N
% First three term is two-body interaction, coming from 10.1063/1.1586251
% Three body term coming from Candler 10.1063/1.1586251
%% Two-body terms
kcaltoev=0.0433634 ;
% NO X^2 \Pi
De1 =  152.53*kcaltoev;       % eV
Re1 = 1.1508;        % A
% nue1= 1947.3;       % cm^(-1)
% nuexe1 = 16.128;      % cm^(-1)
a11 = 6.1958;        % A^(-1)
a21 = 11.1408;        % A^(-2)
a31 = 9.2570;        % A^(-3)
a41 = 6.9823;
a51 = 19.6021;
%Re1      = 1.1508;    % A        Morse fit
%beta1    = 2.7611;     % A^-1
%Dm1      = 6.6142;    % eV

rho1 = R(1) - Re1;
rho3 = R(3) - Re1;

V1 = -De1*(1 + a11*rho1 + a21*rho1^2 + a31*rho1^3 + a41*rho1^4 + a51*rho1^5)*exp(-a11*rho1);
V3 = -De1*(1 + a11*rho3 + a21*rho3^2 + a31*rho3^3 + a41*rho3^4 + a51*rho3^5)*exp(-a11*rho3);

dV1=[-De1*(a11 + 2*a21*rho1 + 3*a31*rho1^2 + 4*a41*rho1^3 + 5*a51*rho1^4)*exp(-a11*rho1),0,0];
dV1=dV1+[V1*(-a11),0,0];

dV3=[0,0,-De1*(a11 + 2*a21*rho3 + 3*a31*rho3^2 + 4*a41*rho3^3 + 5*a51*rho3^4)*exp(-a11*rho3)];
dV3=dV3+[0,0,V3*(-a11)];


% N2 X^1\Sigma_g^+
De2 = 228.41*kcaltoev;       % eV
Re2 = 1.0977;        % A
% nue2= 2388.2;       % cm^(-1)
% nuexe2 = 14.582;      % cm^(-1)
a12 = 3.7790;        % A^(-1)
a22 = -0.2694;        % A^(-2)
a32 = -0.6111;        % A^(-3)
a42 = -1.9853;
a52 = 0.8992;
%Re1      = 1.0977;
%beta1    = 2.7976;
%Dm1      = 9.9046;

rho2 = R(2) - Re2;

V2 = -De2*(1 + a12*rho2 + a22*rho2^2 + a32*rho2^3 + a42*rho2^4 + a52*rho2^5)*exp(-a12*rho2);

dV2=[0,-De2*(a12 + 2*a22*rho2 + 3*a32*rho2^2 + 4*a42*rho2^3 + 5*a52*rho2^4)*exp(-a12*rho2),0];
dV2=dV2+[0,V2*(-a12),0];

%% Three body terms
Rtri1 = 1.4789;      % A    N-O
Rtri2 = 1.9680;      % A    N-N

rhotri1 = R(2) - Rtri2;  %NN
rhotri2 = R(1) - Rtri1;  %NO
rhotri3 = R(3) - Rtri1;  %NO

s = [rhotri1,1/sqrt(2)*(rhotri2+rhotri3),1/sqrt(2)*(rhotri2-rhotri3)];
spower = [1.0 	1.0 	1.0;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ];  %faster than ones
for i=2:7
    spower(i,:) = spower(i-1,:).*s;
end


if isempty(c)
    c = zeros(7,7,7);
    c(1,1,1) = 4.6391   ;  c(1,5,1) = -0.3493  ;  c(4,4,1) = -4.3822  ;
    c(2,1,1) = -0.4166  ;  c(1,3,3) = -13.3716 ;  c(4,2,3) = 6.8153  ;
    c(1,2,1) = -2.2228  ;  c(1,1,5) = -7.5467  ;  c(3,5,1) = 2.1739  ;
    c(3,1,1) = 6.7267   ;  c(6,1,1) = 4.7293   ;  c(3,3,3) = -26.2565  ;
    c(2,2,1) = -4.4265  ;  c(5,2,1) = -16.2801 ;  c(3,1,5) = 8.0923  ;
    c(1,3,1) = 3.5126   ;  c(4,3,1) = 19.8618  ;  c(2,6,1) = -1.8714  ;
    c(1,1,3) = 3.1430   ;  c(4,1,3) = -15.1099 ;  c(2,4,3) = 19.3009  ;
    c(4,1,1) = 4.7954   ;  c(3,4,1) = -11.8867 ;  c(2,2,5) = 4.2956  ;
    c(3,2,1) = -9.4334  ;  c(3,2,3) = 44.9758  ;  c(1,7,1) = -0.1853  ;
    c(2,3,1) = 4.8414   ;  c(2,5,1) = 4.6653   ;  c(1,5,3) = -2.1531  ;
    c(2,1,3) = 6.0499   ;  c(2,3,3) = -43.1208 ;  c(1,3,5) = -11.7849  ;
    c(1,4,1) = -3.2647  ;  c(2,1,5) = -9.8957  ;  c(1,1,7) = -0.1736  ;
    c(1,2,3) = -3.9775  ;  c(1,6,1) = -0.0326  ;  g =  [2.3806  2.1632];
    c(5,1,1) = 4.9946   ;  c(1,4,3) = 19.0150  ;  
    c(4,2,1) = -12.3015 ;  c(1,2,5) = 20.2721  ;
    c(3,3,1) = 13.4089  ;  c(7,1,1) = 1.0226   ;
    c(3,1,3) = -18.0314 ;  c(6,2,1) = -7.0602  ;
    c(2,4,1) = -11.3859 ;  c(5,3,1) = 8.6305   ;
    c(2,2,3) = 19.3841  ;  c(5,1,3) = 0.3747   ;
end

P=0.0;
dVtri1 = [0.0 0.0 0.0];
ds= 1/sqrt(2);
invs0 = 1./s.*[1 ds ds]; 
invs = invs0;
for k=0:2:6
    p3=invs0(3)*k;
    for j=0:1:6-k
        p2=invs0(2)*j;
        invs(1)=p2+p3; invs(3)=p2-p3;
        for i=0:1:6-k-j
            fac = c(i+1,j+1,k+1)*spower(i+1,1)*spower(j+1,2)*spower(k+1,3);
            P = P + fac;
           % dVtri1 = dVtri1+fac*invs*[0 i 0;j 0 j;k 0 -k];
            invs(2)=invs0(1)*i;
          % invs = invs0.*[i j k];
           dVtri1 = dVtri1+fac*invs;
        end
    end
end

w = g.*s(1:2)/2;
pol = 1-tanh(w);
polpro = pol(1)*pol(2);

sechf = sech(w);
sechf = sechf.*sechf;

Vtri = P*polpro;

dVtri1 = dVtri1*polpro;
dVtri2 = -P/2*(sechf(1)*pol(2)*g(1)*[0.0 1.0 0.0]...
    +sechf(2)*pol(1)*g(2)*ds*[1.0 0.0 1.0]);

dVtri = dVtri1+dVtri2;
%% Summation
V = V1+V2+V3+Vtri;
dV = dV1+dV2+dV3+dVtri;


