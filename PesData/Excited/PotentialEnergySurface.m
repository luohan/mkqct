function [V,dV] = PotentialEnergySurface( R )
persistent c g;
%% Triple A prime Gamallo's PES This is the potential surface for the main collision
% R(1), R(3) = N-O N-O
% R(2) = N-N
% First three term is two-body interaction, coming from 10.1063/1.1586251
% Three body term coming from Candler 10.1063/1.1586251
%% Two-body terms
kcaltoev=0.0433634 ;
% NO X^2 \Pi
De1 =  152.53*kcaltoev;       % eV
Re1 = 1.1508;        % A
% nue1= 1947.3;       % cm^(-1)
% nuexe1 = 16.128;      % cm^(-1)
a11 = 6.1958;        % A^(-1)
a21 = 11.1408;        % A^(-2)
a31 = 9.2570;        % A^(-3)
a41 = 6.9823;
a51 = 19.6021;
%Re1      = 1.1508;    % A        Morse fit
%beta1    = 2.7611;     % A^-1
%Dm1      = 6.6142;    % eV

rho1 = R(1) - Re1;
rho3 = R(3) - Re1;

V1 = -De1*(1 + a11*rho1 + a21*rho1^2 + a31*rho1^3 + a41*rho1^4 + a51*rho1^5)*exp(-a11*rho1);
V3 = -De1*(1 + a11*rho3 + a21*rho3^2 + a31*rho3^3 + a41*rho3^4 + a51*rho3^5)*exp(-a11*rho3);

dV1=[-De1*(a11 + 2*a21*rho1 + 3*a31*rho1^2 + 4*a41*rho1^3 + 5*a51*rho1^4)*exp(-a11*rho1),0,0];
dV1=dV1+[V1*(-a11),0,0];

dV3=[0,0,-De1*(a11 + 2*a21*rho3 + 3*a31*rho3^2 + 4*a41*rho3^3 + 5*a51*rho3^4)*exp(-a11*rho3)];
dV3=dV3+[0,0,V3*(-a11)];


% N2 X^1\Sigma_g^+
De2 = 228.41*kcaltoev;       % eV
Re2 = 1.0977;        % A
% nue2= 2388.2;       % cm^(-1)
% nuexe2 = 14.582;      % cm^(-1)
a12 = 3.7790;        % A^(-1)
a22 = -0.2694;        % A^(-2)
a32 = -0.6111;        % A^(-3)
a42 = -1.9853;
a52 = 0.8992;
%Re1      = 1.0977;
%beta1    = 2.7976;
%Dm1      = 9.9046;

rho2 = R(2) - Re2;

V2 = -De2*(1 + a12*rho2 + a22*rho2^2 + a32*rho2^3 + a42*rho2^4 + a52*rho2^5)*exp(-a12*rho2);

dV2=[0,-De2*(a12 + 2*a22*rho2 + 3*a32*rho2^2 + 4*a42*rho2^3 + 5*a52*rho2^4)*exp(-a12*rho2),0];
dV2=dV2+[0,V2*(-a12),0];

%% Three body terms
Rtri1 = 1.4789;      % A    N-O
Rtri2 = 1.9680;      % A    N-N

rhotri1 = R(2) - Rtri2;  %NN
rhotri2 = R(1) - Rtri1;  %NO
rhotri3 = R(3) - Rtri1;  %NO

s = [rhotri1,1/sqrt(2)*(rhotri2+rhotri3),1/sqrt(2)*(rhotri2-rhotri3)];
spower = [1.0 	1.0 	1.0;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ];  %faster than ones
for i=2:7
    spower(i,:) = spower(i-1,:).*s;
end


if isempty(c)
    c = zeros(7,7,7);
    
c(1,1,1) =  -0.24433; c(2,1,3) = -134.1400; c(1,3,3) =  35.1620; c(2,1,5) = 158.4800; c(3,5,1) =  93.8390; 
c(2,1,1) =  16.9690; c(1,4,1) =   7.2390; c(1,1,5) = 103.7800; c(1,6,1) =   1.8731; c(3,3,3) =   5.9270; 
c(1,2,1) =   4.8575; c(1,2,3) =  43.3780; c(6,1,1) = -54.5180; c(1,4,3) =  17.0210; c(3,1,5) = 112.2900; 
c(3,1,1) =  56.0340; c(5,1,1) =  55.6610; c(5,2,1) = 176.6800; c(1,2,5) = -59.6940; c(2,6,1) =  14.6400; 
c(2,2,1) = -18.5080; c(4,2,1) = 221.5000; c(4,3,1) = 150.7600; c(7,1,1) =  15.1950; c(2,4,3) = -106.1000; 
c(1,3,1) =  -4.0797; c(3,3,1) = -23.8990; c(4,1,3) = -312.0400; c(6,2,1) = -78.3090; c(2,2,5) = 113.1500; 
c(1,1,3) = -38.1060; c(3,1,3) = -177.7000; c(3,4,1) =  14.0730; c(5,3,1) =   3.6847; c(1,7,1) =  -1.9621; 
c(4,1,1) = 183.2900; c(2,4,1) =  49.8530; c(3,2,3) = -179.4100; c(5,1,3) =  82.4050; c(1,5,3) =  41.7610; 
c(3,2,1) =  58.6890; c(2,2,3) =  17.9350; c(2,5,1) =  51.1780; c(4,4,1) =  56.8990; c(1,3,5) = -28.7550; 
c(2,3,1) = -48.6340; c(1,5,1) =  -4.0670; c(2,3,3) =  56.3210; c(4,2,3) = -204.1700; c(1,1,7) = -74.6060; 
end

P=0.0;
dVtri1 = [0.0 0.0 0.0];
ds= 1/sqrt(2);
invs0 = 1./s.*[1 ds ds]; 
invs = invs0;
for k=0:2:6
    p3=invs0(3)*k;
    for j=0:1:6-k
        p2=invs0(2)*j;
        invs(1)=p2+p3; invs(3)=p2-p3;
        for i=0:1:6-k-j
            fac = c(i+1,j+1,k+1)*spower(i+1,1)*spower(j+1,2)*spower(k+1,3);
            P = P + fac;
           % dVtri1 = dVtri1+fac*invs*[0 i 0;j 0 j;k 0 -k];
            invs(2)=invs0(1)*i;
          % invs = invs0.*[i j k];
           dVtri1 = dVtri1+fac*invs;
        end
    end
end

w = g.*s(1:2)/2;
pol = 1-tanh(w);
polpro = pol(1)*pol(2);

sechf = sech(w);
sechf = sechf.*sechf;

Vtri = P*polpro;

dVtri1 = dVtri1*polpro;
dVtri2 = -P/2*(sechf(1)*pol(2)*g(1)*[0.0 1.0 0.0]...
    +sechf(2)*pol(1)*g(2)*ds*[1.0 0.0 1.0]);

dVtri = dVtri1+dVtri2;
%% Summation
V = V1+V2+V3+Vtri;
dV = dV1+dV2+dV3+dVtri;


