!*********************************************************************** 
!   System:                     N2O (lowest 3A" state) 
!   Functional form:            six-order polynomials
!   Common name:                N2O-3App (adiabatic triplet ground state)
!   Number of derivatives:      1
!   Number of bodies:           3
!   Number of electronic surfaces: 1
!   Interface: Section-2
!
!   References: P.Gamallo, Miguel Gonzalez, and R. Sayos
!
!   Note: HE-MM-1 format
!
!    1 : O - N
!    2 : N - N'
!    3 : N'- O
!
!   Input: X(3),Y(3),Z(3)               in units of bohr
!   Output: E                           in units of hartree
!   Output: dEdX(3),dEdY(3),dEdZ(3)     hartree/bohr
!***********************************************************************
      subroutine pot(symb,x,y,z,e,dEdX,dEdY,dEdZ,nat,mnat)
      implicit none
      integer,parameter :: dp=8
      integer :: nat,mnat
      character(2) :: symb(mnat)
      real(dp) :: x(nat), y(nat),z(nat),e,dEdX(nat),dEdY(nat),dEdZ(nat)
      real(dp) :: xpes(3,3),dEdXpes(3,3)
      real(dp) :: temp(3)
      integer :: i,io=1
      
      xpes(:,1) = x
      xpes(:,2) = y
      xpes(:,3) = z


      do i=1,nat
        if (symb(i) .eq. 'o') then
          if (i .ne.1) then
            ! make sure first one is always oxygen
            io = i
            temp = xpes(1,:)
            xpes(1,:) = xpes(io,:)
            xpes(io,:) = temp          
            exit
          endif
        endif
      enddo

      call n2opes(xpes,e,dEdXpes)

      if (io .ne. 1 ) then
        temp = dEdXpes(io,:)
        dEdXpes(io,:) = dEdXpes(1,:)
        dEdXpes(1,:) = temp
      endif
      dEdX = dEdXpes(:,1)
      dEdY = dEdXpes(:,2)
      dEdZ = dEdXpes(:,3)

      end subroutine pot

! =============================================================================
!                      N2O PES
! ============================================================================
      subroutine n2opes(X,E,dEdX)

      implicit none 
      
      integer,parameter :: dp=8
      ! input and ioutput
      ! first index is for different atom
      ! second index is for x,y,z
      ! oxygen should always be the first atom
      real(dp) :: X(3,3)   
      real(dp) :: E,dEdX(3,3)

      ! constants
      real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: irt2 =1.0D0/dsqrt(2.0D0)
      ! two body constants NO, N2, NO
      integer,parameter :: MolType(3) =  (/1,2,1/)
      real(dp),parameter :: De(2)=(/152.53D0,228.41D0/)*kcal2ev
      real(dp),parameter :: Re(2)=(/1.1508D0,1.0977D0/)
      real(dp),parameter :: a(3,2)=reshape((/4.3205D0,1.6392D0,1.4946D0,&
                               5.2376D0,6.3901D0,6.3676D0/),(/3,2/))

      ! three body constants
      real(dp),parameter :: Rtri(2)=(/1.3231D0,2.0980D0/)
      real(dp),parameter :: c(50) =  (/   7.4166D0,   6.2889D0,  14.4281D0,&
                             13.4837D0,  14.8181D0,   8.9826D0,   1.8306D0,&
                             -0.7505D0,  -8.2247D0, -38.7067D0, -48.1654D0,&
                            -15.3167D0,  -0.3524D0,  12.8055D0,  32.9097D0,&
                             71.4660D0,  45.2012D0,   3.0192D0,  12.9529D0,&
                             -7.0240D0, -31.3627D0,  -9.0038D0, -21.3448D0,&
                            -12.7283D0,   7.2399D0,   5.4116D0,   4.3132D0,&
                             -0.1399D0,  -8.4511D0, -30.9122D0, -34.5457D0,&
                            -23.2229D0, -12.2485D0,  12.7846D0,  74.1564D0,&
                             74.0436D0,  14.2243D0, -85.2948D0,-150.0063D0,&
                            -47.4708D0,  69.8672D0,  45.2050D0,  -5.4665D0,&
                             -3.0718D0,  13.6449D0,  11.6069D0,  41.4556D0,&
                              7.7826D0, -23.4852D0,  -3.8063D0/)
      real(dp),parameter :: gam(2) = (/2.5067D0, 3.0405D0/)
      
      ! variables used in the function 
      integer :: i,j,k,ic
      real(dp) :: R(3),dX(3,3),dRdX(3,3),dRdY(3,3),dRdZ(3,3)
      real(dp) :: rho(3),V2(3),rhotri(3),dV2dR(3)
      real(dp) :: spower(3,0:6),s(3),invs(3),dSdR(3),pfac(3),P,T,fac,dPdR(3)
      real(dp) :: tfac(2), dTdS(2),dTdR(3),V3,dV3dR(3)=0.0D0,dVdR(3)
 
      DATA ((dRdX(i,j),i=1,3),j=1,3) /9*0.0D00/
      DATA ((dRdY(i,j),i=1,3),j=1,3) /9*0.0D00/
      DATA ((dRdZ(i,j),i=1,3),j=1,3) /9*0.0D00/
      DATA dPdR /3*0.0D00/
      DATA dV2dR /3*0.0D00/
      DATA ((spower(i,j),i=1,3),j=0,6) /21*1.0D00/

      do i = 1,3 !loop through O-N, N-N', N'-O bond
        j = i+1
        if (j > 3) j=1

        dX(i,:) = X(i,:) - X(j,:)
        R(i) = dsqrt(dX(i,1)*dX(i,1) + dX(i,2)*dX(i,2) + dX(i,3)*dX(i,3))

        dRdX(i,i) =  dX(i,1)/R(i)
        dRdX(i,j) = -dRdX(i,i)

        dRdY(i,i) =  dX(i,2)/R(i)
        dRdY(i,j) = -dRdY(i,i)

        dRdZ(i,i) =  dX(i,3)/R(i)
        dRdZ(i,j) = -dRdZ(i,i)
      enddo
      dX = dX*bohr2a
      R = R*bohr2a

    
      !-------------- Two-body interaction term
      do i = 1,3
        j = MolType(i)
        rho(i) = R(i) - Re(j)
        rhotri(i) = R(i) - Rtri(j)

        V2(i) = -De(j)*(1.0D0 + rho(i)*(a(1,j)+rho(i)*(a(2,j)+rho(i)*a(3,j))))
        V2(i) = V2(i)*dexp(-a(1,j)*rho(i))
     
        dV2dR(i) = -De(j)*(a(1,j)+rho(i)*(2.0D0*a(2,j)+3.0D0*rho(i)*a(3,j)))
        dV2dR(i) = dV2dR(i)*dexp(-a(1,j)*rho(i)) 
        dV2dR(i) = dV2dR(i) + V2(i)*(-a(1,j))
      enddo

      !--------------  Three-body interaction term
      ! precompute power
      s(1) = rhotri(2) 
      s(2) = irt2*(rhotri(1)+rhotri(3))
      s(3) = irt2*(rhotri(1)-rhotri(3))
      invs(1) = 1.0D0/s(1)
      invs(2) = irt2/s(2)
      invs(3) = irt2/s(3)
  
      spower(:,1) = s
      do i = 2,6
        spower(:,i) = spower(:,i-1)*s
      enddo

      P = 0 
      ic = 0
      dPdR = 0.0D0
      do k = 0,6,2
        pfac(3) = invs(3)*k
        do j = 0,(6-k)
          pfac(2) = invs(2)*j
          dSdR(1) = pfac(2) + pfac(3)
          dSdR(3) = pfac(2) - pfac(3)
          do i = 0,(6-k-j)
            dSdR(2) = invs(1)*i

            ic = ic + 1
            fac = c(ic)*spower(1,i)*spower(2,j)*spower(3,k)
            P = P + fac

            dPdR = dPdR + fac*dSdR
          enddo
        enddo
      enddo

      pfac(1:2) = gam(1:2)*s(1:2)/2.0D0
      tfac = 1.0D0 - dtanh( pfac(1:2))
      T = tfac(1)*tfac(2)
      dTdS = 1.0D0/dcosh(pfac(1:2))
      dTdS = - dTdS * dTdS * gam/2  !dT/dS

      dTdR(2) = dTdS(1)*tfac(2)
      dTdR(1) = tfac(1)*dTdS(2)*irt2
      dTdR(3) = dTdR(1)

      V3 = P*T
      dV3dR = dPdR*T + P*dTdR

      ! ---------- summation
      E = V2(1)+V2(2)+V2(3)+V3
      E = E*ev2hart

      dVdR = dV2dR + dV3dR
      dEdX(:,1) = dVdR(1)*dRdX(1,:) + dVdR(2)*dRdX(2,:) + &
                  dVdR(3)*dRdX(3,:)
      dEdX(:,2) = dVdR(1)*dRdY(1,:) + dVdR(2)*dRdY(2,:) + &
                  dVdR(3)*dRdY(3,:)
      dEdX(:,3) = dVdR(1)*dRdZ(1,:) + dVdR(2)*dRdZ(2,:) + &
                  dVdR(3)*dRdZ(3,:)

      dEdX = dEdX*ev2hart*bohr2a
      end subroutine n2opes

!==================================================================
!                   N2O two body interaction
!==================================================================
      subroutine diapot_int(r,im,nsurf,v)
      ! two body interaction
      ! im = 1,3 NO bond
      ! im = 2   NN bond
      ! r in bohr, v in hartree
      implicit none
      integer,parameter :: dp = 8
      ! constans
      real(dp),parameter :: kcal2ev=0.0433634D0,ev2hart=0.0367502D0
      real(dp),parameter :: bohr2a=0.52917721067D0
      real(dp),parameter :: De(2)=(/152.53D0,228.41D0/)*kcal2ev
      real(dp),parameter :: Re(2)=(/1.1508D0,1.0977D0/)
      real(dp),parameter :: a(3,2)=reshape((/4.3205D0,1.6392D0,1.4946D0,&
                               5.2376D0,6.3901D0,6.3676D0/),(/3,2/))
      ! contants
      real(dp) :: r(3), rA ,v, rho
      integer :: im,nsurf,j

      if (im .le. 3)then
        if (im .eq. 2)then
          j = 2
        else
          j = 1
        endif

        rA = r(im)*bohr2a

        rho = rA - Re(j)
        v = -De(j)*(1.0D0 + rho*(a(1,j)+rho*(a(2,j)+rho*a(3,j))))
        v = v*dexp(-a(1,j)*rho)
        v = v*ev2hart
      endif
      end subroutine diapot_int



      subroutine prepot

      return
      end
