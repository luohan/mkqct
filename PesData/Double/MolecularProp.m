function [prop] = MolecularProp()

%% This function holds all needed molecular properties for QCT
prop = struct('M',0,'mu_bc',0,'mu_ac',0,'mu_ab',0, 'mu_a_bc',0, 'mu_b_ac',0, 'mu_c_ab',0);

% Conversions
%prop.conv = 9.64853e-3; % amu*A^2/fs^2 / eV
conv1 = 1/8065.54429;     % converstion factor, eV / cm^-1

%% Masses of atoms
prop.ma = 16;   % amu, mass of atom
prop.mb = 14;   % amu, mass of the first  atom in the molecule
prop.mc = 14;   % amu, mass of the second atom in the molecule


%% Potential Parameters
prop.Rinf = 100; % How far should the 3rd atom be moved to make the potential independent of it. 
prop.LevType = 4; %'Complex000'; % How are ro-vib levels specificed. Options: Complex or Table 

% =1 -> Use complex ro-vib model ( must specify we,wexe, wellDepth, Be, ae, De)
% =2 -> Use table for vib levels and complex for rot levels ( must specif Ev, EvL, Be, ae, De)
% =3 -> Use table for ro-vib levels ( not used) not developed
% =4 -> Use E(v,j=0) and Be, -De, and Hv
%            E(v,j) = E(v,j=0) +  Be(v)*j*(j+1)-De(v)*j^2*(j+1)^2+Hv(v)*j^3*(j+1)^3
%       #prop.ntmol mat files are readen. The file should contain a matrix
%       named FitPar. First column is E(v,j=0). Second column is Jmax. 
%       3-5:  Be, De, Hv

% Morse properties 
prop.Dm     = [9.904798 6.6142];    %eV This is needed to be assigned
prop.beta   = [2.8107  2.7811];     %A^-1
prop.Re     = [1.0977  1.1508];    %A This is needed to be assigned

% RoVibLevel properties, must specify
prop.ntmol   = 2;                          % number of types of molecules 
prop.moltype = [2 1 2];                    % type of molecule for AB, BC and AC, BC is always the first type
prop.Vmax    = [60 52];                    % maximum vibirational energy
prop.MVmax   = max(prop.Vmax);   
prop.MaxJlevel = 280;                      % assumed max Jlevel for all molecules, prop.Jmax will be solved
% -----------------------------------------------------------------------------------
prop.we     = [2374.8 1900.4]*conv1;       % From Gammalo's paper Triple A prime    |
prop.wexe   = [15.757 16.348]*conv1;       % From Gammalo's paper                   |
prop.Be     = [1.9754 1.6861]*conv1;       % From Gammalo's paper                   | ->for compiling use
prop.ae     = [0.0215 0.0199]*conv1;       % From Gammalo's paper                   | ->will be overlaped by the following data
prop.wellDepth = [228.41 152.53]*0.043;    % From Gammalo's paper                   |
prop.De     = [5.72e-6 5.40e-6]*conv1;       % From Gammalo's paper                 |
prop.Ev     = zeros(prop.MVmax+1,prop.ntmol);      %                                |
prop.EvL    = zeros(prop.MVmax+1,prop.ntmol);      %                                |
prop.Par    = zeros(prop.MVmax+1,3,prop.ntmol);    %                                |
%------------------------------------------------------------------------------------
if prop.LevType == 1
    % Parameter for prop.LevType = 1
    prop.we     = [2374.8 1900.4]*conv1;       % From Gammalo's paper Triple A prime    
    prop.wexe   = [15.757 16.348]*conv1;       % From Gammalo's paper                   
    prop.Be     = [1.9754 1.6861]*conv1;       % From Gammalo's paper                  
    prop.ae     = [0.0215 0.0199]*conv1;       % From Gammalo's paper                  
    prop.wellDepth = [228.41 152.53]*0.043;    % From Gammalo's paper                  
    prop.De     = [5.72e-6 5.40e-6]*conv1;       % From Gammalo's paper       
elseif prop.LevType == 2
    % Parameter for prop.LevType = 2 not used
    prop.Ev     = zeros(prop.MVmax+1,prop.ntmol);
    prop.EvL    = zeros(prop.MVmax+1,prop.ntmol);
    prop.Be     = [1.9754 1.6861]*conv1;       % From Gammalo's paper                  
    prop.ae     = [0.0215 0.0199]*conv1;       % From Gammalo's paper  
    prop.De     = [5.72e-6 5.40e-6]*conv1;       % From Gammalo's paper       
elseif prop.LevType == 4
    %Parameter for prop.LevType = 4
    datafile    = {'N2double.mat','NOdouble.mat'};       % Name of file
    prop.Ev     = zeros(prop.MVmax+1,prop.ntmol);
    prop.EvL    = zeros(prop.MVmax+1,prop.ntmol);
    prop.Jmax   = ones(prop.MVmax+1,prop.ntmol)*(-1);
    prop.Par    = zeros(prop.MVmax+1,3,prop.ntmol);
    for i=1:prop.ntmol
        load(datafile{i});
        [m, ~]              = size(FitPar);
        prop.Ev(1:m,i)      = FitPar(:,1)*conv1;
        prop.EvL(1:m,i)     = 0:m-1;
        prop.Jmax(1:m,i)    = FitPar(:,2);
        prop.Par(1:m,1:3,i) = FitPar(:,3:5)*conv1;
        clear FitPar;
    end
else
    error('Not supported RoVibModel');
end

    
%% Other needed constants. Dont touch these
prop.k = 1.3806488e-23; % m^2kg/s^2/K
prop.hbar = 1.05457148e-34; % m^2kg/s, Plank's const/2pi 
prop.M = prop.ma + prop.mb + prop.mc;                % amu
prop.mu_bc = (prop.mb*prop.mc)/(prop.mb+prop.mc);    % amu
prop.mu_ac = (prop.ma*prop.mc)/(prop.ma+prop.mc);    % amu
prop.mu_ab = (prop.ma*prop.mb)/(prop.ma+prop.mb);    % amu
prop.mu_a_bc = prop.ma*(prop.mb+prop.mc)/prop.M;     % amu 
prop.mu_b_ac = prop.mb*(prop.ma+prop.mc)/prop.M;     % amu
prop.mu_c_ab = prop.mc*(prop.ma+prop.mb)/prop.M;     % amu

prop.mc_mbmc = prop.mc/(prop.mb+prop.mc);
prop.mb_mbmc = prop.mb/(prop.mb+prop.mc);


%% Space Holders for Molecular Parameters
% These are computed within RunVibPeriod
prop.RR = 0;
prop.PP = 0;
prop.IRUN = 0;
prop.R_stretch= zeros(1e7,1);
prop.Pv_stretch = zeros(1e7,1);





return

