function [V,dV] = PotentialEnergySurface( R )
persistent c g;
%% Triple A double prime Gamallo's PES This is the potential surface for the main collision
% R(1), R(3) = N-O N-O
% R(2) = N-N
% First three term is two-body interaction, coming from 10.1063/1.1586251
% Three body term coming from Candler 10.1063/1.1586251
%% Two-body terms
kcaltoev=0.0433634 ;
% NO X^2 \Pi
De1 =  152.53*kcaltoev;       % eV
Re1 = 1.1508;        % A
% nue1= 1900.4;       % cm^(-1)
% nuexe1 = 16.348;      % cm^(-1)
a11 = 4.3205;        % A^(-1)
a21 = 1.6392;        % A^(-2)
a31 = 1.4946;        % A^(-3)


rho1 = R(1) - Re1;
rho3 = R(3) - Re1;

V1 = -De1*(1 + a11*rho1 + a21*rho1^2 + a31*rho1^3)*exp(-a11*rho1);
V3 = -De1*(1 + a11*rho3 + a21*rho3^2 + a31*rho3^3)*exp(-a11*rho3);

dV1=[-De1*(a11 + 2*a21*rho1 + 3*a31*rho1^2)*exp(-a11*rho1),0,0];
dV1=dV1+[V1*(-a11),0,0];

dV3=[0,0,-De1*(a11 + 2*a21*rho3 + 3*a31*rho3^2)*exp(-a11*rho3)];
dV3=dV3+[0,0,V3*(-a11)];


% N2 X^1\Sigma_g^+
De2 = 228.41*kcaltoev;       % eV
Re2 = 1.0977;        % A
% nue2= 2374.8;       % cm^(-1)
% nuexe2 = 15.757;      % cm^(-1)
a12 = 5.2376;        % A^(-1)
a22 = 6.3901;        % A^(-2)
a32 = 6.3676;        % A^(-3)


rho2 = R(2) - Re2;

V2 = -De2*(1 + a12*rho2 + a22*rho2^2 + a32*rho2^3)*exp(-a12*rho2);

dV2=[0,-De2*(a12 + 2*a22*rho2 + 3*a32*rho2^2)*exp(-a12*rho2),0];
dV2=dV2+[0,V2*(-a12),0];

%% Three body terms
Rtri1 = 1.3231;      % A    N-O
Rtri2 = 2.0980;      % A    N-N

rhotri1 = R(2) - Rtri2;  %NN
rhotri2 = R(1) - Rtri1;  %NO
rhotri3 = R(3) - Rtri1;  %NO

s = [rhotri1,1/sqrt(2)*(rhotri2+rhotri3),1/sqrt(2)*(rhotri2-rhotri3)];
spower = [1.0 	1.0 	1.0;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ;
        1.0 	1.0 	1.0 ];  %faster than ones
for i=2:7
    spower(i,:) = spower(i-1,:).*s;
end


if isempty(c)
    c = zeros(7,7,7);
    c(1,1,1)=7.4166   ;   c(1,5,1)=-21.3448 ;    c(4,4,1)=-9.0038 ;
    c(2,1,1)=6.2889   ;   c(1,3,3)=-85.2948 ;    c(4,2,3)=14.2243 ;
    c(1,2,1)=-0.7505  ;   c(1,1,5)=-3.0718  ;    c(3,5,1)=7.2399 ;
    c(3,1,1)=14.4281  ;   c(6,1,1)=8.9826   ;    c(3,3,3)=-47.4708 ;
    c(2,2,1)=-8.2247  ;   c(5,2,1)=-15.3167 ;    c(3,1,5)=11.6069 ;
    c(1,3,1)=12.8055  ;   c(4,3,1)=45.2012  ;    c(2,6,1)=4.3132 ;
    c(1,1,3)=-8.4511  ;   c(4,1,3)=-23.2229 ;    c(2,4,3)=45.2050 ;
    c(4,1,1)=13.4837  ;   c(3,4,1)=-31.3627 ;    c(2,2,5)=7.7826 ;
    c(3,2,1)=-38.7067 ;   c(3,2,3)=74.0436  ;    c(1,7,1)=-0.1399 ;
    c(2,3,1)=32.9097  ;   c(2,5,1)=-12.7283 ;    c(1,5,3)=-5.4665 ;
    c(2,1,3)=-30.9122 ;   c(2,3,3)=-150.0063;    c(1,3,5)=-23.4852 ;
    c(1,4,1)=12.9529  ;   c(2,1,5)=13.6449  ;    c(1,1,7)=-3.8063 ;
    c(1,2,3)=12.7846  ;   c(1,6,1)=5.4116   ;    g=[2.5067 3.0405];
    c(5,1,1)=14.8181  ;   c(1,4,3)=69.8672  ;   
    c(4,2,1)=-48.1654 ;   c(1,2,5)=41.4556  ;
    c(3,3,1)=71.4660  ;   c(7,1,1)=1.8306   ;
    c(3,1,3)=-34.5457 ;   c(6,2,1)=-0.3524  ;
    c(2,4,1)=-7.0240  ;   c(5,3,1)=3.0192   ;
    c(2,2,3)=74.1564  ;   c(5,1,3)=-12.2485 ;
end


P=0.0;
dVtri1 = [0.0 0.0 0.0];
ds= 1/sqrt(2);
invs0 = 1./s.*[1 ds ds]; 
invs = invs0;
for k=0:2:6
    p3=invs0(3)*k;
    for j=0:1:6-k
        p2=invs0(2)*j;
        invs(1)=p2+p3; invs(3)=p2-p3;
        for i=0:1:6-k-j
            fac = c(i+1,j+1,k+1)*spower(i+1,1)*spower(j+1,2)*spower(k+1,3);
            P = P + fac;
           % dVtri1 = dVtri1+fac*invs*[0 i 0;j 0 j;k 0 -k];
            invs(2)=invs0(1)*i;
          % invs = invs0.*[i j k];
           dVtri1 = dVtri1+fac*invs;
        end
    end
end

w = g.*s(1:2)/2;
pol = 1-tanh(w);
polpro = pol(1)*pol(2);

sechf = sech(w);
sechf = sechf.*sechf;

Vtri = P*polpro;

dVtri1 = dVtri1*polpro;
dVtri2 = -P/2*(sechf(1)*pol(2)*g(1)*[0.0 1.0 0.0]...
    +sechf(2)*pol(1)*g(2)*ds*[1.0 0.0 1.0]);

dVtri = dVtri1+dVtri2;
%% Summation
V = V1+V2+V3+Vtri;
dV = dV1+dV2+dV3+dVtri;




