function [prop] = MolecularProp()

%% This function holds all needed molecular properties for QCT
prop = struct('M',0,'mu_bc',0,'mu_ac',0,'mu_ab',0, 'mu_a_bc',0, 'mu_b_ac',0, 'mu_c_ab',0);

% Conversions
%prop.conv = 9.64853e-3; % amu*A^2/fs^2 / eV
conv1 = 1.23976e-4;     % converstion factor, eV / cm^-1

%% Masses of atoms
prop.ma = 16;   % amu, mass of atom
prop.mb = 16;   % amu, mass of the first  atom in the molecule
prop.mc = 16;   % amu, mass of the second atom in the molecule


%% Potential Parameters
prop.LevType = 2; %'Complex000'; % How are ro-vib levels specificed. Options: Complex or Table 

% =1 -> Use complex ro-vib model ( must specify we,wexe, wellDepth, Be, ae, De)
% =2 -> Use table for vib levels and complex for rot levels ( must specif Ev, EvL, Be, ae, De)
% =3 -> Use table for ro-vib levels ( not used) 

% Morse properties 

prop.Dm     = 5.210999979;    %eV
prop.beta   = 2.78;     %A^-1
prop.Re     = 1.207;    %A

% If ro-vib levels are fit to the Complex model then we need to specify
% these: 
prop.Rinf = 100; % How far should the 3rd atom be moved to make the potential independent of it. 
% Fit to Varandas
prop.we =   0.195466478846889; % %Fit to Esposito for levels 0 and 1
prop.wexe = 0.001132864106852; % Fit to Esposito or levels 0 and 1

prop.wellDepth= 5.21275; % From Esposito, potential well depth 
prop.Be     = 1.4456*conv1;   %eV, from Laurendeau for X^3S-g
prop.ae     = 0.0159*conv1;   %eV, from Laurendeau for X^3S-g
prop.De     = 4.84e-6*conv1;  %eV, from Laurendeau for X^3S-g
prop.ntmol  = 1;                              %number of types of molecules       for O-O+O, nmol=1
prop.moltype = [1 1 1];                        %type of molecule for AB, BC and AC
%prop.idia is only used to in RoVibLevels.
%Varandas Potential Energy Levels
Vmax = 46;
% Energy of levels in eV
prop.Ev = zeros(Vmax+1,prop.ntmol);
prop.Ev(:,1) = [ -5.1153; -4.9221; -4.7315; -4.5433; -4.3578;...
    -4.1749; -3.9948; -3.8175; -3.6431; -3.4717;...
    -3.3034; -3.1381; -2.9760; -2.8172; -2.6617;...
    -2.5096; -2.3610; -2.2160; -2.0746; -1.9369;...
    -1.8030; -1.6729; -1.5469; -1.4249; -1.3071;...
    -1.1935; -1.0842; -.97939; -.87913; -.78354;...
    -.69274; -.60685; -.52601; -.45036; -.38004;...
    -.31522; -.25607; -.20277; -.15550; -.11445;...
    -.079818; -0.051751; -0.030323; -0.015397; -0.0063748; 
    -0.0019261; -0.00029275];
% Level quantum number
prop.EvL = zeros(Vmax+1,prop.ntmol);
prop.EvL(:,1) = linspace(0,Vmax, Vmax+1);  
prop.Vmax = zeros(1,prop.ntmol);
prop.Vmax(:,1) = Vmax;


%% Other needed constants. Dont touch these
prop.k = 1.3806488e-23; % m^2kg/s^2/K
prop.hbar = 1.05457148e-34; % m^2kg/s, Plank's const/2pi 
prop.M = prop.ma + prop.mb + prop.mc;                % amu
prop.mu_bc = (prop.mb*prop.mc)/(prop.mb+prop.mc);    % amu
prop.mu_ac = (prop.ma*prop.mc)/(prop.ma+prop.mc);    % amu
prop.mu_ab = (prop.ma*prop.mb)/(prop.ma+prop.mb);    % amu
prop.mu_a_bc = prop.ma*(prop.mb+prop.mc)/prop.M;     % amu 
prop.mu_b_ac = prop.mb*(prop.ma+prop.mc)/prop.M;     % amu
prop.mu_c_ab = prop.mc*(prop.ma+prop.mb)/prop.M;     % amu

prop.mc_mbmc = prop.mc/(prop.mb+prop.mc);
prop.mb_mbmc = prop.mb/(prop.mb+prop.mc);


%% Space Holders for Molecular Parameters
% These are computed within RunVibPeriod
prop.RR = 0;
prop.PP = 0;
prop.IRUN = 0;
prop.R_stretch= zeros(1e7,1);
prop.Pv_stretch = zeros(1e7,1);





return

