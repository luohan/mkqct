%% Test Sample size compare reuslt with esposito
clear
clc
matlabrc
addpath('./Core');  %Core Code
jobid = 1;
PES_use    = 2;
PES_folder = {'One','Double'};
addpath(['./PesData/',PES_folder{PES_use}]); % PES data

log_folder = ['LogXsec/',PES_folder{PES_use}];   % folder to save log
if ~exist(log_folder,'dir')
    mkdir(log_folder);
end
%% Set run enviroment
runon = 1;    %1 for rice 2 for carter 3 for local
cores = 38;
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
else
    p = parcluster('local');
    p.NumWorkers = cores;
%     p.saveProfile;
end
if runon == 1
    cluster = 'rice';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA2/Xsec/',PES_folder{PES_use}];
elseif runon == 2
    cluster = 'carter';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA2/Xsec/',PES_folder{PES_use}];
else
    cluster = 'rice';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA2/Xsec/',PES_folder{PES_use}];
    %trj_folder = ['XsecTrj/',PES_folder{PES_use}];
end

%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%---------------------------------------------------------------------------------------------------------------
%% Load property
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalParTrj();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
%--------------------------------------------------------------------------------------------------------------
%% Set sweep range
kmstoafs=0.01;
Vrel = [11 13 15 17 19 21];    %km/s only ONE value
Ecol = 0.5*prop.mu_a_bc*(kmstoafs*Vrel).^2*par.conv3;
Tcol = Ecol*1.60217662e-19/3*2/prop.k;
Vrel = Vrel*kmstoafs;
Vsweep = [0 1 5 7 10 20];
Jsweep = [0 20 50 100];
I0Loop = 2000;   %maximum inner loop
ILoop = 0;        %change in the code
Ncalls = 1;  % will be changed in function
Remain = 0;
%---------------------------------------------------------------------------------------------------------------
%% set up input
if runon == 1 || runon ==2
    try
        parpool(conf,cores);
    catch
        try
            pause(300)
            parpool(conf,cores);
        catch
            try
                pause(600)
                parpool(conf,cores);
            catch
                pause(600)
                parpool(conf,cores);
            end
        end
    end
else
     parpool(conf);
end
% fprintf(global_log,'Pool is open\n');

%% Take note of running cluster number
if runon==1 || runon==2
    fid=fopen(['hostlist',num2str(jobid)],'w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen(['hostlist',num2str(jobid)],'a+');
        [~,name]=system('hostname');
        fprintf(fid,'Parallel:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end
%% Calculate Trj
tic
for VrLoop = 1:length(Vrel)
    input.Vrel = Vrel(VrLoop);
    
    DATE1 = date;
    DATE2 = clock;
    input.Log = [log_folder,'/Trj_Vr=',num2str(input.Vrel/kmstoafs),'.txt'];
    input.LogFid = fopen(input.Log,'a+');
    fprintf (input.LogFid,' Running Reac Trj Refine \n');
    fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
    
    if isunix
        TrjFile = [trj_folder,'/Vr=',num2str(input.Vrel/kmstoafs),'/Vrel=',num2str(input.Vrel),'.h5'];
        sfolder = [trj_folder,'/Vr=',num2str(input.Vrel/kmstoafs)]; 
    else
        TrjFile = [trj_folder,'\Vr=',num2str(input.Vrel/kmstoafs),'\Vrel=',num2str(input.Vrel),'.h5'];
        sfolder = [trj_folder,'\Vr=',num2str(input.Vrel/kmstoafs)]; 
    end
    
    if ~exist(TrjFile,'file')
        fprintf(input.LogFid,'Vr = %f Trj file is not found\n',input.Vrel);
        continue
    end
    h5fid = H5F.open(TrjFile,'H5F_ACC_RDWR','H5P_DEFAULT');
    
    
    for VLoop = 1: length(Vsweep)
        input.V = Vsweep(VLoop);
        for JLoop = 1: length(Jsweep)
            input.J = Jsweep(JLoop);
            if input.J > prop.Jmax(input.V+1,1)
               continue
            end
            CurrentV = input.V; CurrentJ = input.J;
            h5dataset = ['/V=',num2str(input.V),'/J=',num2str(input.J)]
            
            if ~H5L.exists(h5fid,h5dataset,'H5P_DEFAULT')
                fprintf(input.LogFid,'------Vr = %f V = %d J = %d Trjs are not found\n',input.Vrel,input.V,input.J);
                continue
            end

            [RR, PP, IRUN, R_stretch, Pv_stretch,~] = RunVibPeriod_mex(input.V,input.J,prop,par); % Calculate vib period
            prop.IRUN = IRUN;
            prop.R_stretch= R_stretch;
            prop.Pv_stretch = Pv_stretch;
            prop.RR = RR;
            prop.PP = PP;
            
            %% Start Calculation
            IC = h5read(TrjFile,h5dataset);    %load intial conditions
                  
            %% Next Dissociation
            DisIndex = find(IC(11,:)==4);
            if isempty(DisIndex)
                fprintf (input.LogFid,' State (Vr, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t have dissociation \n', input.Vrel, input.V, input.J);
            else
                savefolder = [sfolder,'/Dis/'];
                if ~exist(savefolder,'dir')
                    mkdir(savefolder);
                end
                DisIC = IC(1:6,DisIndex);
                DisLen = length(DisIndex);
                [Out,Error] = CalculateTrj( DisIC,DisLen,I0Loop,prop,par,input.Vrel,input.V,input.J,'Dis',savefolder);
                fprintf (input.LogFid,' State (Vr, V, J)=(%5.3f, %5.0f, %5.0f) dissociation finished \n', input.Vrel, input.V, input.J);
            end
        end
    end
    H5F.close(h5fid);
    fclose(input.LogFid);
end
toc
