%% Test Sample size compare reuslt with esposito
clear
clc
matlabrc
addpath('./Core');  %Core Code

if isunix
    slash = '/';
else
    slash = '\';
end

PES_use    = 2;
PES_folder = {'One','Double'};
addpath(['./PesData/',PES_folder{PES_use}]); % PES data

jobid = 001;                          % identify hostlist and rand number, 3 digit
global_log = fopen(['Log',num2str(jobid,'%03d'),'.txt'],'a+');

log_folder = ['LogXsec',slash,PES_folder{PES_use}];   % folder to save log
if ~exist(log_folder,'dir')
    mkdir(log_folder);
end


%% Set run enviroment
runon = 1;    %1 for rice 2 for carter 3 for local
cores = 19;
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
else
    p = parcluster('local');
    p.NumWorkers = cores;
%     p.saveProfile;
end
if runon == 1
    cluster = 'rice';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/Xsec/',PES_folder{PES_use}];
elseif runon == 2
    cluster = 'carter';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/Xsec/',PES_folder{PES_use}];
else
%     trj_folder = ['.'];
    trj_folder = ['D:\Research\QCT\Xsec\Double'];
    %%#trj_folder = ['XsecTrj/',PES_folder{PES_use}];
end
fprintf(global_log,'Parallel enviroment set\n');

%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%---------------------------------------------------------------------------------------------------------------
%% Load property
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalParTrj();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
%--------------------------------------------------------------------------------------------------------------
%% Set sweep range
kmstoafs=0.01;
Vrel = [1 3 5 7 9 11];    %km/s only ONE value
Ecol = 0.5*prop.mu_a_bc*(kmstoafs*Vrel).^2*par.conv3;
Tcol = Ecol*1.60217662e-19/3*2/prop.k;
Vrel = Vrel*kmstoafs;
Vsweep = [0 1 5 7 10 20];
Jsweep = [0 20 50 100];
I0Loop = 2500;   %maximum inner loop
ILoop = 0;        %change in the code
Ncalls = 1;  % will be changed in function
Remain = 0;
%---------------------------------------------------------------------------------------------------------------
%% set up input
fprintf(global_log,'Work before pool is done\n');
if runon == 1 || runon ==2
    try
        parpool(conf,cores);
    catch
        try
            pause(300)
            parpool(conf,cores);
        catch
            try
                pause(600)
                parpool(conf,cores);
            catch
                pause(600)
                parpool(conf,cores);
            end
        end
    end
else
    parpool(conf);
end
fprintf(global_log,'Pool is open\n');

%% Take note of running cluster number
if runon==1 || runon==2
    fid=fopen(['hostlist',num2str(jobid)],'w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen(['hostlist',num2str(jobid)],'a+');
        [~,name]=system('hostname');
        fprintf(fid,'Parallel:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end
%% Calculate Trj
tic
time(1) = 0;
time_counter = 1;
fprintf(global_log,' Running Non reaction Trj Refine \n');
fprintf (global_log,' JobId = %3d \n',        jobid);

for VrLoop = 1:length(Vrel)
    input.Vrel = Vrel(VrLoop);
  % log the calculation  
    DATE1 = date;
    DATE2 = clock;
    input.Log = [log_folder,slash,'Trj_Vr=',num2str(input.Vrel/kmstoafs),'.txt'];
    input.LogFid = fopen(input.Log,'a+');
    fprintf (input.LogFid,' Running Non reaction Trj Refine \n');
    fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
    fprintf (input.LogFid,' JobId = %3d \n',        jobid);
    

    fprintf (global_log,'\n Running for Vr=%f A/fs\n',input.Vrel);
    fprintf (global_log, ['  ',DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ]);
    time_counter = time_counter+1;
    time(time_counter) = toc;

    
    TrjFile = [trj_folder,slash,'Vr=',num2str(input.Vrel/kmstoafs),slash,'Vrel=',num2str(input.Vrel),'.h5'];
    sfolder = [trj_folder,slash,'Vr=',num2str(input.Vrel/kmstoafs)];
    
    if ~exist(TrjFile,'file')
        fprintf(input.LogFid,'Vr = %f Trj file is not found\n',input.Vrel);
        continue
    end
    h5fid = H5F.open(TrjFile,'H5F_ACC_RDWR','H5P_DEFAULT');

  % loop between V
    for VLoop = 1: length(Vsweep)
        input.V = Vsweep(VLoop);
        for JLoop = 1: length(Jsweep)
            input.J = Jsweep(JLoop);
            if input.J > prop.Jmax(input.V+1,1)
                continue
            end
            CurrentV = input.V; CurrentJ = input.J;
            h5dataset = ['/V=',num2str(input.V),'/J=',num2str(input.J)]
            
            if ~H5L.exists(h5fid,h5dataset,'H5P_DEFAULT')
                fprintf(input.LogFid,'------Vr = %f V = %d J = %d Trjs are not found\n',input.Vrel,input.V,input.J);
                continue
            end
            [RR, PP, IRUN, R_stretch, Pv_stretch,~] = RunVibPeriod_mex(input.V,input.J,prop,par); % Calculate vib period
            prop.IRUN = IRUN;
            prop.R_stretch= R_stretch;
            prop.Pv_stretch = Pv_stretch;
            prop.RR = RR;
            prop.PP = PP;
                
            %% Start Calculation
            h5did = H5D.open(h5fid,h5dataset);
            IC = H5D.read(h5did);
            H5D.close(h5did);       %load intial conditions
            
            %% Find initial conditions to sweep
            Index = find(IC(11,:)==1 );   
            
            VIndex = find((round(IC(8,:)) ~= CurrentV) & IC(11,:)==1);  %vibrational level changes
                        
            NVIndex  = setdiff(Index,VIndex);            
            
            %  ------------ the followings are calculated
            Jindex = find(round(IC(9,NVIndex)) ~= CurrentJ);  %only rotational level change
            
            Nindex = setdiff(NVIndex,Jindex);   % v and J both don't change
            
            VJIndex = find(round(IC(9,VIndex)) ~= CurrentJ);  % both VJ changes
            
            VIndex = setdiff(VIndex,VJIndex);   % only V changes
            %  ------------ the above are calculated

            Index = {VJIndex,VIndex,Jindex,Nindex};
            IndexType = {'VJ','V','J','Elastic'};
            
            fprintf (global_log,'State (Vr, V, J)=(%5.3f, %5.0f, %5.0f):\n',input.Vrel, input.V, input.J);
            
            time_counter_beforerun = time_counter;
                        
            for TLoop = 1:length(IndexType)
                CIndex = Index{TLoop};
                CType  = IndexType{TLoop};
                fprintf(global_log,'   %s : %d ',CType,length(CIndex));
                time_counter = time_counter+1;
                if isempty(CIndex)
                    fprintf (input.LogFid,' State (Vr, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t have %s trajectories.\n',...
                        input.Vrel, input.V, input.J, IndexType{TLoop});
                else
                    savefolder = [sfolder,slash,'NonReac',slash];
                    if ~exist(savefolder,'dir')
                        mkdir(savefolder);
                    end
                    CurrentIC = IC(1:6,CIndex);
                    CurrentLen = length(CIndex);
                    [Out,Error] = CalculateTrj( CurrentIC,CurrentLen,I0Loop,prop,par,input.Vrel,input.V,input.J,CType,savefolder);
                    time(time_counter) = toc;
                    fprintf (input.LogFid,' State (Vr, V, J)=(%5.3f, %5.0f, %5.0f) %s trajectories take %f min.\n',...
                        input.Vrel, input.V, input.J, IndexType{TLoop},(time(time_counter)-time(time_counter-1))/60);
                end
            end
            fprintf(global_log,'   Time used: %f min \n',(time(time_counter)-time(time_counter_beforerun))/60);
        end
    end
    H5F.close(h5fid);
    fclose(input.LogFid);
end
fclose(global_log);
delete(gcp)
