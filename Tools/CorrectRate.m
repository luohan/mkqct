%% Test Sample size compare reuslt with esposito
clear
clc
%% Set run enviroment
runon = 2;    %1 for rice 2 for carter 3 for local
cores = 16;
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),',',walltime,' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
    matlabpool(conf,cores);
else
    p = parcluster('local');
    p.NumWorkers = cores;
    p.saveProfile;
    parpool(conf,cores);
end

%% set data location
if runon == 1
    load_direc = '/scratch/rice/l/luo160/Equil/';
elseif runon == 2
    load_direc = '/scratch/carter/l/luo160/Equil/';
else
    load_direc = 'T=10000\';
end
save_direc = load_direc;
loghead = 'DATA/log';
%% set sweep range
%T = 2500*[1:3];
T = 2500*[5 6];
%T = 2500*[6:7];
%T = 20000;

tic
time(1) = 0;
for i=1:length(T)
    filename = [load_direc,'T=',num2str(T(i)),'.mat'];
    logname = [loghead,'_T=',num2str(T(i)),'.txt'];
    fid = fopen(logname,'w');
    fprintf(fid,'Correct for T = %d start at %f s\n',T(i),time(i));
    [StateRate,ErrorCount,input,prop,par] = CorrectWrongRate(filename,fid);
    filesave = [save_direc,'T=',num2str(T(i)),'_correct.mat'];
    save(filesave,'StateRate','ErrorCount','input','prop','par','-v7.3');
    clear StateRate ErrorCount input prop par
    fclose(fid);
    fprintf('T = %d is finished\n',T(i));
end
    