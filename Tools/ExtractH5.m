V = 1;
J = 20;
file = 'Vrel=0.01.h5'
dest_file = 'test.h5'
if (exist(dest_file,'file') == 0)
    h5fid = H5F.create(dest_file);
else
    h5fid = H5F.open(dest_file,'H5F_ACC_RDWR','H5P_DEFAULT');
end
%create group
for i = 1:length(V)
    CurrentV = V(i);
    Vlink = strcat('/V=',num2str(CurrentV));
    if H5L.exists(h5fid,Vlink,'H5P_DEFAULT')
        gid=H5G.open(h5fid,Vlink,'H5P_DEFAULT');
    else
        gid=H5G.create(h5fid,Vlink,'H5P_DEFAULT','H5P_DEFAULT','H5P_DEFAULT');
    end
    for j = 1:length(J)
        CurrentJ = J(j);
        h5dataset=strcat(Vlink,'/J=',num2str(CurrentJ));
        DAT = h5read(file,h5dataset);
        [m,n]=size(DAT);
        type_id = H5T.copy('H5T_NATIVE_DOUBLE'); 
        unlimited = H5ML.get_constant_value('H5S_UNLIMITED');
        space_id = H5S.create_simple(2,[n m],[unlimited m]);
        dcpl = H5P.create('H5P_DATASET_CREATE');
        H5P.set_chunk(dcpl,[100 m]);
       % try
            did = H5D.create(gid,['J=',num2str(CurrentJ)],type_id,space_id,dcpl);
       % catch
        %    did = H5D.open(gid,num2str(CurrentJ));
   %     end
        H5D.write(did,type_id,space_id,'H5S_ALL','H5P_DEFAULT',DAT);
        H5D.close(did);
    end
    H5G.close(gid);
end
H5F.close(h5fid);
