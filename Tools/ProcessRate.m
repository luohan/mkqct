editclear
clc
load ParameterOne.mat
T = 20000;

n=3;
Vl = cell(1,n);
fname = ['T=',num2str(T),'/T=',num2str(num2str(T)),'.h5'];
cg = ' |awk ''/group.+\/V/'' | sed ''s/group.\+\/V=\([0-9]\+\)/\1/''';
for i = 1:n
    if i == 1
        folder = '/scratch/rice/l/luo160/DATA2/EqRate/One/';
    else
        folder = ['/scratch/rice/l/luo160/DATA2/EqRate',num2str(i),'/One/'];
    end
    crun = ['h5dump -n ',folder,fname,cg];
     [~,a]=system(crun);
    Vl{i}=sort(str2num(a));
end

Vl2 = [];
for i = 1:n
    Vl2 = [Vl2, Vl{i}'];
end

h5name =[ '/scratch/rice/l/luo160/DATA2/EqRate/One/',fname];
for i = 2:3
    folder = ['/scratch/rice/l/luo160/DATA2/EqRate',num2str(i),'/One/'];
    
    for CurrentV=Vl{i}'
        for j=1:prop.Jmax(CurrentV+1,1)+1
            CurrentJ = j-1;
            h5dataset = ['/V=',num2str(CurrentV),'/J=',num2str(CurrentJ)];
            try
            data = h5read([folder,fname],h5dataset);
            [m,n]=size(data);
            if n~=0
                try
                    h5create(h5name,h5dataset,[14 Inf],'Datatype','double','ChunkSize',[14,100],'Deflate',9);
                catch
                end
                h5write(h5name,h5dataset,data,[1,1],[m n]);
            else
                h5dataset
            end
            catch
                h5dataset
            end
        end
    end
end
