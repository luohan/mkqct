h5name = 'Vrel=0.072.h5';

Vsweep = [0 5];%30:prop.Vmax(1);
Jsweep = [0 20];

for i=1:length(Vsweep)
    CV = Vsweep(i);
    for j=1:(prop.Jmax(CV+1,1)+1)
        CJ = j-1;
        h5dataset = ['/V=',num2str(CV),'/J=',num2str(CJ)];
        try
            
            data = h5read('Vrel=0.07.h5',h5dataset);
            
            [m,n]=size(data);
            try
            h5create(h5name,h5dataset,[14 Inf],'Datatype','double','ChunkSize',[14,100],'Deflate',9);
            catch
            end
            
            
            h5write(h5name,h5dataset,data,[1,1],[m n]);
            h5dataset
        catch
        end
    end
end
