function [StateRate,ErrorCount,input,prop,par] = CorrectWrongRate(file,fid)
%% correct rate for quantum decomposition error
prop=struct; ErrorCount=[]; StateRate=[]; par=struct; input=[];
load(file, 'prop', 'ErrorCount', 'StateRate', 'par', 'input');
Ncalls = input.Ncalls;
Ttrans = input.Ttrans;
Bmax   = input.Bmax;
for i = 1:prop.Vmax(1)+1
    CurrentV = i-1;
    for j = 1:prop.Jmax(i,1)+1
        CurrentJ = j-1;
        OldErrorCount = ErrorCount{i,j}.Count;
        
        if OldErrorCount ~= 0 && OldErrorCount ~= Ncalls
            EClist    = ErrorCount{i,j}.ListOfErrors;
            
            % find out the ones we need to recalculate
            ErrorFieldList = [EClist.code];
            index = find(ErrorFieldList == 53 | ErrorFieldList==54);   %the one we need to recalculate
            PossibleCorret = length(index);
            
            % get inital condition matrix
            if ~isempty(index)
                fprintf(fid,'  V = %d, J = %d\n',CurrentV,CurrentJ);
                %convert the original State Rate to RoVibLevels
                temp = sqrt(8*pi*prop.k*Ttrans/prop.mu_a_bc/par.conv5)*(Bmax*par.conv6)^2;
                RoVibLevel = StateRate{i,j}.RATE*(Ncalls-OldErrorCount)/temp;
                
                % get initial condition
                Temp1 = struct2cell(EClist);
                Temp1 = Temp1(2:7,:,index);
                Initial = squeeze(cell2mat(Temp1));  %row [Th Phi Psi Eta B Vr]
                
                % eliminate which we modify in Errorlist
                index2 = setdiff([1:OldErrorCount],index);
                NewErrorCount = length(index2);
                if NewErrorCount == 0
                    EClist =[];
                else
                    EClist = EClist(index2);
                end
                
                % Let's calculate
                [RR, PP, IRUN, R_stretch, Pv_stretch,~] = RunVibPeriod_mex(CurrentV,CurrentJ,prop,par); % Calculate vib period
                prop.IRUN = IRUN;
                prop.R_stretch= R_stretch;
                prop.Pv_stretch = Pv_stretch;
                prop.RR = RR;
                prop.PP = PP;
                out(PossibleCorret) = struct( 'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0);
                error(PossibleCorret) = struct('code', 0, 'where', 0, 'what', 0);
                
                parfor k = 1: PossibleCorret
                    CI = Initial(:,k);
                    [out(k), error(k)] = Backbone(CI(5),CI(6),CI(1),CI(2),CI(3),CI(4),CurrentJ,CurrentV,prop,par,par.filename1,par.filename2);
                end
                
                % post process
                for l = 1 : PossibleCorret
                    if error(l).code == 0 || error(l).code ==41 || error(l).code ==42 % Lets log the good calculations
                        
                        
                        if out(l).VLevel <0  % This is were the round will fail.
                            out(l).VLevel = 0;
                        end
                        
                        % Rate
                        RoVibLevel(out(l).React,round(out(l).VLevel)+1,round(out(l).JLevel)+1) = RoVibLevel(out(l).React,round(out(l).VLevel)+1,round(out(l).JLevel)+1)+1; %Note, first index is 0th level
                    else %Lets also log the errors
                        NewErrorCount = NewErrorCount + 1;
                        EClist(NewErrorCount).code = error(l).code;
                        EClist(NewErrorCount).Th = Initial(1,l);
                        EClist(NewErrorCount).Phi = Initial(2,l);
                        EClist(NewErrorCount).Psi = Initial(3,l);
                        EClist(NewErrorCount).Eta = Initial(4,l);
                        EClist(NewErrorCount).B = Initial(5,l);
                        EClist(NewErrorCount).Vr = Initial(6,l);
                        EClist(NewErrorCount).V = CurrentV;
                        EClist(NewErrorCount).J = CurrentJ;
                        EClist(NewErrorCount).out = out(l);
                        EClist(NewErrorCount).error = error(l);
                    end
                end
                fprintf(fid,'           Original Error: %d/%d for QM, Final: %d Correct: %d \n',...
                    PossibleCorret,OldErrorCount,NewErrorCount,PossibleCorret-NewErrorCount);

                EC.Count = NewErrorCount;
                if NewErrorCount == 0
                    EC.ListOfErrors = 0;
                else
                    EC.ListOfErrors=EClist;
                end
                
                
                output.RATE = sqrt(8*pi*prop.k*Ttrans/prop.mu_a_bc/par.conv5)*(Bmax*par.conv6)^2 * RoVibLevel/(Ncalls-NewErrorCount);
                
                
                % We have nothing to do with this ..sigh
                output.CI11 = StateRate{i,j}.CI11;
                output.CI12 = StateRate{i,j}.CI12;
                output.CI13 = StateRate{i,j}.CI13;
                output.CI22 = StateRate{i,j}.CI22;
                output.CI23 = StateRate{i,j}.CI23;
                
                StateRate{i,j} = output;
                ErrorCount{i,j} = EC;
                
                clear out error EClist EC
            end
        end
    end
end
end


