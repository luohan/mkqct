%% Test Sample size compare reuslt with esposito
clear
clc
addpath('./Core');  %Core Code

PES_use    = 1;
PES_folder = {'One','Double'};
addpath(['./PesData/',PES_folder{PES_use}]); % PES data

jobid = 406;                          % identify hostlist and rand number, 3 digit
global_log = fopen(['Log',num2str(jobid,'%03d'),'.txt'],'a+');

log_folder = ['LogXsec/',PES_folder{PES_use}];
if ~exist(log_folder,'dir')
    mkdir(log_folder);
end

data_folder = ['Xsec/',PES_folder{PES_use}];
if ~exist(data_folder,'dir')
    mkdir(data_folder);
end

%% Set run enviroment
%% Set run enviroment
runon = 1;    %1 for rice 2 for carter 3 for local
cores = 40;
if runon == 1
    conf = 'standby_rice';
    ppn = 20; nodes=floor(cores/ppn);
    walltime = 'walltime=4:00:00';
elseif runon == 2
    conf = 'standby_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=4:00:00';
else
    conf = 'local';
end
%if runon == 1 || runon ==2
%    p = parcluster(conf);
%    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),' -q standby'];
%    p.SubmitArguments = ['-l ',walltime];
%    p.saveProfile;
%else
%    p = parcluster('local');
%    p.NumWorkers = cores;
%    p.saveProfile;
%end
if runon == 1
    cluster = 'rice';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/Xsec/',PES_folder{PES_use}];
elseif runon == 2
    cluster = 'carter';
    trj_folder = ['/scratch/',cluster,'/l/luo160/DATA/Xsec/',PES_folder{PES_use}];
else
    trj_folder = ['XsecTrj/',PES_folder{PES_use}];
end
fprintf(global_log,'Parallel enviroment set\n');

%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%---------------------------------------------------------------------------------------------------------------
%% Load property
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);
%--------------------------------------------------------------------------------------------------------------
%% Set sweep range
kmstoafs=0.01;
Ecol = [6];   % total collisional energy
Vrel = sqrt(2*Ecol/prop.mu_a_bc/par.conv5*1.60217662e-19)/1000;  %unit: km/s

Tcol = Ecol*1.60217662e-19/3*2/prop.k;  %this value doesn't matter
Vrel = Vrel*kmstoafs;  %unit: A/fs
Vsweep = [0 1 5 10 20 30 7 15 25 40];
Jsweep = [0 20 50 100 150];
%---------------------------------------------------------------------------------------------------------------
%% set up input
input.Bmax=6.5; % A
input.Calc=1; % Type 1 is X-sect calc, Type 2 is state specific rate calc., Type 3 is monoquantum Deex
input.Ttrans=300; % Not important
input.Ncalls = 1e6;
input.InnerLoops =100;
input.SaveData = 1;

fprintf(global_log,'Work before pool is done\n');
if runon == 1 || runon ==2
    try
        parpool(conf,cores);
    catch
        try
            pause(300)
            parpool(conf,cores);
        catch
            try
                pause(600)
                parpool(conf,cores);
            catch
                pause(600)
                parpool(conf,cores);
            end
        end
    end
else
    parpool(conf,cores);
end
fprintf(global_log,'Pool is open\n');

%% Take note of running cluster number
if runon==1 || runon==2
    fid=fopen(['hostlist',num2str(jobid)],'w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen(['hostlist',num2str(jobid)],'a+');
        [~,name]=system('hostname');
        fprintf(fid,'Parallel:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end
if exist(['random',num2str(jobid,'%03d'),'.mat'],'file')
    load(['random',num2str(jobid,'%03d'),'.mat'])
    rng(rangset)
end
%% Write gllobal log

DATE1 = date;
DATE2 = clock;
fprintf (global_log,' Running CrossSection Sweep \n');
fprintf (global_log, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
fprintf (global_log,' JobId      = %3d \n',        jobid);
fprintf (global_log,' Bmax       = %5.3f A\n', input.Bmax);
fprintf (global_log,' Calc Type  = %5.3f \n', input.Calc);
fprintf (global_log,' Trans      = %10.3f K \n', input.Ttrans);
fprintf (global_log,' Ncalls     = %5.3f \n', input.Ncalls);
fprintf (global_log,' InnerLoops = %5.3f \n', input.InnerLoops);
fprintf (global_log,' Ecol       = ['); fprintf (global_log,'%5.2f', Ecol);fprintf (global_log,']\n');
fprintf (global_log,' Vsweep     = ['); fprintf (global_log,'%5d', Vsweep);fprintf (global_log,']\n');
fprintf (global_log,' Jsweep     = ['); fprintf (global_log,'%5d', Jsweep);fprintf (global_log,']\n');


tic
time(1) = 0;
l = 1;
Output = cell(1,length(Vsweep),length(Jsweep));
ErrorC = cell(1,length(Vsweep),length(Jsweep));

Output_global = cell(length(Ecol),length(Vsweep),length(Jsweep));
ErrorC_global = cell(length(Ecol),length(Vsweep),length(Jsweep));

for EcLoop = 1:length(Ecol);
    input.Ecol = Ecol(EcLoop);
    
    input.Log = [log_folder,'/Log_Ecol=',num2str(input.Ecol),'.txt'];
    input.LogFid = fopen(input.Log,'a+');
    fprintf (input.LogFid,' Running CrossSection Sweep \n');
    fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
    fprintf (input.LogFid,' JobId      = %3d \n',        jobid);
    fprintf (input.LogFid,' Bmax       = %5.3f \n', input.Bmax);
    fprintf (input.LogFid,' Calc Type  = %5.3f \n', input.Calc);
    fprintf (input.LogFid,' Trans      = %10.3f \n', input.Ttrans);
    fprintf (input.LogFid,' Ncalls     = %5.3f \n', input.Ncalls);
    fprintf (input.LogFid,' InnerLoops = %5.3f \n', input.InnerLoops);
    
    
    input.TrjLoc = [trj_folder,'/Ecol=',num2str(input.Ecol)];
    if ~exist(input.TrjLoc,'dir')
        mkdir(input.TrjLoc);
    end
    input.TrjLog = [log_folder,'/Log_Trj_Ecol=',num2str(input.Ecol),'.txt'];
    input.TrjFid = fopen(input.TrjLog,'a+');
    
    
    Restartfile = [data_folder,'/Ecol=',num2str(input.Ecol),'/'];
    if ~exist(Restartfile,'dir')
        mkdir(Restartfile);
    end
    for VLoop = 1: length(Vsweep)
        clear ErrorC Output               %for safety
        Output = cell(1,length(Vsweep),length(Jsweep));
        ErrorC = cell(1,length(Vsweep),length(Jsweep));

        input.V = Vsweep(VLoop);
        for JLoop = 1: length(Jsweep)
            l = l + 1;
            input.J = Jsweep(JLoop);
            
            if input.J > prop.Jmax(input.V+1,1)
                fprintf (input.LogFid,' State (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t exist, J is too high\n', input.Ecol, input.V, input.J);
                fprintf (global_log,' State (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t exist, J is too high\n', input.Ecol, input.V, input.J);
                continue
            end
            
            Et = input.Ecol-RoVibLevels(input.V,input.J,prop)-prop.wellDepth(1);
                        
            if Et < 0
                fprintf (input.LogFid,' State (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t exist \n', input.Ecol, input.V, input.J);
                fprintf (global_log,' State (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) doesn''t exist \n', input.Ecol, input.V, input.J);
                continue
            end

            name2 = strcat(Restartfile,'Ecol=',num2str(input.Ecol),'V=',num2str(input.V),'J=',num2str(input.J),'.mat');
            time(l) = toc;
            if exist(name2,'file')
                load(name2)
                Output{1,VLoop,JLoop} = CurrentOut;
                ErrorC{1,VLoop,JLoop} = CurrentError;
                Output_global{EcLoop,VLoop,JLoop} = CurrentOut;
                ErrorC_global{EcLoop,VLoop,JLoop} = CurrentError;
                
                fprintf (input.LogFid,' Load (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ecol, input.V, input.J, ErrorC{1, VLoop, JLoop}.Count);
                fprintf (input.LogFid,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);
            else
              input.Vrel = sqrt(2*Et/par.conv3/prop.mu_a_bc);

              % Calculate Cross Sections for this level
              [Output{1,VLoop,JLoop}, ErrorC{1,VLoop,JLoop}] = CrossSection_Rate_Calc_Ecol_Standby(input, prop, par); % saved to global
              Output_global{EcLoop,VLoop,JLoop} = Output{1,VLoop,JLoop};
              ErrorC_global{EcLoop,VLoop,JLoop} = ErrorC{1,VLoop,JLoop};

              time(l) = toc;

              % Report progress
              fprintf (' Ran (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ecol, input.V, input.J, ErrorC{1, VLoop, JLoop}.Count);
              fprintf (' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600)
              fprintf (input.LogFid,' Ran (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ecol, input.V, input.J, ErrorC{1, VLoop, JLoop}.Count);
              fprintf (input.LogFid,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);
              fprintf (global_log,' Ran (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ecol, input.V, input.J, ErrorC{1, VLoop, JLoop}.Count);
              fprintf (global_log,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);

              fprintf (input.TrjFid,' Ran (Ecol, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ecol, input.V, input.J, ErrorC{1, VLoop, JLoop}.Count);
              fprintf (input.TrjFid,' This took %5.3f s . Total %f hours \n', time(l)-time(l-1),time(l)/3600);
              CurrentOut = Output{1,VLoop,JLoop};
              CurrentError = ErrorC{1,VLoop,JLoop};
              name2 = strcat(Restartfile,'Ecol=',num2str(input.Ecol),'V=',num2str(input.V),'J=',num2str(input.J),'.mat');
              save(name2,'CurrentOut','CurrentError');
              name2 = strcat(input.TrjLoc,'/Ecol=',num2str(input.Ecol),'V=',num2str(input.V),'J=',num2str(input.J),'.mat');
              save(name2,'CurrentOut','CurrentError');
          end
        end
    end
    name2 = strcat(trj_folder,'/Ecol=',num2str(input.Ecol),'.mat');
    fclose(input.TrjFid);
    temp = whos;
    if max([temp.bytes])/1024/1024/1024 > 1.2
      save(name2,'Output','ErrorC','input','prop','par','Ecol','EcLoop','Vsweep','Jsweep','-v7.3');
    else
      save(name2,'Output','ErrorC','input','prop','par','Ecol','EcLoop','Vsweep','Jsweep');
    end
    
    fclose(input.LogFid)
    fclose(input.TrjFid)
    
end
rangset = rng;
save(['random',num2str(jobid,'%03d'),'.mat'],'rangset')

clear Output ErrorC
Output = Output_global; ErrorC = ErrorC_global;
clear Output_global ErrorC_global
name2 = [data_folder,num2str(jobid,'%03d'),'.mat'];
if max([temp.bytes])/1024/1024/1024 > 1.2
    save(name2,'-v7.3');
else
    save(name2);
end
fclose all;
delete(gcp)
