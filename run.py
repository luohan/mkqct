#!/usr/bin/python

import shlex,time,os
from subprocess import PIPE,Popen

sfile = raw_input("Name of function: ")
workname = raw_input("Work Name: ")
runc = int(raw_input("Runtime: "))
lenid = int(raw_input("Length of workid: "))
stindex = int(raw_input("Inputstart index: "))
for i in range(runc):
  fname = workname+str(i+stindex)+'.sub'
  ofile = open(fname,'w')
  ofile.write("#!/bin/sh -l\n");
  ofile.write("#PBS -q standby\n")
  ofile.write("#PBS -N "+workname+"_"+str(i+stindex)+"\n")
  ofile.write("#PBS -l nodes=1:ppn=1,naccesspolicy=singleuser,walltime=4:00:00\n")
#  ofile.write("#PBS -m ae\n")
  ofile.write("module load matlab/R2016a\ncd $PBS_O_WORKDIR\n")
  ofile.write("matlab -nodisplay -r "+sfile)
  ofile.close()

firstid = int(raw_input("Input first workid(0 if this is the begging"))
for i in range(runc):
  fname = workname+str(i+stindex)+'.sub'
  if i == 0:
    if firstid == 0:
       command = "qsub "+fname
    else:
       print("This is a continue work")
       id=str(firstid)
       continue  
  else:
    command = "qsub -W depend=afterany:"+id+" "+fname;
    command = str(command)
    print(command+"\n")
  c=Popen(shlex.split(command),stdout=PIPE,stderr=PIPE)
  o,e = c.communicate()
  o = o.decode("utf-8")
  id = o[0:lenid];
#  print("Work: "+str(i)+" id:"+id+"\n")
  time.sleep(2)
  print(o)
  if i != 0:
    os.remove(fname)     
