function [qtest,out,error] = CallMaratCode( X0,P0,prop,par )
%% call marat's code
oldpath = addpath('E:\Research\QCT\QCT_CodeVersions\plottrj\');
% oldpath = addpath('E:\Git\mkqct\Core');

Tinv = [zeros(3,3), eye(3,3)*(prop.mb+prop.mc)/prop.M, eye(3,3); ...
        -eye(3,3)*(prop.mc/(prop.mb+prop.mc)), -eye(3,3)*(prop.ma/prop.M),eye(3,3);...
        eye(3,3)*(prop.mb/(prop.mb+prop.mc)), -eye(3,3)*(prop.ma/prop.M),eye(3,3)];
    
T = [zeros(3,3), -eye(3,3), eye(3,3);...
    eye(3,3), -eye(3,3)*(prop.mb/(prop.mb+prop.mc)), -eye(3,3)*(prop.mc/(prop.mb+prop.mc));...
    eye(3,3)*prop.ma/prop.M, eye(3,3)*prop.mb/prop.M,eye(3,3)*prop.mc/prop.M];


S = [zeros(3,3), -eye(3,3)*prop.mc_mbmc, eye(3,3)*prop.mb_mbmc;...
    eye(3,3)*(prop.mb+prop.mc)/prop.M, -eye(3,3)*prop.ma/prop.M, -eye(3,3)*prop.ma/prop.M; ...
    eye(3,3), eye(3,3), eye(3,3)];

Sinv = [zeros(3,3), eye(3,3),                         eye(3,3)*prop.ma/prop.M;...
    -eye(3,3), -eye(3,3)*(prop.mb/(prop.mb+prop.mc)), eye(3,3)*prop.mb/prop.M;...
     eye(3,3), -eye(3,3)*(prop.mc/(prop.mb+prop.mc)), eye(3,3)*prop.mc/prop.M];
 
Q = T*X0;
P = S*P0;

Y0 = [Q(1:6);P(1:6)];
Y0 = Y0';
error = struct('code', 0, 'where', 'Nowhere', 'what', 'Nothing');
out = struct( 'React', 0, 'Chi', 0, 'Vr', 0, 'JLevel', 0, 'VLevel', 0, 'Interaction', 0, 'EtolInteg', 0, 'EtolTot', 0);

[Yf,out, error] = Integration (Y0, out, prop,par ); % Integrate the trajectories

[out,error] = PostCollision (Yf,out,prop,par, 2); % Lets figure out what we have now


path(oldpath)
load trj qtest
end

