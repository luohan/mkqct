%% Test Sample size compare reuslt with esposito
clear
clc
%% Set run enviroment
runon = 1;    %1 for rice 2 for carter 3 for local
cores = 38;
if runon == 1
    conf = 'qct_rice';
    ppn = 19; nodes=floor(cores/ppn);
    walltime = 'walltime=5:00:00';
    cluster = 'rice';
elseif runon == 2
    conf = 'qct_carter';
    ppn = 16;nodes=floor(cores/ppn);
    walltime = 'walltime=200:00:00';
    cluster = 'carter';
else
    conf = 'local';
end
if runon == 1 || runon ==2
    p = parcluster(conf);
    p.ResourceTemplate = ['-l nodes=',num2str(nodes),':ppn=',num2str(ppn),',',walltime,' -q alexeenk'];
    p.SubmitArguments = ['-l ',walltime];
    p.saveProfile;
else
    p = parcluster('local');
    p.NumWorkers = cores;
    p.saveProfile;
end
%% Compile the matlab code to C code
CompileFlag = 0;
if CompileFlag == 1 || exist('RunVibPeriod_mex','file') ~= 3 || exist('Backbone_mex','file') ~= 3 % Do I need to recompile?
    % Lets clean up our files first
    if exist('CComp','dir') % Lets clean up our files first
        rmdir('CComp', 's')
    end
    
    if exist('CComp2','dir') % Lets clean up our files first
        rmdir('CComp2', 's')
    end
    
    if  exist('Backbone_mex','file')
        delete('Backbone_mex.mexa64')
    end
    
    if  exist('RunVibPeriod_mex','dir')
        delete('RunVibPeriod_mex.mexa64')
    end
    
    CompileQCT;
end
%% Take note of running cluster number
if runon == 1 || runon ==2
    try
        matlabpool(conf,cores);
    catch
        try
            pause(300)
            matlabpool(conf,cores);
        catch
            try
                pause(600)
                matlabpool(conf,cores);
            catch
                pause(600)
                matlabpool(conf,cores);
            end
        end
    end
else
    parpool(conf,cores);
end

if runon==1 || runon==2
    fid=fopen('hostlist','w');
    [~,name]=system('hostname');
    fprintf(fid,'Serial:  %s\n',name);
    fclose(fid);
    parfor i=1:cores
        fid=fopen('hostlist','a+');
        [~,name]=system('hostname');
        fprintf(fid,'Parallel:  %s\n',name);
        fclose(fid);
    end
    clear name fid
end

%% set up input
input.Bmax=6.5; % A
input.Calc=2; % Type 1 is X-sect calc, Type 2 is state specific rate calc., Type 3 is monoquantum Deex
input.Vrel=1; %useless
input.Ncalls = 1;
input.InnerLoops =1;
input.SaveData=1;
Restartfile = ['/scratch/',cluster,'/l/luo160/Equil/double'];
if ~exist(Restartfile,'dir')
    mkdir(Restartfile);
end
if ~exist('DATA','dir')
    mkdir('DATA');
end
%% Load property
prop = MolecularProp();                                                      % grab needed molecular properties
par = NumericalPar();                                                        % also look up numerical parameters
prop.Rmin = MinEnergy(prop,par);                                             % calculate the postion of minimum energy
[prop.Jmax,prop.RVmaxBC, prop.Vmax, prop.MaxJlevel] = MaxLevels(prop,par);            % and max ro-vibrational levels
prop.MVmax = max(prop.Vmax);

%% Set sweep range
Tsweep = [25000];
Vsweep = 0:prop.Vmax(1);

%EquilOut=struct('T',num2cell(Tsweep));       %ALL DATA ARE SAVED HERE!!!
%% Calculation
for Tloop = 1:length(Tsweep)
    tic
    clear time
    time(1) = 0;
    l = 1;
    
    input.Ttrans = Tsweep(Tloop);
    input.Log = ['DATA/Log_T=',num2str(input.Ttrans),'.txt'];
    input.LogFid = fopen(input.Log,'a+');
    
    input.TrjLog = ['DATA/Log_Trj_T=',num2str(input.Ttrans),'.txt'];
    input.TrjFid = fopen(input.TrjLog,'a+');
    input.TrjLoc = Restartfile;
    
    RestartT = [Restartfile,'/T=',num2str(input.Ttrans)];
    if ~exist(RestartT,'dir')
        mkdir(RestartT);
    end
    DATE1 = date;
    DATE2 = clock;
    fprintf (input.LogFid,' Running Equilibrium Rate Calculation \n');
    fprintf (input.LogFid, strcat (DATE1,'_ ', num2str(DATE2(4)), 'h_', num2str(DATE2(5)), 'm_', num2str(DATE2(6)), 's \n' ));
    fprintf (input.LogFid,' Bmax       = %5.3f \n', input.Bmax);
    fprintf (input.LogFid,' Trans      = %10.3f \n', input.Ttrans);
    
    
    StateRate = cell(prop.Vmax(1)+1,prop.Jmax(1,1)+1);
    ErrorCount = cell(prop.Vmax(1)+1,prop.Jmax(1,1)+1);
    
    for VLoop = 1: (prop.Vmax(1)+1)
        input.V = VLoop-1;
        
        time(l) = toc;
        name2 = [RestartT,'/V=',num2str(input.V),'.mat'];
        if exist(name2,'file')
            load(name2,'CurrentStateRate','CurrentErrorCount')
            StateRate(VLoop,:) = CurrentStateRate;
            ErrorCount(VLoop,:)= CurrentErrorCount;
            l = l+1;
            time(l) = toc;
            fprintf (' Load (T, V)=(%5.3f, %5.0f)  \n', input.Ttrans, input.V);
            fprintf (' This took %5.3f s . Total %f mins \n', time(l)-time(l-1),time(l)/60);
            
            fprintf (input.LogFid,' Load (T, V)=(%5.3f, %5.0f)  \n', input.Ttrans, input.V);
            fprintf (input.LogFid,' This took %5.3f s . Total %f mins \n', time(l)-time(l-1),time(l)/60);
        else
            for JLoop = 1: (prop.Jmax(VLoop)+1)
                input.J = JLoop-1;
                
                time(l) = toc;
                
                [StateRate{VLoop,JLoop}, ErrorCount{VLoop,JLoop}] = CrossSection_Rate_Calc_NNO(input, prop, par);
                
                l = l+1;
                time(l) = toc;
                
                % Report progress
                fprintf (' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                fprintf (' This took %5.3f s . Total %f mins \n', time(l)-time(l-1),time(l)/60)
                
                fprintf (input.LogFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                fprintf (input.LogFid,' This took %5.3f s . Total %f mins \n', time(l)-time(l-1),time(l)/60);
                
                fprintf (input.TrjFid,' Ran (T, V, J)=(%5.3f, %5.0f, %5.0f) with %5.0f errors \n', input.Ttrans, input.V, input.J,  ErrorCount{VLoop,JLoop}.Count);
                fprintf (input.TrjFid,' This took %5.3f s . Total %f mins \n', time(l)-time(l-1),time(l)/60);
                
            end
            % temperoray save
            CurrentStateRate = StateRate(VLoop,:);
            CurrentErrorCount = ErrorCount(VLoop,:);
            
            name2 = [RestartT,'/V=',num2str(input.V),'.mat'];
            temp = whos('CurrentErrorCount');
            if max([temp.bytes])/1024/1024/1024 > 1.2
                save(name2,'CurrentStateRate','CurrentErrorCount','input','-v7.3');
            else
                save(name2,'CurrentStateRate','CurrentErrorCount','input');
            end
        end
        
    end
    
    fprintf (input.LogFid,'Calculation is done\n.');
    fprintf (input.TrjFid,'Calculation is done\n.');
    
    clear CurrentStateRate
    clear CurrentErrorCount
    
    name2 = [RestartT,'.mat'];
    temp = whos;
    if max([temp.bytes])/1024/1024/1024 > 1.2
        save(name2,'-v7.3');
    else
        save(name2);
    end
    
    fprintf (input.LogFid,'Multiple data file is saved');
    fprintf (input.TrjFid,'Multiple data file is saved');
    
    clear StateRate;
    clear ErrorCount;
    fclose(input.LogFid);
    fclose(input.TrjFid);
    
end

